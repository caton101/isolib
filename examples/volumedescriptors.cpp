/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "../include/isochop.hpp"

/**
 * @brief  This crashes the demo program in the event of an error
 * @param  message: the error message
 * @retval None
 */
void crash(std::string message)
{
    std::cerr << "[ERROR] main: " << message << std::endl;
    exit(1);
}

/**
 * @brief  This is a callback for ISO::parseISO
 * @param  status: the ISOCHOP_ISO_PARSE_STATUS_* code
 * @retval None
 */
void status_callback_parsing(int status)
{
    switch (status)
    {
    case ISOCHOP_ISO_PARSE_STATUS_OPENING_DISC:
        std::cout << "[INFO] load (file): Opening disc..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_READING_SYSTEM_AREA:
        std::cout << "[INFO] load (file): Reading system area..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_READING_VOLUME_DESCRIPTORS:
        std::cout << "[INFO] load (file): Reading volume descriptors..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_READING_EXTENT_TREE:
        std::cout << "[INFO] load (file): Reading extent tree..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_MARKING_SYSTEM_AREA:
        std::cout << "[INFO] load (file): Marking system reserved area..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_MARKING_VOLUME_DESCRIPTORS:
        std::cout << "[INFO] load (file): Marking volume descriptors..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_MARKING_EXTENT_TREE:
        std::cout << "[INFO] load (file): Marking extent tree..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_READING_ORPHANS:
        std::cout << "[INFO] load (file): Reading orphans..." << std::endl;
        break;
    case ISOCHOP_ISO_PARSE_STATUS_DONE:
        std::cout << "[INFO] load (file): Done parsing disc." << std::endl;
        break;
    default:
        std::cerr << "[WARNING] load (file): Unknown status code." << std::endl;
        break;
    }
}

/**
 * @brief  This is a callback for ISO::import_data
 * @param  status: the ISOCHOP_ISO_IMPORT_STATUS_OPENING_* code
 * @retval None
 */
void status_callback_import(int status)
{
    switch (status)
    {
    case ISOCHOP_ISO_IMPORT_STATUS_DONE:
        std::cout << "[INFO] load (directory): Done importing ISO." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_VALIDATE_DIRECTORY:
        std::cout << "[INFO] load (directory): Validating export directory..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_OPENING_MANIFEST:
        std::cout << "[INFO] load (directory): Opening manifest for reading..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_OPENING_ROOT:
        std::cout << "[INFO] load (directory): Opening STRUCTURE_ROOT for reading..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_EXTENT_TREE:
        std::cout << "[INFO] load (directory): Reading extent tree..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_MANIFEST:
        std::cout << "[INFO] load (directory): Reading ISO manifest..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_ORPHANS:
        std::cout << "[INFO] load (directory): Reading orphans..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_ROOT:
        std::cout << "[INFO] load (directory): Reading STRUCTURE_ROOT file..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_SYSTEM_AREA:
        std::cout << "[INFO] load (directory): Reading system reserved area..." << std::endl;
        break;
    case ISOCHOP_ISO_IMPORT_STATUS_READING_VOLUME_DESCRIPTORS:
        std::cout << "[INFO] load (directory): Reading volume descriptors..." << std::endl;
        break;
    default:
        std::cerr << "[WARNING] load (directory): Unknown status code." << std::endl;
        break;
    }
}

/**
 * @brief  This is the driver for the demo code
 * @param  iso: pointer to the ISO object
 * @retval None
 */
void demo(std::string path)
{
    // load the ISO image
    std::cout << "[INFO] Loading ISO image. This may take a while." << std::endl;
    int result;
    isochop::ISOChop iso = isochop::ISOChop(path, status_callback_parsing, status_callback_import, &result);
    if (!iso.good())
    {
        crash(iso.explainLoadResult(result));
    }
    // demo code goes below this line
    std::cout << "[INFO] Gathering volume descripter information..." << std::endl;
    std::cout << iso.showVolumeDescriptors() << std::endl;
}

/**
 * @brief  This is the main driver for the program
 * @note   argv[1] must contain a valid iso file
 * @param  argc number of arguments
 * @param  argv argument array
 * @retval exit code
 */
int main(int argc, char **argv)
{
    // do not run unless a file was defined
    if (argc != 2)
    {
        std::cout << "Usage: readiso FILE" << std::endl;
        return 1;
    }

    // get the path
    std::string path = argv[1];

    // run the demo
    demo(path);

    // stop program
    return 0;
}