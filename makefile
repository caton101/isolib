# NOTE: This makefile just calls the other makefiles. There is nothing important in here.

# directory paths
LIBDIR = ./include
EXDIR = ./examples
TESTDIR = ./test
BUILDDIR = ./bin

# file paths
LIBFILE = $(LIBDIR)/isochop.a
TESTFILE = $(TESTDIR)/test

all:
	make isochop
	make examples
	make test

isochop:
	make -C $(LIBDIR)
	mkdir -p $(BUILDDIR)
	cp $(LIBFILE) $(BUILDDIR)

examples:
	make -C $(EXDIR)
	mkdir -p $(BUILDDIR)
	cp $(EXDIR)/everything $(BUILDDIR)
	cp $(EXDIR)/export $(BUILDDIR)
	cp $(EXDIR)/extentinfo $(BUILDDIR)
	cp $(EXDIR)/import $(BUILDDIR)
	cp $(EXDIR)/tree $(BUILDDIR)
	cp $(EXDIR)/volumedescriptors $(BUILDDIR)

test:
	make -C $(TESTDIR)
	mkdir -p $(BUILDDIR)
	cp $(TESTFILE) $(BUILDDIR)

ci-test:
	make -C $(TESTDIR) run

clean:
	make -C $(LIBDIR) clean
	make -C $(EXDIR) clean
	make -C $(TESTDIR) clean
	rm -rf $(BUILDDIR)

.PHONY: all isochop examples test clean ci-test