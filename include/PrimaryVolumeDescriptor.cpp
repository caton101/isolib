/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "definitions.hpp"
#include "PrimaryVolumeDescriptor.hpp"
#include "VolumeDescriptor.hpp"
#include "utils.hpp"
#include "DecDatetime.hpp"
#include "Directory.hpp"

isochop::PrimaryVolumeDescriptor::PrimaryVolumeDescriptor() : VolumeDescriptor()
{
    // initialize header
    this->setType(ISOCHOP_VD_TYPE_PRIMARY);
    // make string for generation
    std::string str;
    // add system identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setSystemIdentifier(str);
    // add volume identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setVolumeIdentifier(str);
    // add volume set identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setVolumeSetIdentifier(str);
    // add publisher identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setPublisherIdentifier(str);
    // add data preparer identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setDataPreparerIdentifier(str);
    // add application identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setApplicationIdentifier(str);
    // add copyright file identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setCopyrightFileIdentifier(str);
    // add abstract file identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setAbstractFileIdentifier(str);
    // add biblographic file identifier
    str = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        str += 'A' + (i % 26);
    }
    this->setBiblographicFileIdentifier(str);
    // add file structure version
    this->setFileStructureVersion(1);
}

isochop::PrimaryVolumeDescriptor::PrimaryVolumeDescriptor(const std::vector<char> &raw_data, unsigned long address) : VolumeDescriptor(raw_data, address)
{
    // NOTE: the type is derrived from raw_data and does not need to be set
}

isochop::PrimaryVolumeDescriptor::~PrimaryVolumeDescriptor()
{
    // NOTE: Valgrind does not report any leaks at this time
}

bool isochop::PrimaryVolumeDescriptor::isValid()
{
    // make validation flag
    bool valid = true;
    // check if header is valid
    valid &= this->VolumeDescriptor::isValid();
    // check the type
    valid &= this->getType() == ISOCHOP_VD_TYPE_PRIMARY;
    valid &= this->data.at(7) == 0;
    valid &= utils::isStrA(this->getSystemIdentifier(), true);
    valid &= utils::isStrD(this->getVolumeIdentifier(), true);
    valid &= utils::isStrD(this->getVolumeSetIdentifier(), true);
    valid &= utils::isStrA(this->getPublisherIdentifier(), false);
    valid &= utils::isStrA(this->getDataPreparerIdentifier(), false);
    valid &= utils::isStrA(this->getApplicationIdentifier(), false);
    valid &= utils::isStrD(this->getCopyrightFileIdentifier(), false);
    valid &= utils::isStrD(this->getAbstractFileIdentifier(), false);
    valid &= utils::isStrD(this->getBiblographicFileIdentifier(), false);
    valid &= this->getFileStructureVersion() == 1;
    valid &= this->data.at(882) == 0;
    // return the validation flag
    return valid;
}

std::string isochop::PrimaryVolumeDescriptor::toString()
{
    // initialize a string
    std::string str;
    // copy stuff from parent
    str += this->VolumeDescriptor::toString();
    str += '\n';
    // add the system identifier
    str += "System identifier: '";
    str += this->getSystemIdentifier();
    str += "'\n";
    // add the volume identifier
    str += "Volume identifier: '";
    str += this->getVolumeIdentifier();
    str += "'\n";
    // add the volume space size
    str += "Volume space size: ";
    str += std::to_string(this->getVolumeSpaceSize());
    str += "\n";
    // add the volume set size
    str += "Volume set size: ";
    str += std::to_string(this->getVolumeSetSize());
    str += "\n";
    // add the volume sequence number
    str += "Volume sequence number: ";
    str += std::to_string(this->getVolumeSequenceNumber());
    str += "\n";
    // add the logical block size
    str += "Locical block size: ";
    str += std::to_string(this->getLogicalBlockSize());
    str += "\n";
    // add the path table size
    str += "Path table size: ";
    str += std::to_string(this->getPathTableSize());
    str += "\n";
    // add the location of type-l path table
    str += "Location of type-l path table: ";
    str += std::to_string(this->getLocationOfTypeLPathTable());
    str += "\n";
    // add the location of optional type-l path table
    str += "Location of optional type-l path table: ";
    str += std::to_string(this->getLocationOfOptionalTypeLPathTable());
    str += "\n";
    // add the location of type-m path table
    str += "Location of type-m path table: ";
    str += std::to_string(this->getLocationOfTypeMPathTable());
    str += "\n";
    // add the location of optional type-m path table
    str += "Location of optional type-m path table: ";
    str += std::to_string(this->getLocationOfOptionalTypeMPathTable());
    str += "\n";
    // add the root directory entry
    str += "Root Directory Entry: ";
    str += this->getRootDirectoryEntry().toString();
    str += "\n";
    // add the volume set identifier
    str += "Volume set identifier: '";
    str += this->getVolumeSetIdentifier();
    str += "'\n";
    // add the publisher identifier
    str += "Publisher identifier: '";
    str += this->getPublisherIdentifier();
    str += "'\n";
    // add the data preparer identifier
    str += "Data preparer identifier: '";
    str += this->getDataPreparerIdentifier();
    str += "'\n";
    // add the application identifier
    str += "Application identifier: '";
    str += this->getApplicationIdentifier();
    str += "'\n";
    // add the copyright file identifier
    str += "Copyright file identifier: '";
    str += this->getCopyrightFileIdentifier();
    str += "'\n";
    // add the abstract file identifier
    str += "Abstract file identifier: '";
    str += this->getAbstractFileIdentifier();
    str += "'\n";
    // add the biblographic file identifier
    str += "Biblographic file identifier: '";
    str += this->getBiblographicFileIdentifier();
    str += "'\n";
    // add the volume creation date and time
    str += "Volume creation date and time: '";
    str += this->getVolumeCreationDateAndTime().toString();
    str += "'\n";
    // add the volume modification date and time
    str += "Volume modification date and time: '";
    str += this->getVolumeModificationDateAndTime().toString();
    str += "'\n";
    // add the volume expiration date and time
    str += "Volume expiration date and time: '";
    str += this->getVolumeExpirationDateAndTime().toString();
    str += "'\n";
    // add the volume effective date and time
    str += "Volume effective date and time: '";
    str += this->getVolumeEffectiveDateAndTime().toString();
    str += "'\n";
    // add the file structure version
    str += "File structure version: ";
    str += std::to_string(this->getFileStructureVersion());
    // return the string
    return str;
}

isochop::VolumeDescriptor *isochop::PrimaryVolumeDescriptor::clone()
{
    // make container
    PrimaryVolumeDescriptor *pmvd = new PrimaryVolumeDescriptor();
    // deep copy data
    pmvd->setData(this->getData());
    // return a new boot record volume descriptor
    return pmvd;
}

std::string isochop::PrimaryVolumeDescriptor::getSystemIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setSystemIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getVolumeIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setVolumeIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
}

uint32_t isochop::PrimaryVolumeDescriptor::getVolumeSpaceSize()
{
    // decode the volume space size (int32LSB-MSB)
    std::vector<char> tmp(8, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_SPACE_SIZE, 0, 8);
    return utils::decodeInt32LSB_MSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setVolumeSpaceSize(uint32_t size)
{
    // encode the volume space size (int32LSB-MSB)
    std::vector<char> tmp = utils::encodeInt32LSB_MSB(size);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SPACE_SIZE, 8);
}

uint16_t isochop::PrimaryVolumeDescriptor::getVolumeSetSize()
{
    // decode the volume set size (int16LSB-MSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_SET_SIZE, 0, 4);
    return utils::decodeInt16LSB_MSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setVolumeSetSize(uint16_t size)
{
    // encode the volume set size (int16LSB-MSB)
    std::vector<char> tmp = utils::encodeInt16LSB_MSB(size);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_SIZE, 4);
}

uint16_t isochop::PrimaryVolumeDescriptor::getVolumeSequenceNumber()
{
    // decode the volume sequence number (int16LSB-MSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_SEQUENCE_NUMBER, 0, 4);
    return utils::decodeInt16LSB_MSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setVolumeSequenceNumber(uint16_t size)
{
    // encode the volume set size (int16LSB-MSB)
    std::vector<char> tmp = utils::encodeInt16LSB_MSB(size);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SEQUENCE_NUMBER, 4);
}

uint16_t isochop::PrimaryVolumeDescriptor::getLogicalBlockSize()
{
    // decode the logical block size (int16LSB-MSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_LOGICAL_BLOCK_SIZE, 0, 4);
    return utils::decodeInt16LSB_MSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setLogicalBlockSize(uint16_t size)
{
    // encode the volume set size (int16LSB-MSB)
    std::vector<char> tmp = utils::encodeInt16LSB_MSB(size);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_LOGICAL_BLOCK_SIZE, 4);
}

uint32_t isochop::PrimaryVolumeDescriptor::getPathTableSize()
{
    // decode the path table size (int32LSB-MSB)
    std::vector<char> tmp(8, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_PATH_TABLE_SIZE, 0, 8);
    return utils::decodeInt32LSB_MSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setPathTableSize(uint32_t size)
{
    // encode the path table size (int32LSB-MSB)
    std::vector<char> tmp = utils::encodeInt32LSB_MSB(size);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_PATH_TABLE_SIZE, 8);
}

uint32_t isochop::PrimaryVolumeDescriptor::getLocationOfTypeLPathTable()
{
    // decode the location of type L path table (int32LSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_L_PATH_TABLE, 0, 4);
    return utils::decodeInt32LSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setLocationOfTypeLPathTable(uint32_t location)
{
    // encode the location of type L path table (int32LSB)
    std::vector<char> tmp = utils::encodeInt32LSB(location);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_L_PATH_TABLE, 4);
}

uint32_t isochop::PrimaryVolumeDescriptor::getLocationOfOptionalTypeLPathTable()
{
    // decode the location of optional type L path table (int32LSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_L_PATH_TABLE, 0, 4);
    return utils::decodeInt32LSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setLocationOfOptionalTypeLPathTable(uint32_t location)
{
    // encode the location of optional type L path table (int32LSB)
    std::vector<char> tmp = utils::encodeInt32LSB(location);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_L_PATH_TABLE, 4);
}

uint32_t isochop::PrimaryVolumeDescriptor::getLocationOfTypeMPathTable()
{
    // decode the location of type M path table (int32LSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_M_PATH_TABLE, 0, 4);
    return utils::decodeInt32LSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setLocationOfTypeMPathTable(uint32_t location)
{
    // encode the location of type M path table (int32LSB)
    std::vector<char> tmp = utils::encodeInt32LSB(location);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_M_PATH_TABLE, 4);
}

uint32_t isochop::PrimaryVolumeDescriptor::getLocationOfOptionalTypeMPathTable()
{
    // decode the location of optional type M path table (int32LSB)
    std::vector<char> tmp(4, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_M_PATH_TABLE, 0, 4);
    return utils::decodeInt32LSB(tmp);
}

void isochop::PrimaryVolumeDescriptor::setLocationOfOptionalTypeMPathTable(uint32_t location)
{
    // encode the location of optional type M path table (int32LSB)
    std::vector<char> tmp = utils::encodeInt32LSB(location);
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_M_PATH_TABLE, 4);
}

isochop::Directory isochop::PrimaryVolumeDescriptor::getRootDirectoryEntry()
{
    // get the bytes in the directory entry
    std::vector<char> tmp(ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_ROOT_DIRECTORY_ENTRY, 0, ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY);
    // make directory object
    Directory rde = Directory(tmp);
    // return the root directory entry
    return rde;
}

void isochop::PrimaryVolumeDescriptor::setRootDirectoryEntry(Directory entry)
{
    // copy root directory to data vector
    std::vector<char> tmp = entry.getData();
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_ROOT_DIRECTORY_ENTRY, ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY);
}

std::string isochop::PrimaryVolumeDescriptor::getVolumeSetIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setVolumeSetIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getPublisherIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setPublisherIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getDataPreparerIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setDataPreparerIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getApplicationIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setApplicationIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getCopyrightFileIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setCopyrightFileIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getAbstractFileIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setAbstractFileIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
}

std::string isochop::PrimaryVolumeDescriptor::getBiblographicFileIdentifier()
{
    // return extracted string
    return utils::extractStringFromVector(this->data, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
}

void isochop::PrimaryVolumeDescriptor::setBiblographicFileIdentifier(const std::string &id)
{
    // check for errors
    if (id.size() != ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER)
    {
        throw std::invalid_argument("id.size() != ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER");
    }
    // convert to vector and overwrite data
    std::vector<char> temp = utils::extractVectorFromString(id, 0, id.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
}

isochop::DecDatetime isochop::PrimaryVolumeDescriptor::getVolumeCreationDateAndTime()
{
    std::vector<char> tmp(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME, 0, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
    DecDatetime dd = DecDatetime(tmp);
    return dd;
}

void isochop::PrimaryVolumeDescriptor::setVolumeCreationDateAndTime(DecDatetime time)
{
    std::vector<char> tmp = time.getData();
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
}

isochop::DecDatetime isochop::PrimaryVolumeDescriptor::getVolumeModificationDateAndTime()
{
    std::vector<char> tmp(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME, 0, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
    DecDatetime dd = DecDatetime(tmp);
    return dd;
}

void isochop::PrimaryVolumeDescriptor::setVolumeModificationDateAndTime(DecDatetime time)
{
    std::vector<char> tmp = time.getData();
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
}

isochop::DecDatetime isochop::PrimaryVolumeDescriptor::getVolumeExpirationDateAndTime()
{
    std::vector<char> tmp(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME, 0, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
    DecDatetime dd = DecDatetime(tmp);
    return dd;
}

void isochop::PrimaryVolumeDescriptor::setVolumeExpirationDateAndTime(DecDatetime time)
{
    std::vector<char> tmp = time.getData();
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
}

isochop::DecDatetime isochop::PrimaryVolumeDescriptor::getVolumeEffectiveDateAndTime()
{
    std::vector<char> tmp(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME, 0, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
    DecDatetime dd = DecDatetime(tmp);
    return dd;
}

void isochop::PrimaryVolumeDescriptor::setVolumeEffectiveDateAndTime(DecDatetime time)
{
    std::vector<char> tmp = time.getData();
    utils::copyVector(&tmp, &this->data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME, ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
}

uint8_t isochop::PrimaryVolumeDescriptor::getFileStructureVersion()
{
    return this->data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION);
}

void isochop::PrimaryVolumeDescriptor::setFileStructureVersion(uint8_t version)
{
    this->data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = version;
}

std::vector<char> isochop::PrimaryVolumeDescriptor::getApplicationUsed()
{
    std::vector<char> tmp(ISOCHOP_PMVD_SIZE_APPLICATION_USED, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_APPLICATION_USED, 0, ISOCHOP_PMVD_SIZE_APPLICATION_USED);
    return tmp;
}

void isochop::PrimaryVolumeDescriptor::setApplicationUsed(std::vector<char> data)
{
    // check the size
    if (data.size() != ISOCHOP_PMVD_SIZE_APPLICATION_USED)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_PMVD_SIZE_APPLICATION_USED");
    }
    // overwrite data section
    utils::copyVector(&data, &this->data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_USED, ISOCHOP_PMVD_SIZE_APPLICATION_USED);
}

std::vector<char> isochop::PrimaryVolumeDescriptor::getReserved()
{
    std::vector<char> tmp(ISOCHOP_PMVD_SIZE_RESERVED, 0);
    utils::copyVector(&this->data, &tmp, ISOCHOP_PMVD_OFFSET_RESERVED, 0, ISOCHOP_PMVD_SIZE_RESERVED);
    return tmp;
}

void isochop::PrimaryVolumeDescriptor::setReserved(std::vector<char> data)
{
    // check the size
    if (data.size() != ISOCHOP_PMVD_SIZE_RESERVED)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_PMVD_SIZE_RESERVED");
    }
    // overwrite data section
    utils::copyVector(&data, &this->data, 0, ISOCHOP_PMVD_OFFSET_RESERVED, ISOCHOP_PMVD_SIZE_RESERVED);
}
