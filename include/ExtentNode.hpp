/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <fstream>
#include "Directory.hpp"
#include "ExtentData.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements a directory entry (in the form of an extent tree)
     */
    class ExtentNode
    {
    protected:
        // this stores the directory entry
        Directory directory;
        // this stores the extent
        ExtentData extent_data;
        // this stores the starting address
        unsigned long address;
        // this stores any children
        std::vector<ExtentNode> children;

    public:
        /**
         * @brief  The no-args constructor for a ExtentNode object
         * @note   default values may not be valid
         * @retval a new ExtentNode object
         */
        ExtentNode();

        /**
         * @brief  The a small constructor for a ExtentNode object (useful for recursive parsing)
         * @param  directory: the Directory entry
         * @param  address: the starting address
         * @retval a new ExtentNode object
         */
        ExtentNode(const Directory &directory, unsigned long address);

        /**
         * @brief  The normal constructor for a ExtentNode object
         * @param  directory: the Directory entry
         * @param  extent: the extent
         * @param  address: the starting address
         * @param  children: any children ExtentNodes (only of it is a directory)
         * @retval a new ExtentNode object
         */
        ExtentNode(const Directory &directory, const ExtentData &extent_data, unsigned long address, const std::vector<ExtentNode> &children);

        /**
         * @brief  A special constructor which loads an ExtentNode from a file
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of this extent node's manifest file
         * @retval a new ExtentNode object
         */
        ExtentNode(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  The deconstructor for a ExtentNode object
         */
        ~ExtentNode();

        /**
         * @brief  Generates a string summarizing the ExtentNode
         * @note   string output may change format in future versions
         * @retval string representing the ExtentNode
         */
        std::string toString();

        /**
         * @brief  Clone the ExtentNode object
         * @retval a clone of the ExtentNode object
         */
        ExtentNode clone();

        /**
         * @brief  Fetch the starting address field
         * @retval the starting address
         */
        unsigned long getAddress();

        /**
         * @brief  Set the starting address field
         * @param  address: the starting address
         * @retval None
         */
        void setAddress(unsigned long address);

        /**
         * @brief  Fetch the extent field
         * @retval the extent field
         */
        ExtentData getExtentData();

        /**
         * @brief  Set the extent field
         * @param  extent: the extent field
         */
        void setExtentData(const ExtentData &extent_data);

        /**
         * @brief  Fetch the directory field
         * @retval the directory entry
         */
        Directory getDirectoryEntry();

        /**
         * @brief  Set the directory field
         * @param  directory: the directory entry
         */
        void setDirectoryEntry(const Directory &directory);

        /**
         * @brief  Fetch the children vector
         * @retval a vector of child ExtentNodes
         */
        std::vector<ExtentNode> getChildren();

        /**
         * @brief  Set the children vector
         * @param  children: vector of child ExtentNodes
         */
        void setChildren(const std::vector<ExtentNode> &children);

        /**
         * @brief  Recursively populate ExtentNodes
         * @param  file the file buffer
         * @param  parsedAddresses a vector of addresses that have already been parsed
         * @retval None
         */
        void parseData(std::fstream *file, std::vector<uint64_t> *parsedAddresses);

        /**
         * @brief  Recursively write ExtentNodes to a file
         * @param  file: the file buffer
         * @retval None
         */
        void writeData(std::fstream *file);

        /**
         * @brief  Return a tree of all the extents and their children
         * @note   tree generation subject to change at any time
         * @param  indentLevel: the number indentions to apply to all entries
         * @param  indentSpaces: the number of spaces per indention level
         * @retval a tree of all the extents and their children
         */
        std::string tree(size_t indentLevel, size_t indentSpaces);

        /**
         * @brief  Recursively export extent nodes
         * @param  dirpath: the directory path where all exported files will be stored
         * @retval the SHA256 checksum of this ExtentNode's manifest
         */
        std::string export_data(const std::string &dirpath);

        /**
         * @brief  Recursively import extent nodes
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of this extent node's manifest file
         * @retval None
         */
        void import_data(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  Take the bit vector and mark the byte addresses covered by this object
         * @note   must be big enough to contain the address space
         * @param  bits: the vector of bits
         * @retval None
         */
        void markAddresses(std::vector<bool> *bits);
    };
}
