/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <fstream>
#include "ExtentNode.hpp"
#include "PrimaryVolumeDescriptor.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements an ExtentTree entry
     */
    class ExtentTree
    {
    protected:
        // this stores the root directory
        ExtentNode root;

    public:
        /**
         * @brief  The no-args constructor for an ExtentTree object
         * @note   default values may not be valid
         * @retval a new ExtentTree object
         */
        ExtentTree();

        /**
         * @brief  Construct an ExtentTree object from an ExtentNode
         * @param  root: the root file node
         * @retval a new ExtentTree object
         */
        ExtentTree(const ExtentNode &root);

        /**
         * @brief  Construct an ExtentTree object from a PrimaryVolumeDescriptor
         * @param  pmvd: a primary volume descriptor
         * @retval a new ExtentTree object
         */
        ExtentTree(PrimaryVolumeDescriptor pmvd);

        /**
         * @brief   Construct an ExtentTree object from a directory structure
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of this ExtentTree's manifest file
         * @retval None
         */
        ExtentTree(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  The deconstructor for an ExtentTree object
         */
        ~ExtentTree();

        /**
         * @brief  Generates a string summarizing the ExtentTree
         * @note   string output may change format in future versions
         * @retval string representing the ExtentTree
         */
        std::string toString();

        /**
         * @brief  Return a deep copy of the ExtentTree object
         * @note   this is recursive and may take a while
         * @retval a deep copy of the ExtentTree object
         */
        ExtentTree clone();

        /**
         * @brief  Fetch the root directory information
         * @retval the root directory
         */
        ExtentNode getRoot();

        /**
         * @brief  Set the root directory information
         * @param  root: root directory information
         */
        void setRoot(ExtentNode root);

        /**
         * @brief  Recursively populate ExtentNodes
         * @param  file the file buffer
         * @retval None
         */
        void parseData(std::fstream *file);

        /**
         * @brief  Recursively write ExtentNodes to a file
         * @param  file: the file buffer
         * @retval None
         */
        void writeData(std::fstream *file);

        /**
         * @brief  Return a string representing the directory tree
         * @note   string output may change format in future versions
         * @param  indentSpaces the number of spaces per indentation level
         * @retval string representing the directory tree
         */
        std::string tree(size_t indentSpaces);

        /**
         * @brief  Export the entire extent tree
         * @param  dirpath: the directory path where all exported files will be stored
         * @retval the SHA256 checksum of this ExtentTree's manifest
         */
        std::string export_data(const std::string &dirpath);

        /**
         * @brief  Import the entire extent tree from a directory structure
         * @param  dirpath: the directory path where all exported files are stored
         * @param  manifesthash: the hash of this ExtentTree's manifest file
         * @retval None
         */
        void import_data(const std::string &dirpath, const std::string &manifesthash);

        /**
         * @brief  Take the bit vector and mark the byte addresses covered by this object
         * @note   must be big enough to contain the address space
         * @param  bits: the vector of bits
         * @retval None
         */
        void markAddresses(std::vector<bool> *bits);
    };
}
