/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <cstdint>
#include "definitions.hpp"
#include "VolumeDescriptor.hpp"
#include "DecDatetime.hpp"
#include "Directory.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class represents a Primary Volume Descriptor
     */
    class PrimaryVolumeDescriptor : public VolumeDescriptor
    {
    public:
        /**
         * @brief  Initialize the Primary Volume Descriptor with dummy values.
         * @note   dummy values may not be legitimate
         * @retval None
         */
        PrimaryVolumeDescriptor();

        /**
         * @brief  Initialize the Primary Volume Descriptor with parsed data.
         * @note   char array must be same size as VD_TOTAL_SIZE
         * @param  raw_data byte array to parse
         * @param  address address of the first byte
         * @retval None
         */
        PrimaryVolumeDescriptor(const std::vector<char> &raw_data, unsigned long address);

        /**
         * @brief  This frees all memory from a Primary Volume Descriptor object
         * @note   relies on the parent to free inherited fields
         * @retval None
         */
        ~PrimaryVolumeDescriptor();

        /**
         * @brief  Check if volume descriptor contains valid data
         * @note   only checks type, identifier, and version
         * @retval true if volume descriptor is valid
         */
        bool isValid();

        /**
         * @brief  Generates a string with volume descriptor information
         * @retval a string with volume descriptor information
         */
        std::string toString();

        /**
         * @brief  This clones the primary volume descriptor
         * @note   this is a deep copy
         * @retval a clone of the primary volume descriptor
         */
        VolumeDescriptor *clone();

        /**
         * @brief  Fetch the system identifier
         * @retval the system identifier
         */
        std::string getSystemIdentifier();

        /**
         * @brief  Set the system identifier
         * @note   id must be of size PMVD_SIZE_SYSTEM_IDENTIFIER
         * @param  id: new system identifier
         */
        void setSystemIdentifier(const std::string &id);

        /**
         * @brief  Fetch the volume identifier
         * @retval the volume identifier
         */
        std::string getVolumeIdentifier();

        /**
         * @brief  Set the volume identifier
         * @note   id must be of size PMVD_SIZE_VOLUME_IDENTIFIER
         * @param  id: new volume identifier
         */
        void setVolumeIdentifier(const std::string &id);

        /**
         * @brief  Fetch the volume space size
         * @retval the volume space size
         */
        uint32_t getVolumeSpaceSize();

        /**
         * @brief  Set the volume space size
         * @param  size: new volume space size
         */
        void setVolumeSpaceSize(uint32_t size);

        /**
         * @brief  Fetch the volume set size
         * @retval the volume set size
         */
        uint16_t getVolumeSetSize();

        /**
         * @brief  Set the volume set size
         * @param  size: new volume set size
         */
        void setVolumeSetSize(uint16_t size);

        /**
         * @brief  Fetch the volume space size
         * @retval the volume space size
         */
        uint16_t getVolumeSequenceNumber();

        /**
         * @brief  Set the volume sequence number
         * @param  size: new volume sequence number
         */
        void setVolumeSequenceNumber(uint16_t size);

        /**
         * @brief  Fetch the logical block size
         * @retval the logical block size
         */
        uint16_t getLogicalBlockSize();

        /**
         * @brief  Set the logical block size
         * @param  size: new logical block size
         */
        void setLogicalBlockSize(uint16_t size);

        /**
         * @brief  Fetch the path table size
         * @retval the path table size
         */
        uint32_t getPathTableSize();

        /**
         * @brief  Set the path table size
         * @param  size: new path table size
         */
        void setPathTableSize(uint32_t size);

        /**
         * @brief  Fetch the location of the type L path table
         * @retval the location of the type L path table
         */
        uint32_t getLocationOfTypeLPathTable();

        /**
         * @brief  Set the new location of the type L path table
         * @param  location: new location of the type L path table
         */
        void setLocationOfTypeLPathTable(uint32_t location);

        /**
         * @brief  Fetch the location of the optional type L path table
         * @retval the location of the optional type L path table
         */
        uint32_t getLocationOfOptionalTypeLPathTable();

        /**
         * @brief  Set the new location of the optional type L path table
         * @param  location: new location of the optional type L path table
         */
        void setLocationOfOptionalTypeLPathTable(uint32_t location);

        /**
         * @brief  Fetch the location of the type M path table
         * @retval the location of the type M path table
         */
        uint32_t getLocationOfTypeMPathTable();

        /**
         * @brief  Set the new location of the type M path table
         * @param  location: new location of the type M path table
         */
        void setLocationOfTypeMPathTable(uint32_t location);

        /**
         * @brief  Fetch the location of the optional type M path table
         * @retval the location of the optional type M path table
         */
        uint32_t getLocationOfOptionalTypeMPathTable();

        /**
         * @brief  Set the new location of the optional type M path table
         * @param  location: new location of the optional type M path table
         */
        void setLocationOfOptionalTypeMPathTable(uint32_t location);

        /**
         * @brief  Get the root directory entry
         * @retval the root directory entry
         */
        Directory getRootDirectoryEntry();

        /**
         * @brief  Set the root directory entry
         * @param  entry: new root directory entry
         */
        void setRootDirectoryEntry(Directory entry);

        /**
         * @brief  Get the volume set identifier
         * @retval the volume set identifier
         */
        std::string getVolumeSetIdentifier();

        /**
         * @brief  Set the volume set identifier
         * @note   id must be of size PMVD_SIZE_VOLUME_SET_IDENTIFIER
         * @param  id: new volume set identifier
         */
        void setVolumeSetIdentifier(const std::string &id);

        /**
         * @brief  Get the publisher identifier
         * @retval the publisher identifier
         */
        std::string getPublisherIdentifier();

        /**
         * @brief  Set the publisher identifier
         * @note   id must be of size PMVD_SIZE_PUBLISHER_IDENTIFIER
         * @param  id: new publisher identifier
         */
        void setPublisherIdentifier(const std::string &id);

        /**
         * @brief  Get the data preparer identifier
         * @retval the data preparer identifier
         */
        std::string getDataPreparerIdentifier();

        /**
         * @brief  Set the data preparer identifier
         * @note   id must be of size PMVD_SIZE_DATA_PREPARER_IDENTIFIER
         * @param  id: new data preparer identifier
         */
        void setDataPreparerIdentifier(const std::string &id);

        /**
         * @brief  Get the application identifier
         * @retval the application identifier
         */
        std::string getApplicationIdentifier();

        /**
         * @brief  Set the application identifier
         * @note   id must be of size PMVD_SIZE_APPLICATION_IDENTIFIER
         * @param  id: new application identifier
         */
        void setApplicationIdentifier(const std::string &id);

        /**
         * @brief  Get the copyright file identifier
         * @retval the copyright file identifier
         */
        std::string getCopyrightFileIdentifier();

        /**
         * @brief  Set the copyright file identifier
         * @note   id must be of size PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER
         * @param  id: new copyright file identifier
         */
        void setCopyrightFileIdentifier(const std::string &id);

        /**
         * @brief  Get the abstract file identifier
         * @retval the abstract file identifier
         */
        std::string getAbstractFileIdentifier();

        /**
         * @brief  Set the abstract file identifier
         * @note   id must be of size PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER
         * @param  id: new abstract file identifier
         */
        void setAbstractFileIdentifier(const std::string &id);

        /**
         * @brief  Get the biblographic file identifier
         * @retval the biblographic file identifier
         */
        std::string getBiblographicFileIdentifier();

        /**
         * @brief  Set the biblographic file identifier
         * @note   id must be of size PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER
         * @param  id: new biblographic file identifier
         */
        void setBiblographicFileIdentifier(const std::string &id);

        /**
         * @brief  Fetch the volume creation date and time
         * @retval the volume creation date and time
         */
        DecDatetime getVolumeCreationDateAndTime();

        /**
         * @brief  Set the volume creation date and time
         * @param  time: new volume creation date and time
         */
        void setVolumeCreationDateAndTime(DecDatetime time);

        /**
         * @brief  Fetch the volume modification date and time
         * @retval the volume modification date and time
         */
        DecDatetime getVolumeModificationDateAndTime();

        /**
         * @brief  Set the volume modification date and time
         * @param  time: new volume modification date and time
         */
        void setVolumeModificationDateAndTime(DecDatetime time);

        /**
         * @brief  Fetch the volume expiration date and time
         * @retval the volume expiration date and time
         */
        DecDatetime getVolumeExpirationDateAndTime();

        /**
         * @brief  Set the volume expiration date and time
         * @param  time: new volume expiration date and time
         */
        void setVolumeExpirationDateAndTime(DecDatetime time);

        /**
         * @brief  Fetch the volume effective date and time
         * @retval the volume effective date and time
         */
        DecDatetime getVolumeEffectiveDateAndTime();

        /**
         * @brief  Set the volume effective date and time
         * @param  time: new volume effective date and time
         */
        void setVolumeEffectiveDateAndTime(DecDatetime time);

        /**
         * @brief  Fetch the file structure version
         * @retval the file structure version
         */
        uint8_t getFileStructureVersion();

        /**
         * @brief  Set the file structure version
         * @param  version: new file structure version
         */
        void setFileStructureVersion(uint8_t version);

        /**
         * @brief  Fetch the application used area
         * @retval the application used area
         */
        std::vector<char> getApplicationUsed();

        /**
         * @brief  Set the application used area
         * @note   data must be of size PMVD_SIZE_APPLICATION_USED
         * @param  data: new application used area
         */
        void setApplicationUsed(std::vector<char> data);

        /**
         * @brief  Fetch the reserved area
         * @retval the reserved area
         */
        std::vector<char> getReserved();

        /**
         * @brief  Set the reserved area
         * @note   data must be of size PMVD_OFFSET_RESERVED
         * @param  data: new reserved area
         */
        void setReserved(std::vector<char> data);
    };
}
