/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdint>
#include <openssl/sha.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "utils.hpp"
#include "definitions.hpp"

void isochop::utils::copyBytes(void *origin, void *destination, int size)
{
    // keep copying bytes until finished
    for (int offset = 0; offset < size; offset++)
    {
        // NOTE: assuming a char is 1 byte and size is valid
        ((char *)destination)[offset] = ((char *)origin)[offset];
    }
}

void isochop::utils::copyVector(std::vector<char> *origin, std::vector<char> *destination, std::size_t origin_start, std::size_t destination_start, std::size_t size)
{
    // check for errors
    if (!origin)
    {
        throw std::invalid_argument("origin can not be void");
    }
    if (!destination)
    {
        throw std::invalid_argument("origin can not be void");
    }
    if ((origin_start + size) > origin->size())
    {
        throw std::invalid_argument("size+start is too large for origin");
    }
    if ((destination_start + size) > destination->size())
    {
        throw std::invalid_argument("size+start is too large for destination");
    }
    // perform copy
    for (std::size_t i = 0; i < size; i++)
    {
        destination->at(i + destination_start) = origin->at(i + origin_start);
    }
}

std::string isochop::utils::extractStringFromVector(std::vector<char> vec, std::size_t offset, std::size_t size)
{
    // check for errors
    if (size < 0)
    {
        throw std::invalid_argument("size can not be negative");
    }
    if (offset < 0)
    {
        throw std::invalid_argument("offset can not be negative");
    }
    if ((offset + size) > vec.size())
    {
        throw std::invalid_argument("size+offset is too large for vec");
    }
    // perform copy
    std::string str;
    for (std::size_t i = 0; i < size; i++)
    {
        str += vec[i + offset];
    }
    // return the string
    return str;
}

std::vector<char> isochop::utils::extractVectorFromString(std::string str, std::size_t offset, std::size_t size)
{
    // check for errors
    if (size < 0)
    {
        throw std::invalid_argument("size can not be negative");
    }
    if (offset < 0)
    {
        throw std::invalid_argument("offset can not be negative");
    }
    if (offset + size > str.size())
    {
        throw std::invalid_argument("size+offset is too large for string");
    }
    // copy string to vector
    std::vector<char> vec;
    for (std::size_t i = 0; i < size; i++)
    {
        vec.push_back(str.at(i + offset));
    }
    // return the vector
    return vec;
}

bool isochop::utils::isStrA(const std::string &str, bool allowZero)
{
    /*
     * a-characters:
     *   A B C D E F G H I J K L M
     *   N O P Q R S T U V W X Y Z
     *   0 1 2 3 4 5 6 7 8 9 _ ! "
     *   % & ' ( ) * + , - . / : ;
     *   < = > ?
     */
    std::string allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_!\"%&'()*+,-./:;<=>? ";
    for (char c : str)
    {
        // check for invalid pattern
        if (allowZero && c == '\0')
        {
            continue;
        }
        if (allowed.find(c) == std::string::npos)
        {
            return false;
        }
    }
    return true;
}

bool isochop::utils::isStrD(const std::string &str, bool allowZero)
{
    /*
     * d-characters:
     *   A B C D E F G H I J K L M
     *   N O P Q R S T U V W X Y Z
     *   0 1 2 3 4 5 6 7 8 9 _
     */
    std::string allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ";
    for (char c : str)
    {
        // check for invalid pattern
        if (allowZero && c == '\0')
        {
            continue;
        }
        if (allowed.find(c) == std::string::npos)
        {
            return false;
        }
    }
    return true;
}

uint16_t isochop::utils::decodeInt16LSB(const std::vector<char> &bytes)
{
    // ensure there are 2 bytes
    if (bytes.size() != 2)
    {
        throw std::invalid_argument("bytes.size() != 2");
    }
    // decode and return
    // Note: the human number 15 would be 51 in LSB
    uint16_t total = (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

uint16_t isochop::utils::decodeInt16MSB(const std::vector<char> &bytes)
{
    // ensure there are 2 bytes
    if (bytes.size() != 2)
    {
        throw std::invalid_argument("bytes.size() != 2");
    }
    // decode and return
    // Note: the human number 15 would be 15 in MSB
    uint16_t total = (uint8_t)bytes.at(0) << 8;
    total += (uint8_t)bytes.at(1);
    return total;
}

uint16_t isochop::utils::decodeInt16LSB_MSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // ensure the byte order is flipped correctly
    if (bytes.at(0) != bytes.at(3) || bytes.at(1) != bytes.at(2))
    {
        throw std::invalid_argument("number is not LSB-MSB encoded");
    }
    // read the LSB one
    uint16_t total = (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

int16_t isochop::utils::decodeSint16LSB(const std::vector<char> &bytes)
{
    // ensure there are 2 bytes
    if (bytes.size() != 2)
    {
        throw std::invalid_argument("bytes.size() != 2");
    }
    // decode and return
    // Note: the human number 15 would be 51 in LSB
    int16_t total = (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

int16_t isochop::utils::decodeSint16MSB(const std::vector<char> &bytes)
{
    // ensure there are 2 bytes
    if (bytes.size() != 2)
    {
        throw std::invalid_argument("bytes.size() != 2");
    }
    // decode and return
    // Note: the human number 15 would be 15 in MSB
    int16_t total = (uint8_t)bytes.at(0) << 8;
    total += (uint8_t)bytes.at(1);
    return total;
}

int16_t isochop::utils::decodeSint16LSB_MSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // ensure the byte order is flipped correctly
    if (bytes.at(0) != bytes.at(3) || bytes.at(1) != bytes.at(2))
    {
        throw std::invalid_argument("number is not LSB-MSB encoded");
    }
    // read the MSB one since it is easier to understand
    int16_t total = (uint8_t)bytes.at(2) << 8;
    total += (uint8_t)bytes.at(3);
    return total;
}

uint32_t isochop::utils::decodeInt32LSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // Note: the human number 1234 would be 4321 in LSB
    uint32_t total = (uint8_t)bytes.at(3) << 24;
    total += (uint8_t)bytes.at(2) << 16;
    total += (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

uint32_t isochop::utils::decodeInt32MSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // Note: the human number 1234 would be 1234 in MSB
    uint32_t total = (uint8_t)bytes.at(0) << 24;
    total += (uint8_t)bytes.at(1) << 16;
    total += (uint8_t)bytes.at(2) << 8;
    total += (uint8_t)bytes.at(3);
    return total;
}

uint32_t isochop::utils::decodeInt32LSB_MSB(const std::vector<char> &bytes)
{
    // ensure there are 8 bytes
    if (bytes.size() != 8)
    {
        throw std::invalid_argument("bytes.size() != 8");
    }
    // ensure the byte order is flipped correctly
    if (
        bytes.at(0) != bytes.at(7) || bytes.at(1) != bytes.at(6) || bytes.at(2) != bytes.at(5) || bytes.at(3) != bytes.at(4))
    {
        throw std::invalid_argument("number is not LSB-MSB encoded");
    }
    // read the LSB one
    uint32_t total = (uint8_t)bytes.at(3) << 24;
    total += (uint8_t)bytes.at(2) << 16;
    total += (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

int32_t isochop::utils::decodeSint32LSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // Note: the human number 1234 would be 4321 in LSB
    int32_t total = (uint8_t)bytes.at(3) << 24;
    total += (uint8_t)bytes.at(2) << 16;
    total += (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

int32_t isochop::utils::decodeSint32MSB(const std::vector<char> &bytes)
{
    // ensure there are 4 bytes
    if (bytes.size() != 4)
    {
        throw std::invalid_argument("bytes.size() != 4");
    }
    // Note: the human number 1234 would be 1234 in MSB
    int32_t total = (uint8_t)bytes.at(0) << 24;
    total += (uint8_t)bytes.at(1) << 16;
    total += (uint8_t)bytes.at(2) << 8;
    total += (uint8_t)bytes.at(3);
    return total;
}

int32_t isochop::utils::decodeSint32LSB_MSB(const std::vector<char> &bytes)
{
    // ensure there are 8 bytes
    if (bytes.size() != 8)
    {
        throw std::invalid_argument("bytes.size() != 8");
    }
    // ensure the byte order is flipped correctly
    if (
        bytes.at(0) != bytes.at(7) || bytes.at(1) != bytes.at(6) || bytes.at(2) != bytes.at(5) || bytes.at(3) != bytes.at(4))
    {
        throw std::invalid_argument("number is not LSB-MSB encoded");
    }
    // read the LSB one
    int32_t total = (uint8_t)bytes.at(3) << 24;
    total += (uint8_t)bytes.at(2) << 16;
    total += (uint8_t)bytes.at(1) << 8;
    total += (uint8_t)bytes.at(0);
    return total;
}

/**
 * @brief  This encodes a byte vector from an int16_LSB encoded number
 * @note   int16_LSB means unsigned 16-bit little-endian integer
 * @param  number: a uint16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt16LSB(uint16_t number)
{
    // Note: the human number 15 would be 51 in LSB
    std::vector<char> data(2, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an int16_MSB encoded number
 * @note   int16_MSB means unsigned 16-bit big-endian integer
 * @param  number: a uint16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt16MSB(uint16_t number)
{
    // Note: the human number 15 would be 15 in MSB
    std::vector<char> data(2, 0);
    data.at(0) = (number >> 8) & 0xFF;
    data.at(1) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an int16_LSB-MSB encoded number
 * @note   int16_LSB-MSB means unsigned 16-bit both-endian integer (little then big)
 * @param  number: a uint16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt16LSB_MSB(uint16_t number)
{
    // Note: the human number 15 would be 5115 in LSB-MSB
    std::vector<char> data(4, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 8) & 0xFF;
    data.at(3) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint16_LSB encoded number
 * @note   sint16_LSB means signed 16-bit little-endian integer
 * @param  number: an int16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint16LSB(int16_t number)
{
    // Note: the human number 15 would be 51 in LSB
    std::vector<char> data(2, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint16_MSB encoded number
 * @note   sint16_MSB means signed 16-bit big-endian integer
 * @param  number: an int16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint16MSB(int16_t number)
{
    // Note: the human number 15 would be 15 in MSB
    std::vector<char> data(2, 0);
    data.at(0) = (number >> 8) & 0xFF;
    data.at(1) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint16_LSB-MSB encoded number
 * @note   sint16_LSB-MSB means signed 16-bit both-endian integer (little then big)
 * @param  number: an int16_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint16LSB_MSB(int16_t number)
{
    // Note: the human number 15 would be 5115 in LSB-MSB
    std::vector<char> data(4, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 8) & 0xFF;
    data.at(3) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an int32_LSB encoded number
 * @note   int32_LSB means unsigned 32-bit little-endian integer
 * @param  number: a uint32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt32LSB(uint32_t number)
{
    // Note: the human number 1234 would be 4321 in LSB
    std::vector<char> data(4, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 16) & 0xFF;
    data.at(3) = (number >> 24) & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an int32_MSB encoded number
 * @note   int32_MSB means unsigned 32-bit big-endian integer
 * @param  number: a uint32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt32MSB(uint32_t number)
{
    // Note: the human number 1234 would be 1234 in MSB
    std::vector<char> data(4, 0);
    data.at(0) = (number >> 24) & 0xFF;
    data.at(1) = (number >> 16) & 0xFF;
    data.at(2) = (number >> 8) & 0xFF;
    data.at(3) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an int32_LSB-MSB encoded number
 * @note   int32_LSB-MSB means unsigned 32-bit both-endian integer (little then big)
 * @param  number: a uint32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeInt32LSB_MSB(uint32_t number)
{
    // Note: the human number 1234 would be 43211234 in LSB-MSB
    std::vector<char> data(8, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 16) & 0xFF;
    data.at(3) = (number >> 24) & 0xFF;
    data.at(4) = (number >> 24) & 0xFF;
    data.at(5) = (number >> 16) & 0xFF;
    data.at(6) = (number >> 8) & 0xFF;
    data.at(7) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint32_LSB encoded number
 * @note   sint32_LSB means signed 32-bit little-endian integer
 * @param  number: an int32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint32LSB(int32_t number)
{
    // Note: the human number 1234 would be 4321 in LSB
    std::vector<char> data(4, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 16) & 0xFF;
    data.at(3) = (number >> 24) & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint32_MSB encoded number
 * @note   sint32_MSB means signed 32-bit big-endian integer
 * @param  number: an int32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint32MSB(int32_t number)
{
    // Note: the human number 1234 would be 1234 in MSB
    std::vector<char> data(4, 0);
    data.at(0) = (number >> 24) & 0xFF;
    data.at(1) = (number >> 16) & 0xFF;
    data.at(2) = (number >> 8) & 0xFF;
    data.at(3) = number & 0xFF;
    return data;
}

/**
 * @brief  This encodes a byte vector from an sint32_LSB-MSB encoded number
 * @note   sint32_LSB-MSB means signed 32-bit both-endian integer (little then big)
 * @param  number: an int32_t number
 * @retval a byte vector
 */
std::vector<char> isochop::utils::encodeSint32LSB_MSB(int32_t number)
{
    // Note: the human number 1234 would be 43211234 in LSB-MSB
    std::vector<char> data(8, 0);
    data.at(0) = number & 0xFF;
    data.at(1) = (number >> 8) & 0xFF;
    data.at(2) = (number >> 16) & 0xFF;
    data.at(3) = (number >> 24) & 0xFF;
    data.at(4) = (number >> 24) & 0xFF;
    data.at(5) = (number >> 16) & 0xFF;
    data.at(6) = (number >> 8) & 0xFF;
    data.at(7) = number & 0xFF;
    return data;
}

bool isochop::utils::getBit(uint8_t byte, uint8_t pos)
{
    if (pos > 7)
    {
        throw std::invalid_argument("pos > 7");
    }
    return (byte >> pos) & 1;
}

uint8_t isochop::utils::setBit(uint8_t byte, uint8_t pos, bool bit)
{
    if (pos > 7)
    {
        throw std::invalid_argument("pos > 7");
    }
    return (bit << pos) | (byte & ~(1 << pos));
}

uint32_t isochop::utils::convertSectorToByteAddress(uint32_t sector)
{
    return sector * ISOCHOP_SECTOR_SIZE;
}

uint32_t isochop::utils::convertByteAddressToSector(uint32_t address)
{
    return address / ISOCHOP_SECTOR_SIZE;
}

std::string isochop::utils::sha256sum(std::vector<char> *vec)
{
    // NOTE: This code is modified from https://stackoverflow.com/a/10632725
    // ensure vec is not null
    if (vec == nullptr)
    {
        throw std::invalid_argument("vec == null");
    }
    // perform the digest
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    for (char c : *vec)
    {
        SHA256_Update(&sha256, &c, 1);
    }
    SHA256_Final(hash, &sha256);
    std::stringstream ret;
    for (int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        ret << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ret.str();
}

void isochop::utils::dump_vector(std::vector<char> *vec, std::string path, bool *error)
{
    // clear the error flag
    if (error != nullptr)
    {
        *error = false;
    }
    // ensure vector is valid
    if (vec == nullptr)
    {
        if (error != nullptr)
        {
            *error = true;
        }
        return;
    }
    // open the file for writing
    std::fstream file;
    file.open(path, std::fstream::out | std::fstream::binary);
    if (!file.good())
    {
        if (error != nullptr)
        {
            *error = true;
        }
        return;
    }
    // write the output data
    for (char c : *vec)
    {
        file.write(&c, 1);
    }
    // close the file
    file.close();
}

std::vector<char> isochop::utils::load_vector(std::string path, bool *error)
{
    // clear the error flag
    if (error != nullptr)
    {
        *error = false;
    }
    // allocate vector for reading
    std::vector<char> buffer;
    // open the file for reading
    std::fstream file;
    file.open(path, std::fstream::in | std::fstream::binary);
    if (!file.good())
    {
        if (error != nullptr)
        {
            *error = true;
        }
        return buffer;
    }
    // read the file
    while (file.peek() != std::fstream::traits_type::eof())
    {
        char c;
        file.read(&c, 1);
        buffer.push_back(c);
    }
    // return the buffer
    return buffer;
}
