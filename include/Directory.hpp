/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include "DirDatetime.hpp"
#include "FileFlags.hpp"

// put code behind library namespace
namespace isochop
{
    class Directory
    {
    private:
        /**
         * @brief  This calculates the starting address of the system use area
         * @note   will be same as directory length if it does not exist
         * @retval starting address for system use area
         */
        std::size_t calcSystemUseAddress();

    protected:
        // this holds the raw directory information
        std::vector<char> data;

    public:
        /**
         * @brief  This makes a directory object with default values
         * @note   object may not be valid
         * @retval a new Directory object
         */
        Directory();

        /**
         * @brief  This makes a directory object and initializes it with predefined data
         * @note   data size must match the length field
         * @param  data: the data to use
         * @retval a new Directory object
         */
        Directory(const std::vector<char> &data);

        /**
         * @brief  This deconstructs a Directory object
         * @note   object is not usable after calling
         */
        ~Directory();

        /**
         * @brief  This checks a directory object is valid
         * @retval true of the directory data is valid
         */
        bool isValid();

        /**
         * @brief  This generates a string representing the directory object
         * @note   do not trust the formatting of the output string
         * @retval a string representing the directory object
         */
        std::string toString();

        /**
         * @brief  This clones the directory object
         * @retval a clone of the directory object
         */
        Directory clone();

        /**
         * @brief  This fetches the raw data array used by the directory object
         * @retval the raw data array used by the directory object
         */
        std::vector<char> getData();

        /**
         * @brief  This sets the raw data array to be used by the directory object
         * @note   data size must match the length field
         * @param  data: the data to be used by the directory object
         * @retval None
         */
        void setData(const std::vector<char> &data);

        /**
         * @brief  This fetches the length of the directory record
         * @retval length of the directory record
         */
        uint8_t getDirectoryRecordLength();

        /**
         * @brief  This sets the length of the directory record
         * @param  length: new length of the directory record
         */
        void setDirectoryRecordLength(uint8_t length);

        /**
         * @brief  This fetches the length of the extended attribute record
         * @retval length of the extended attribute record
         */
        uint8_t getExtendedAttributeRecordLength();

        /**
         * @brief  This sets the length of the extended attribute record
         * @param  length: new length of the extended attribute record
         */
        void setExtendedAttributeRecordLength(uint8_t length);

        /**
         * @brief  This fetches the location of the extent
         * @retval location of the extent
         */
        uint32_t getLocationOfExtent();

        /**
         * @brief  This sets the location of the extent
         * @param  location: new location of the extent
         */
        void setLocationOfExtent(uint32_t location);

        /**
         * @brief  This fetches the length of the data
         * @retval the length of the file
         */
        uint32_t getDataLength();

        /**
         * @brief  This sets the length of the file
         * @param  length: new length of the file
         */
        void setDataLength(uint32_t length);

        /**
         * @brief  This fetches the date and time of recording
         * @retval date and time of recording
         */
        DirDatetime getRecordingDateAndTime();

        /**
         * @brief  This sets the date and time of recording
         * @param  timestamp: new date and time of recording
         */
        void setRecordingDateAndTime(DirDatetime timestamp);

        /**
         * @brief  This fetches the file flags
         * @retval the file flags
         */
        FileFlags getFileFlags();

        /**
         * @brief  This sets the file flags
         * @param  flags: the file flags
         */
        void setFileFlags(FileFlags flags);

        /**
         * @brief  This fetches the file unit size
         * @note   0 if not recorded in interleaved mode
         * @retval file unit size
         */
        uint8_t getFileUnitSize();

        /**
         * @brief  This sets the file unit size
         * @note   should be 0 if not recorded in interleaved mode
         * @param  size: file unit size
         */
        void setFileUnitSize(uint8_t size);

        /**
         * @brief  This fetches the interleave gap size
         * @note   0 if not recorded in interleaved mode
         * @retval interleave gap size
         */
        uint8_t getInterleaveGapSize();

        /**
         * @brief  This sets the interleave gap size
         * @note   should be 0 if not recorded in interleaved mode
         * @param  size: new interleave gap size
         * @retval None
         */
        void setInterleaveGapSize(uint8_t size);

        /**
         * @brief  This fetches the volume sequence number
         * @retval the volume sequence number
         */
        uint16_t getVolumeSequenceNumber();

        /**
         * @brief  This sets the volume sequence number
         * @param  number: new volume sequence number
         */
        void setVolumeSequenceNumber(uint16_t number);

        /**
         * @brief  This fetches the file identifier length
         * @retval the file identifier length
         */
        uint8_t getFileIdentifierLength();

        /**
         * @brief  This sets the file identifier length
         * @note   this will grow or shrink the file identifier
         * @param  length: new file identifier length
         */
        void setFileIdentifierLength(uint8_t length);

        /**
         * @brief  This fetches the file identifier
         * @retval the file identifier
         */
        std::string getFileIdentifier();

        /**
         * @brief  This sets the file identifier
         * @note   this will update the file identifier length
         * @param  id: new file identifier
         */
        void setFileIdentifier(const std::string &id);

        /**
         * @brief  This fetches the system use area
         * @retval the system use area
         */
        std::vector<char> getSystemUse();

        /**
         * @brief  This sets the system use area
         * @note   size must not grow the directory length above 255
         * @param  data: new system use area
         */
        void setSystemUse(std::vector<char> data);
    };
}
