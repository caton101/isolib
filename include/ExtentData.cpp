/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include "ExtentData.hpp"

isochop::ExtentData::ExtentData()
{
    // make data vector
    std::vector<char> data(10, 0);
    this->setData(data);
}

isochop::ExtentData::ExtentData(const std::vector<char> &data)
{
    // set the data vector
    this->setData(data);
}

isochop::ExtentData::~ExtentData()
{
    // valgrind does not report any leaks at this time
}

std::string isochop::ExtentData::toString()
{
    std::string str;
    str += "{Size: ";
    str += std::to_string(this->getData().size());
    str += "}";
    return str;
}

isochop::ExtentData isochop::ExtentData::clone()
{
    ExtentData ext;
    ext.setData(this->data);
    return ext;
}

std::vector<char> isochop::ExtentData::getData()
{
    return this->data;
}

void isochop::ExtentData::setData(const std::vector<char> &data)
{
    this->data = data;
}
