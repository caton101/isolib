/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <cstdint>
#include "DecDatetime.hpp"
#include "definitions.hpp"
#include "utils.hpp"

isochop::DecDatetime::DecDatetime()
{
    // make data dector
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    this->data = data;
    // populate with placeholder values
    this->setYear("0001");
    this->setMonth("01");
    this->setDay("01");
    this->setHour("01");
    this->setMinute("01");
    this->setSecond("01");
    this->setHundredths("01");
    this->setTimezone(0);
}

isochop::DecDatetime::DecDatetime(const std::vector<char> &data)
{
    // check size of data vector
    if (data.size() != ISOCHOP_DEC_DATETIME_SIZE_TOTAL)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_DEC_DATETIME_SIZE_TOTAL");
    }
    // set the data vector
    this->setData(data);
}

isochop::DecDatetime::~DecDatetime()
{
    // valgrind does not report any leaks at this time
}

bool isochop::DecDatetime::isValid()
{
    // make flags
    bool valid = true;
    // check value ranges
    int year, month, day, hour, minute, second, hundredths;
    try
    {
        year = std::stoi(this->getYear());
        month = std::stoi(this->getMonth());
        day = std::stoi(this->getDay());
        hour = std::stoi(this->getHour());
        minute = std::stoi(this->getMinute());
        second = std::stoi(this->getSecond());
        hundredths = std::stoi(this->getHundredths());
    }
    catch (std::invalid_argument &ia)
    {
        return false;
    }
    uint8_t timezone = this->getTimezone();
    valid &= year >= 1 && year <= 9999;
    valid &= month >= 1 && month <= 12;
    valid &= day >= 1 && day <= 31;
    valid &= hour >= 0 && hour <= 23;
    valid &= minute >= 0 && minute <= 59;
    valid &= second >= 0 && second <= 59;
    valid &= hundredths >= 0 && hundredths <= 99;
    valid &= timezone >= 0 && timezone <= 52;
    // return flags
    return valid;
}

std::string isochop::DecDatetime::toString()
{
    std::string str;
    str += "{";
    str += "Year: " + this->getYear() + ", ";
    str += "Month: " + this->getMonth() + ", ";
    str += "Day: " + this->getDay() + ", ";
    str += "Hour: " + this->getHour() + ", ";
    str += "Minutes: " + this->getMinute() + ", ";
    str += "Seconds: " + this->getSecond() + ", ";
    str += "Timezone: " + std::to_string(this->getTimezone());
    str += "}";
    return str;
}

std::vector<char> isochop::DecDatetime::getData()
{
    return this->data;
}

void isochop::DecDatetime::setData(const std::vector<char> &data)
{
    if (data.size() != ISOCHOP_DEC_DATETIME_SIZE_TOTAL)
    {
        throw new std::invalid_argument("data.size() != ISOCHOP_DEC_DATETIME_SIZE_TOTAL");
    }
    this->data = data;
}

std::string isochop::DecDatetime::getYear()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_YEAR, ISOCHOP_DEC_DATETIME_SIZE_YEAR);
}

void isochop::DecDatetime::setYear(const std::string &year)
{
    if (year.size() != ISOCHOP_DEC_DATETIME_SIZE_YEAR)
    {
        throw new std::invalid_argument("year.size() != ISOCHOP_DEC_DATETIME_SIZE_YEAR");
    }
    std::vector<char> temp = utils::extractVectorFromString(year, 0, year.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_YEAR, ISOCHOP_DEC_DATETIME_SIZE_YEAR);
}

std::string isochop::DecDatetime::getMonth()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_MONTH, ISOCHOP_DEC_DATETIME_SIZE_MONTH);
}

void isochop::DecDatetime::setMonth(const std::string &month)
{
    if (month.size() != ISOCHOP_DEC_DATETIME_SIZE_MONTH)
    {
        throw new std::invalid_argument("month.size() != ISOCHOP_DEC_DATETIME_SIZE_MONTH");
    }
    std::vector<char> temp = utils::extractVectorFromString(month, 0, month.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_MONTH, ISOCHOP_DEC_DATETIME_SIZE_MONTH);
}

std::string isochop::DecDatetime::getDay()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_DAY, ISOCHOP_DEC_DATETIME_SIZE_DAY);
}

void isochop::DecDatetime::setDay(const std::string &day)
{
    if (day.size() != ISOCHOP_DEC_DATETIME_SIZE_DAY)
    {
        throw new std::invalid_argument("day.size() != ISOCHOP_DEC_DATETIME_SIZE_DAY");
    }
    std::vector<char> temp = utils::extractVectorFromString(day, 0, day.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_DAY, ISOCHOP_DEC_DATETIME_SIZE_DAY);
}

std::string isochop::DecDatetime::getHour()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_HOUR, ISOCHOP_DEC_DATETIME_SIZE_HOUR);
}

void isochop::DecDatetime::setHour(const std::string &hour)
{
    if (hour.size() != ISOCHOP_DEC_DATETIME_SIZE_HOUR)
    {
        throw new std::invalid_argument("hour.size() != ISOCHOP_DEC_DATETIME_SIZE_HOUR");
    }
    std::vector<char> temp = utils::extractVectorFromString(hour, 0, hour.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_HOUR, ISOCHOP_DEC_DATETIME_SIZE_HOUR);
}

std::string isochop::DecDatetime::getMinute()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_MINUTE, ISOCHOP_DEC_DATETIME_SIZE_MINUTE);
}

void isochop::DecDatetime::setMinute(const std::string &minute)
{
    if (minute.size() != ISOCHOP_DEC_DATETIME_SIZE_MINUTE)
    {
        throw new std::invalid_argument("minute.size() != ISOCHOP_DEC_DATETIME_SIZE_MINUTE");
    }
    std::vector<char> temp = utils::extractVectorFromString(minute, 0, minute.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_MINUTE, ISOCHOP_DEC_DATETIME_SIZE_MINUTE);
}

std::string isochop::DecDatetime::getSecond()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_SECOND, ISOCHOP_DEC_DATETIME_SIZE_SECOND);
}

void isochop::DecDatetime::setSecond(const std::string &second)
{
    if (second.size() != ISOCHOP_DEC_DATETIME_SIZE_SECOND)
    {
        throw new std::invalid_argument("second.size() != ISOCHOP_DEC_DATETIME_SIZE_SECOND");
    }
    std::vector<char> temp = utils::extractVectorFromString(second, 0, second.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_SECOND, ISOCHOP_DEC_DATETIME_SIZE_SECOND);
}

std::string isochop::DecDatetime::getHundredths()
{
    return utils::extractStringFromVector(this->data, ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS, ISOCHOP_DEC_DATETIME_SIZE_HUNDREDTHS);
}

void isochop::DecDatetime::setHundredths(const std::string &hundredths)
{
    if (hundredths.size() != ISOCHOP_DEC_DATETIME_SIZE_HUNDREDTHS)
    {
        throw new std::invalid_argument("hundredths.size() != ISOCHOP_DEC_DATETIME_SIZE_HUNDREDTHS");
    }
    std::vector<char> temp = utils::extractVectorFromString(hundredths, 0, hundredths.size());
    utils::copyVector(&temp, &this->data, 0, ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS, ISOCHOP_DEC_DATETIME_SIZE_HUNDREDTHS);
}

uint8_t isochop::DecDatetime::getTimezone()
{
    return this->data.at(ISOCHOP_DEC_DATETIME_OFFSET_TIMEZONE);
}

void isochop::DecDatetime::setTimezone(uint8_t timezone)
{
    this->data.at(ISOCHOP_DEC_DATETIME_OFFSET_TIMEZONE) = timezone;
}
