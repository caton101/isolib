/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <stdexcept>
#include <fstream>
#include "json/single_include/nlohmann/json.hpp"
#include "Orphan.hpp"
#include "utils.hpp"

isochop::Orphan::Orphan()
{
    // make data vector
    std::vector<char> data(10, 0);
    this->setData(data);
    // set address
    this->setAddress(0);
}

isochop::Orphan::Orphan(const std::vector<char> &data, unsigned long address)
{
    // set the data vector
    this->setData(data);
    // set the address
    this->setAddress(address);
}

isochop::Orphan::Orphan(const std::string &dirpath, const std::string &manifesthash)
{
    // initialize object from directory structure
    this->import_data(dirpath, manifesthash);
}

isochop::Orphan::~Orphan()
{
    // valgrind does not report any leaks at this time
}

std::string isochop::Orphan::toString()
{
    std::string str;
    str += "{Size: ";
    str += std::to_string(this->getData().size());
    str += ",Address: ";
    str += std::to_string(this->getAddress());
    str += "}";
    return str;
}

isochop::Orphan isochop::Orphan::clone()
{
    Orphan orphan;
    orphan.setData(this->data);
    orphan.setAddress(this->address);
    return orphan;
}

std::vector<char> isochop::Orphan::getData()
{
    return this->data;
}

void isochop::Orphan::setData(const std::vector<char> &data)
{
    this->data = data;
}

unsigned long isochop::Orphan::getAddress()
{
    return this->address;
}

void isochop::Orphan::setAddress(unsigned long address)
{
    this->address = address;
}

std::string isochop::Orphan::export_data(const std::string &dirpath)
{
    // get the SHA256 hash of the data and store it
    std::string sha256data = utils::sha256sum(&this->data);
    utils::dump_vector(&this->data, dirpath + "/" + sha256data, nullptr);
    // generate manifest JSON
    nlohmann::json manifestJSON;
    manifestJSON["manifest_class"] = "Orphan";
    manifestJSON["data_hash"] = sha256data;
    manifestJSON["byte_address"] = this->address;
    // generate manifest string
    std::string manifestString = manifestJSON.dump(4, ' ', true);
    // calculate manifest hash
    std::vector<char> tmp;
    for (char c : manifestString)
    {
        tmp.push_back(c);
    }
    std::string manifestSHA256 = utils::sha256sum(&tmp);
    tmp.clear();
    // write JSON to file
    std::fstream file;
    file.open(dirpath + "/" + manifestSHA256, std::fstream::out);
    file << manifestString;
    file.close();
    // return this Orphan's SHA256 hash
    return manifestSHA256;
}

void isochop::Orphan::import_data(const std::string &dirpath, const std::string &manifesthash)
{
    // parse manifest file
    std::fstream manifestFile;
    manifestFile.open(dirpath + "/" + manifesthash, std::fstream::in);
    nlohmann::json manifestJSON;
    manifestFile >> manifestJSON;
    manifestFile.close();
    // load data from file
    std::string sha256data = manifestJSON["data_hash"];
    unsigned long addr = manifestJSON["byte_address"];
    // set fields
    this->address = addr;
    this->data = utils::load_vector(dirpath + "/" + sha256data, nullptr);
}

void isochop::Orphan::markAddresses(std::vector<bool> *bits)
{
    if (bits == nullptr)
    {
        throw std::invalid_argument("bits == nullptr");
    }
    for (unsigned long i = this->address; i < this->address + this->data.size(); i++)
    {
        bits->at(i) = true;
    }
}
