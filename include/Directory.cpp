/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <cerrno>
#include "Directory.hpp"
#include "FileFlags.hpp"
#include "definitions.hpp"
#include "utils.hpp"

isochop::Directory::Directory()
{
    std::vector<char> tmp(255, 0);
    tmp.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    this->data = tmp;
}

isochop::Directory::Directory(const std::vector<char> &data)
{
    this->setData(data);
}

isochop::Directory::~Directory()
{
    // valgrind does not report any leaks at this time
}

bool isochop::Directory::isValid()
{
    // make valid flag
    bool valid = true;
    // director vector can't be empty
    valid &= this->data.size() != 0;
    // data vector and directory record size must match
    valid &= this->data.size() == this->getDirectoryRecordLength();
    // NOTE 1: directory record size can't be higher than 255 (checked by uint8_t)
    // NOTE 2: file identifier and file identifier length must match (but impossible to check)
    // return valid flag
    return valid;
}

std::string isochop::Directory::toString()
{
    std::string str;
    str += "{ Directory Record Length: " + std::to_string(this->getDirectoryRecordLength());
    str += ", Extended Attribute Record Length: " + std::to_string(this->getExtendedAttributeRecordLength());
    str += ", Location of Extent: " + std::to_string(this->getLocationOfExtent());
    str += ", Data Length: " + std::to_string(this->getDataLength());
    str += ", Recording Date and Time: " + this->getRecordingDateAndTime().toString();
    str += ", File Flags: " + this->getFileFlags().toString();
    str += ", File Unit Size: " + std::to_string(this->getFileUnitSize());
    str += ", Interleave Gap Size: " + std::to_string(this->getInterleaveGapSize());
    str += ", Volume Sequence Number: " + std::to_string(this->getVolumeSequenceNumber());
    str += ", Length of File Identifier: " + std::to_string(this->getFileIdentifierLength());
    str += ", File Identifier: '" + this->getFileIdentifier() + "'";
    str += ", System Use Size: " + std::to_string(this->getSystemUse().size());
    str += " }";
    return str;
}

isochop::Directory isochop::Directory::clone()
{
    Directory dir = Directory(this->data);
    return dir;
}

std::vector<char> isochop::Directory::getData()
{
    return this->data;
}

void isochop::Directory::setData(const std::vector<char> &data)
{
    this->data = data;
}

uint8_t isochop::Directory::getDirectoryRecordLength()
{
    return this->data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH);
}

void isochop::Directory::setDirectoryRecordLength(uint8_t length)
{
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = length;
}

uint8_t isochop::Directory::getExtendedAttributeRecordLength()
{
    return this->data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENDED_ATTRIBUTE_RECORD_LENGTH);
}

void isochop::Directory::setExtendedAttributeRecordLength(uint8_t length)
{
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_EXTENDED_ATTRIBUTE_RECORD_LENGTH) = length;
}

uint32_t isochop::Directory::getLocationOfExtent()
{
    std::vector<char> bytes(8, 0);
    utils::copyVector(&this->data, &bytes, ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION, 0, ISOCHOP_DIRECTORY_SIZE_EXTENT_LOCATION);
    return utils::decodeInt32LSB_MSB(bytes);
}

void isochop::Directory::setLocationOfExtent(uint32_t location)
{
    std::vector<char> bytes = utils::encodeInt32LSB_MSB(location);
    utils::copyVector(&bytes, &this->data, 0, ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION, ISOCHOP_DIRECTORY_SIZE_EXTENT_LOCATION);
}

uint32_t isochop::Directory::getDataLength()
{
    std::vector<char> bytes(8, 0);
    utils::copyVector(&this->data, &bytes, ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH, 0, ISOCHOP_DIRECTORY_SIZE_DATA_LENGTH);
    return utils::decodeInt32LSB_MSB(bytes);
}
void isochop::Directory::setDataLength(uint32_t length)
{
    std::vector<char> bytes = utils::encodeInt32LSB_MSB(length);
    utils::copyVector(&bytes, &this->data, 0, ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH, ISOCHOP_DIRECTORY_SIZE_DATA_LENGTH);
}

isochop::DirDatetime isochop::Directory::getRecordingDateAndTime()
{
    std::vector<char> bytes(ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME, 0);
    utils::copyVector(&this->data, &bytes, ISOCHOP_DIRECTORY_OFFSET_RECORDING_DATE_AND_TIME, 0, ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME);
    DirDatetime dd = DirDatetime(bytes);
    return dd;
}

void isochop::Directory::setRecordingDateAndTime(DirDatetime timestamp)
{
    std::vector<char> bytes = timestamp.getData();
    utils::copyVector(&bytes, &this->data, 0, ISOCHOP_DIRECTORY_OFFSET_RECORDING_DATE_AND_TIME, ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME);
}

isochop::FileFlags isochop::Directory::getFileFlags()
{
    uint8_t byte = this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_FLAGS);
    FileFlags ff = FileFlags(byte);
    return ff;
}

void isochop::Directory::setFileFlags(FileFlags flags)
{
    uint8_t byte = flags.getData();
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_FLAGS) = byte;
}

uint8_t isochop::Directory::getFileUnitSize()
{
    return this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_UNIT_SIZE);
}

void isochop::Directory::setFileUnitSize(uint8_t size)
{
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_UNIT_SIZE) = size;
}

uint8_t isochop::Directory::getInterleaveGapSize()
{
    return this->data.at(ISOCHOP_DIRECTORY_OFFSET_INTERLEAVE_GAP_SIZE);
}

void isochop::Directory::setInterleaveGapSize(uint8_t size)
{
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_INTERLEAVE_GAP_SIZE) = size;
}

uint16_t isochop::Directory::getVolumeSequenceNumber()
{
    std::vector<char> bytes(ISOCHOP_DIRECTORY_SIZE_VOLUME_SEQUENCE_NUMBER, 0);
    utils::copyVector(&this->data, &bytes, ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER, 0, ISOCHOP_DIRECTORY_SIZE_VOLUME_SEQUENCE_NUMBER);
    return utils::decodeInt16LSB_MSB(bytes);
}

void isochop::Directory::setVolumeSequenceNumber(uint16_t number)
{
    std::vector<char> bytes = utils::encodeInt16LSB_MSB(number);
    utils::copyVector(&bytes, &this->data, 0, ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER, ISOCHOP_DIRECTORY_SIZE_VOLUME_SEQUENCE_NUMBER);
}

uint8_t isochop::Directory::getFileIdentifierLength()
{
    return this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH);
}

void isochop::Directory::setFileIdentifierLength(uint8_t length)
{
    this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH) = length;
}

std::string isochop::Directory::getFileIdentifier()
{
    std::string buffer;
    for (std::size_t i = 0; i < this->getFileIdentifierLength(); i++)
    {
        buffer += this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER + i);
    }
    return buffer;
}

void isochop::Directory::setFileIdentifier(const std::string &id)
{
    this->setFileIdentifierLength(id.size());
    for (std::size_t i = 0; i < id.length(); i++)
    {
        this->data.at(ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER + i) = id.at(i);
    }
}

std::size_t isochop::Directory::calcSystemUseAddress()
{
    uint8_t addr = ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER + this->getFileIdentifierLength();
    if (this->getFileIdentifierLength() % 2 == 1)
    {
        addr += 1;
    }
    if (addr > this->getDirectoryRecordLength())
    {
        addr = this->getDirectoryRecordLength();
    }
    return addr;
}

std::vector<char> isochop::Directory::getSystemUse()
{
    uint8_t addr = this->calcSystemUseAddress();
    uint8_t length = this->getDirectoryRecordLength() - addr;
    std::vector<char> tmp(length, 0);
    utils::copyVector(&this->data, &tmp, addr, 0, length);
    return tmp;
}

void isochop::Directory::setSystemUse(std::vector<char> data)
{
    // calculate sizes
    std::size_t addr = this->calcSystemUseAddress();
    std::size_t directoryLength = addr + data.size();
    if (directoryLength > 255)
    {
        throw std::invalid_argument("directoryLength > 255");
    }
    // fit data vector to size
    while (directoryLength != this->data.size())
    {
        if (directoryLength > this->data.size())
        {
            this->data.push_back(0);
        }
        else if (directoryLength < this->data.size())
        {
            this->data.pop_back();
        }
    }
    // update directory record length
    this->setDirectoryRecordLength(directoryLength);
    // copy data
    utils::copyVector(&data, &this->data, 0, addr, data.size());
}
