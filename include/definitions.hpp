/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// the sector size (number of bytes in a sector)
#define ISOCHOP_SECTOR_SIZE 2048

// the system area size (the beginning of an ISO file)
#define ISOCHOP_SYSTEM_AREA_SIZE 0x8000

// the size of a dec-datetime entry
#define ISOCHOP_DEC_DATETIME_SIZE_TOTAL 17

// the offset of a dec-datetime year
#define ISOCHOP_DEC_DATETIME_OFFSET_YEAR 0

// the size of a dec-datetime year
#define ISOCHOP_DEC_DATETIME_SIZE_YEAR 4

// the offset of a dec-datetime month
#define ISOCHOP_DEC_DATETIME_OFFSET_MONTH 4

// the size of a dec-datetime month
#define ISOCHOP_DEC_DATETIME_SIZE_MONTH 2

// the offset of a dec-datetime day
#define ISOCHOP_DEC_DATETIME_OFFSET_DAY 6

// the size of a dec-datetime day
#define ISOCHOP_DEC_DATETIME_SIZE_DAY 2

// the offset of a dec-datetime hour
#define ISOCHOP_DEC_DATETIME_OFFSET_HOUR 8

// the size of a dec-datetime hour
#define ISOCHOP_DEC_DATETIME_SIZE_HOUR 2

// the offset of a dec-datetime minute
#define ISOCHOP_DEC_DATETIME_OFFSET_MINUTE 10

// the size of a dec-datetime minute
#define ISOCHOP_DEC_DATETIME_SIZE_MINUTE 2

// the offset of a dec-datetime second
#define ISOCHOP_DEC_DATETIME_OFFSET_SECOND 12

// the size of a dec-datetime second
#define ISOCHOP_DEC_DATETIME_SIZE_SECOND 2

// the offset of a dec-datetime hundredths
#define ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS 14

// the size of a dec-datetime hundredths
#define ISOCHOP_DEC_DATETIME_SIZE_HUNDREDTHS 2

// the offset of a dec-datetime timezone
#define ISOCHOP_DEC_DATETIME_OFFSET_TIMEZONE 16

// the size of a directory's datetime
#define ISOCHOP_DIR_DATETIME_SIZE_TOTAL 7

// the offset of a directory's datetime year
#define ISOCHOP_DIR_DATETIME_OFFSET_YEAR 0

// the offset of a directory's datetime month
#define ISOCHOP_DIR_DATETIME_OFFSET_MONTH 1

// the offset of a directory's datetime day
#define ISOCHOP_DIR_DATETIME_OFFSET_DAY 2

// the offset of a directory's datetime hour
#define ISOCHOP_DIR_DATETIME_OFFSET_HOUR 3

// the offset of a directory's datetime minute
#define ISOCHOP_DIR_DATETIME_OFFSET_MINUTE 4

// the offset of a directory's datetime second
#define ISOCHOP_DIR_DATETIME_OFFSET_SECOND 5

// the offset of a directory's datetime timezone
#define ISOCHOP_DIR_DATETIME_OFFSET_TIMEZONE 6

// the offset of a FileFlag's hidden flag
#define ISOCHOP_FILEFLAGS_OFFSET_HIDDEN 0

// the offset of a FileFlag's directory flag
#define ISOCHOP_FILEFLAGS_OFFSET_DIRECTORY 1

// the offset of a FileFlag's associated flag
#define ISOCHOP_FILEFLAGS_OFFSET_ASSOCIATED 2

// the offset of a FileFlag's format flag
#define ISOCHOP_FILEFLAGS_OFFSET_FORMAT 3

// the offset of a FileFlag's permissions flag
#define ISOCHOP_FILEFLAGS_OFFSET_PERMISSIONS 4

// the offset of a FileFlag's reserved 5 flag
#define ISOCHOP_FILEFLAGS_OFFSET_RESERVED_5 5

// the offset of a FileFlag's reserved 6 flag
#define ISOCHOP_FILEFLAGS_OFFSET_RESERVED_6 6

// the offset of a FileFlag's final flag
#define ISOCHOP_FILEFLAGS_OFFSET_FINAL 7

// the offset of a Directory's directory record length field
#define ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH 0

// the size of a Directory's directory record length field
#define ISOCHOP_DIRECTORY_SIZE_DIRECTORY_RECORD_LENGTH 1

// the offset of a Directory's extended attribute record length field
#define ISOCHOP_DIRECTORY_OFFSET_EXTENDED_ATTRIBUTE_RECORD_LENGTH 1

// the size of a Directory's extended attribute record length field
#define ISOCHOP_DIRECTORY_SIZE_EXTENDED_ATTRIBUTE_RECORD_LENGTH 1

// the offset of a Directory's extent location field
#define ISOCHOP_DIRECTORY_OFFSET_EXTENT_LOCATION 2

// the size of a Directory's extent location field
#define ISOCHOP_DIRECTORY_SIZE_EXTENT_LOCATION 8

// the offset of a Directory's data length field
#define ISOCHOP_DIRECTORY_OFFSET_DATA_LENGTH 10

// the size of a Directory's data length field
#define ISOCHOP_DIRECTORY_SIZE_DATA_LENGTH 8

// the offset of a Directory's recording date and time field
#define ISOCHOP_DIRECTORY_OFFSET_RECORDING_DATE_AND_TIME 18

// the size of a Directory's recording date and time field
#define ISOCHOP_DIRECTORY_SIZE_RECORDING_DATE_AND_TIME 7

// the offset of a Directory's file flags field
#define ISOCHOP_DIRECTORY_OFFSET_FILE_FLAGS 25

// the size of a Directory's file flags field
#define ISOCHOP_DIRECTORY_SIZE_FILE_FLAGS 1

// the offset of a Directory's file unit size field
#define ISOCHOP_DIRECTORY_OFFSET_FILE_UNIT_SIZE 26

// the size of a Directory's file unit size field
#define ISOCHOP_DIRECTORY_SIZE_FILE_UNIT_SIZE 1

// the offset of a Directory's interleave gap size field
#define ISOCHOP_DIRECTORY_OFFSET_INTERLEAVE_GAP_SIZE 27

// the size of a Directory's interleave gap size field
#define ISOCHOP_DIRECTORY_SIZE_INTERLEAVE_GAP_SIZE 1

// the offset of a Directory's volume sequence number field
#define ISOCHOP_DIRECTORY_OFFSET_VOLUME_SEQUENCE_NUMBER 28

// the size of a Directory's volume sequence number field
#define ISOCHOP_DIRECTORY_SIZE_VOLUME_SEQUENCE_NUMBER 4

// the offset of a Directory's file identifier length field
#define ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER_LENGTH 32

// the size of a Directory's file identifier length field
#define ISOCHOP_DIRECTORY_SIZE_FILE_IDENTIFIER_LENGTH 1

// the offset of a Directory's file identifier field
#define ISOCHOP_DIRECTORY_OFFSET_FILE_IDENTIFIER 33

// the total size of a volume descriptor (one sector)
#define ISOCHOP_VD_SIZE_TOTAL 0x800

// the size of the data area inside a volume descriptor (one sector minus 7 bytes)
#define ISOCHOP_VD_SIZE_DATA 0x7f9

// the size of the identifier string (5 bytes)
#define ISOCHOP_VD_SIZE_IDENTIFIER 5

// the volume descriptor type for a boot area descriptor
#define ISOCHOP_VD_TYPE_BOOT 0

// the volume descriptor type for a primary volume descriptor
#define ISOCHOP_VD_TYPE_PRIMARY 1

// the volume descriptor type for a supplementary volume descriptor
#define ISOCHOP_VD_TYPE_SUPPLEMENTARY 2

// the volume descriptor type for a volume partition descriptor
#define ISOCHOP_VD_TYPE_PARTITION 3

// the volume descriptor type for a volume descriptor set terminator
#define ISOCHOP_VD_TYPE_TERMINATOR 255

// offset for system identifier
#define ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER 8

// size of system identifier
#define ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER 32

// offset for volume identifier
#define ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER 40

// size of volume identifier
#define ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER 32

// offset for volume space size
#define ISOCHOP_PMVD_OFFSET_VOLUME_SPACE_SIZE 80

// offset for volume set size
#define ISOCHOP_PMVD_OFFSET_VOLUME_SET_SIZE 120

// offset for volume sequence number
#define ISOCHOP_PMVD_OFFSET_VOLUME_SEQUENCE_NUMBER 124

// offset for logical block size
#define ISOCHOP_PMVD_OFFSET_LOGICAL_BLOCK_SIZE 128

// offset for path table size
#define ISOCHOP_PMVD_OFFSET_PATH_TABLE_SIZE 132

// offset for location of type-l path table
#define ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_L_PATH_TABLE 140

// offset for location of optional type-l path table
#define ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_L_PATH_TABLE 144

// offset for location of type-m path table
#define ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_M_PATH_TABLE 148

// offset for location of optional type-m path table
#define ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_M_PATH_TABLE 152

// offset for root directory entry
#define ISOCHOP_PMVD_OFFSET_ROOT_DIRECTORY_ENTRY 156

// size of root directory entry
#define ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY 34

// offset for volume set identifier
#define ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER 190

// size of volume set identifier
#define ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER 128

// offset for publisher identifier
#define ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER 318

// size of publisher identifier
#define ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER 128

// offset for data preparer identifier
#define ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER 446

// size of data preparer identifier
#define ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER 128

// offset for application identifier
#define ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER 574

// size of application identifier
#define ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER 128

// offset for copyright file identifier
#define ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER 702

// size of copyright file identifier
#define ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER 37

// offset for abstract file identifier
#define ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER 739

// size of abstract file identifier
#define ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER 37

// offset for biblographic file identifier
#define ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER 776

// size of biblographic file identifier
#define ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER 37

// offset for volume creation date and time
#define ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME 813

// offset for volume modification date and time
#define ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME 830

// offset for volume expiration date and time
#define ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME 847

// offset for volume effective date and time
#define ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME 864

// offset for file structure version
#define ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION 881

// offset for application used
#define ISOCHOP_PMVD_OFFSET_APPLICATION_USED 883

// size of application used
#define ISOCHOP_PMVD_SIZE_APPLICATION_USED 512

// offset for reserved
#define ISOCHOP_PMVD_OFFSET_RESERVED 1395

// size of reserved
#define ISOCHOP_PMVD_SIZE_RESERVED 653

// iso will be loaded using parseISO
#define ISOCHOP_ISO_LOADER_FILE 0

// iso will be loading using import_data
#define ISOCHOP_ISO_LOADER_DIRECTORY 1

// iso was parsed correctly
#define ISOCHOP_ISO_PARSE_RET_SUCCESS 0

// iso has an invalid child volume descriptor
#define ISOCHOP_ISO_PARSE_RET_WARNING_VD_INVALID_CHILD 1

// iso file does not exist
#define ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_FOUND 2

// iso file path is pointing to a directory
#define ISOCHOP_ISO_PARSE_RET_ERROR_FILE_IS_DIRECTORY 3

// the iso fstream is not good
#define ISOCHOP_ISO_PARSE_RET_ERROR_FILE_NOT_GOOD 4

// iso contains an invalid volume descriptor
#define ISOCHOP_ISO_PARSE_RET_ERROR_VD_NOT_VALID 5

// iso file has a volume descriptor with an unknown type
#define ISOCHOP_ISO_PARSE_RET_ERROR_VD_TYPE_UNKNOWN 6

// iso file has no primary volume descriptor
#define ISOCHOP_ISO_PARSE_RET_ERROR_NO_PRIMARY_VD 7

// iso parser opened the disc
#define ISOCHOP_ISO_PARSE_STATUS_OPENING_DISC 0

// iso parser is reading system reserved area
#define ISOCHOP_ISO_PARSE_STATUS_READING_SYSTEM_AREA 1

// iso parser is reading volume descriptors
#define ISOCHOP_ISO_PARSE_STATUS_READING_VOLUME_DESCRIPTORS 2

// iso parser is reading extent tree
#define ISOCHOP_ISO_PARSE_STATUS_READING_EXTENT_TREE 3

// iso parser is marking reserved area
#define ISOCHOP_ISO_PARSE_STATUS_MARKING_SYSTEM_AREA 4

// iso parser is marking volume descriptors
#define ISOCHOP_ISO_PARSE_STATUS_MARKING_VOLUME_DESCRIPTORS 5

// iso parser is marking extent tree
#define ISOCHOP_ISO_PARSE_STATUS_MARKING_EXTENT_TREE 6

// iso parser is reading orphans
#define ISOCHOP_ISO_PARSE_STATUS_READING_ORPHANS 7

// iso parser is done
#define ISOCHOP_ISO_PARSE_STATUS_DONE 8

// iso was imported correctly
#define ISOCHOP_ISO_IMPORT_RET_SUCCESS 0

// iso importer's fstream for STRUCTURE_ROOT was not good
#define ISOCHOP_ISO_IMPORT_RET_ROOT_FSTREAM_NOT_GOOD 1

// iso importer's fstream for ISO manifest was not good
#define ISOCHOP_ISO_IMPORT_RET_ISO_FSTREAM_NOT_GOOD 2

// iso importer is validating the directory
#define ISOCHOP_ISO_IMPORT_VALIDATE_DIRECTORY 0

// iso importer is opening the STRUCTURE_ROOT file
#define ISOCHOP_ISO_IMPORT_STATUS_OPENING_ROOT 1

// iso importer is reading the STRUCTURE_ROOT file
#define ISOCHOP_ISO_IMPORT_STATUS_READING_ROOT 2

// iso importer is opening the ISO manifest file
#define ISOCHOP_ISO_IMPORT_STATUS_OPENING_MANIFEST 3

// iso importer is reading the ISO manifest file
#define ISOCHOP_ISO_IMPORT_STATUS_READING_MANIFEST 4

// iso importer is reading the system area
#define ISOCHOP_ISO_IMPORT_STATUS_READING_SYSTEM_AREA 5

// iso importer is reading the volume descriptors
#define ISOCHOP_ISO_IMPORT_STATUS_READING_VOLUME_DESCRIPTORS 6

// iso importer is reading the extent tree
#define ISOCHOP_ISO_IMPORT_STATUS_READING_EXTENT_TREE 7

// iso importer is reading orphan data
#define ISOCHOP_ISO_IMPORT_STATUS_READING_ORPHANS 8

// iso importer has finished importing
#define ISOCHOP_ISO_IMPORT_STATUS_DONE 9

// iso exporter is cleaning the export directory
#define ISOCHOP_ISO_EXPORT_PREPARING_DIRECTORY 0

// iso exporter is writing the system area
#define ISOCHOP_ISO_EXPORT_WRITING_SYSTEM_AREA 1

// iso exporter is writing the extent tree
#define ISOCHOP_ISO_EXPORT_WRITING_EXTENT_TREE 2

// iso exporter is writing the volume descriptors
#define ISOCHOP_ISO_EXPORT_WRITING_VOLUME_DESCRIPTORS 3

// iso exporter is writing the orphan data
#define ISOCHOP_ISO_EXPORT_WRITING_ORPHANS 4

// iso exporter is generating the ISO manifest
#define ISOCHOP_ISO_EXPORT_GENERATING_MANIFEST 5

// iso exporter is writing the ISO manifest
#define ISOCHOP_ISO_EXPORT_WRITING_MANIFEST 6

// iso exporter is writing the STRUCTURE_ROOT
#define ISOCHOP_ISO_EXPORT_WRITING_STRUCTURE_ROOT 7

// iso exporter is done
#define ISOCHOP_ISO_EXPORT_DONE 8

// iso was written correctly
#define ISOCHOP_ISO_WRITE_RET_SUCCESS 0

// iso writing path already exists
#define ISOCHOP_ISO_WRITE_RET_PATH_EXISTS 1

// the iso fstream is not good
#define ISOCHOP_ISO_WRITE_RET_FILE_NOT_GOOD 2

// iso writer is opening the file path
#define ISOCHOP_ISO_WRITE_STATUS_OPENING_DISC 0

// iso writer is allocating the disc
#define ISOCHOP_ISO_WRITE_STATUS_PREALLOCATING_DISC 1

// iso writer is writing the system area
#define ISOCHOP_ISO_WRITE_STATUS_WRITING_SYSTEM_AREA 2

// iso writer is writing the volume descriptors
#define ISOCHOP_ISO_WRITE_STATUS_WRITING_VOLUME_DESCRIPTORS 3

// iso writer is writing the extent tree
#define ISOCHOP_ISO_WRITE_STATUS_WRITING_EXTENT_TREE 4

// iso writer is writing the orphan data
#define ISOCHOP_ISO_WRITE_STATUS_WRITING_ORPHANS 5

// iso writer is finished
#define ISOCHOP_ISO_WRITE_STATUS_DONE 6
