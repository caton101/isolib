/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <cstdint>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class implements a Datetime entry for directories
     * @note   all fields and methods follow the ISO 9660 specification
     */
    class DirDatetime
    {
    protected:
        // this stores the dir-datetime information
        std::vector<char> data;

    public:
        /**
         * @brief  The no-args constructor for a dir-datetime object
         * @note   default values may not be valid
         * @retval a new dir-datetime object
         */
        DirDatetime();

        /**
         * @brief  The normal constructor for a dir-datetime object
         * @note   data vector must be of size DIR_DATETIME_SIZE_TOTAL
         * @param  data: data vector to parse
         * @retval a new dir-datetime object
         */
        DirDatetime(const std::vector<char> &data);

        /**
         * @brief  The deconstructor for a dir-datetime object
         */
        ~DirDatetime();

        /**
         * @brief  Checks if all values are valid
         * @retval true if all values are valid
         */
        bool isValid();

        /**
         * @brief  Generates a string summarizing the dir-datetime
         * @note   string output may change format in future versions
         * @retval string representing the dir-datetime
         */
        std::string toString();

        /**
         * @brief  Fetch the data vector
         * @retval data vector
         */
        std::vector<char> getData();

        /**
         * @brief  Set the data vector
         * @note   must be of size DIR_DATETIME_SIZE_TOTAL
         * @param  data: data vector
         * @retval None
         */
        void setData(const std::vector<char> &data);

        /**
         * @brief  Fetch the year field
         * @retval the year
         */
        uint8_t getYear();

        /**
         * @brief  Set the year field
         * @param  year: number of years since 1900
         * @retval None
         */
        void setYear(uint8_t year);

        /**
         * @brief  Fetch the month field
         * @retval the month
         */
        uint8_t getMonth();

        /**
         * @brief  Set the month field
         * @param  month: the month (from 1 to 12)
         * @retval None
         */
        void setMonth(uint8_t month);

        /**
         * @brief  Fetch the day field
         * @retval the day
         */
        uint8_t getDay();

        /**
         * @brief  Set the day field
         * @param  day: the day (from 0 to 23)
         * @retval None
         */
        void setDay(uint8_t day);

        /**
         * @brief  Fetch the hour field
         * @retval the hour
         */
        uint8_t getHour();

        /**
         * @brief  Set the hour field
         * @note   must be of size DEC_DATETIME_SIZE_HOUR
         * @param  hour: the hour (from 0 to 23)
         * @retval None
         */
        void setHour(uint8_t hour);

        /**
         * @brief  Fetch the minute field
         * @retval the minute
         */
        uint8_t getMinute();

        /**
         * @brief  Set the minute field
         * @note   must be of size DEC_DATETIME_SIZE_MINUTE
         * @param  minute: the minute (from 0 to 59)
         * @retval None
         */
        void setMinute(uint8_t minute);

        /**
         * @brief  Fetch the second field
         * @retval the second
         */
        uint8_t getSecond();

        /**
         * @brief  Set the second field
         * @note   must be of size DEC_DATETIME_SIZE_SECOND
         * @param  second: the second (from 0 to 59)
         * @retval None
         */
        void setSecond(uint8_t second);

        /**
         * @brief  Fetch the timezone field
         * @retval the timezone
         */
        int8_t getTimezone();

        /**
         * @brief  Set the timezone field
         * @param  timezone: the timezone (from -48 to +52)
         * @retval None
         */
        void setTimezone(int8_t timezone);
    };
}
