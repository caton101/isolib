/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <string>
#include <vector>
#include <cstdint>
#include <stdexcept>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This contain various utility functions
     */
    namespace utils
    {
        /**
         * @brief  This copies bytes from one array to another
         * @note   assumes all arguments are valid
         * @param  origin array to read
         * @param  destination array to write
         * @param  size: number of bytes
         * @retval None
         */
        void copyBytes(void *origin, void *destination, int size);

        /**
         * @brief  This copies bytes from one vector to another
         * @param  origin: vector to read
         * @param  destination: vector to write
         * @param  origin_start: first index to read
         * @param  destination_start: first index to write
         * @param  size: number of bytes
         * @retval None
         */
        void copyVector(std::vector<char> *origin, std::vector<char> *destination, std::size_t origin_start, std::size_t destination_start, std::size_t size);

        /**
         * @brief  This copies characters from a vector to a string
         * @param  vec: vector to read
         * @param  offset: first index to read
         * @param  size: number of characters
         * @retval extracted string
         */
        std::string extractStringFromVector(std::vector<char> vec, std::size_t offset, std::size_t size);

        /**
         * @brief  This copies characters from a string into a vector
         * @param  str: string to read
         * @param  offset: first index to read
         * @param  size: number of bytes
         * @retval extracted vector
         */
        std::vector<char> extractVectorFromString(std::string str, std::size_t offset, std::size_t size);

        /**
         * @brief  This checks if a string conforms to the strA encoding
         * @param  str: text string
         * @param  allowZero: allow \0 character
         * @retval true if string is strA encoded
         */
        bool isStrA(const std::string &str, bool allowZero);

        /**
         * @brief  This checks if a string conforms to the strD encoding
         * @param  str: text string
         * @param  allowZero: allow \0 character
         * @retval true if string is strD encoded
         */
        bool isStrD(const std::string &str, bool allowZero);

        /**
         * @brief  This decodes an int16_LSB encoded number from a byte vector
         * @note   int16_LSB means unsigned 16-bit little-endian integer
         * @param  bytes: the byte vector
         * @retval a uint16_t number
         */
        uint16_t decodeInt16LSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an int16_MSB encoded number from a byte vector
         * @note   int16_MSB means unsigned 16-bit big-endian integer
         * @param  bytes: the byte vector
         * @retval a uint16_t number
         */
        uint16_t decodeInt16MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an int16_LSB-MSB encoded number from a byte vector
         * @note   int16_LSB-MSB means unsigned 16-bit both-endian integer (little then big)
         * @param  bytes: the byte vector
         * @retval a uint16_t number
         */
        uint16_t decodeInt16LSB_MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint16_LSB encoded number from a byte vector
         * @note   sint16_LSB means signed 16-bit little-endian integer
         * @param  bytes: the byte vector
         * @retval an int16_t number
         */
        int16_t decodeSint16LSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint16_MSB encoded number from a byte vector
         * @note   sint16_MSB means signed 16-bit big-endian integer
         * @param  bytes: the byte vector
         * @retval an int16_t number
         */
        int16_t decodeSint16MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint16_LSB-MSB encoded number from a byte vector
         * @note   sint16_LSB-MSB means signed 16-bit both-endian integer (little then big)
         * @param  bytes: the byte vector
         * @retval an int16_t number
         */
        int16_t decodeSint16LSB_MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an int32_LSB encoded number from a byte vector
         * @note   int32_LSB means unsigned 32-bit little-endian integer
         * @param  bytes: the byte vector
         * @retval a uint32_t number
         */
        uint32_t decodeInt32LSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an int32_MSB encoded number from a byte vector
         * @note   int32_MSB means unsigned 32-bit big-endian integer
         * @param  bytes: the byte vector
         * @retval a uint32_t number
         */
        uint32_t decodeInt32MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an int32_LSB-MSB encoded number from a byte vector
         * @note   int32_LSB-MSB means unsigned 32-bit both-endian integer (little then big)
         * @param  bytes: the byte vector
         * @retval a uint32_t number
         */
        uint32_t decodeInt32LSB_MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint32_LSB encoded number from a byte vector
         * @note   sint32_LSB means signed 32-bit little-endian integer
         * @param  bytes: the byte vector
         * @retval an int32_t number
         */
        int32_t decodeSint32LSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint32_MSB encoded number from a byte vector
         * @note   sint32_MSB means signed 32-bit big-endian integer
         * @param  bytes: the byte vector
         * @retval an int32_t number
         */
        int32_t decodeSint32MSB(const std::vector<char> &bytes);

        /**
         * @brief  This decodes an sint32_LSB-MSB encoded number from a byte vector
         * @note   sint32_LSB-MSB means signed 32-bit both-endian integer (little then big)
         * @param  bytes: the byte vector
         * @retval an int32_t number
         */
        int32_t decodeSint32LSB_MSB(const std::vector<char> &bytes);

        /**
         * @brief  This encodes a byte vector from an int16_LSB encoded number
         * @note   int16_LSB means unsigned 16-bit little-endian integer
         * @param  number: a uint16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt16LSB(uint16_t number);

        /**
         * @brief  This encodes a byte vector from an int16_MSB encoded number
         * @note   int16_MSB means unsigned 16-bit big-endian integer
         * @param  number: a uint16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt16MSB(uint16_t number);

        /**
         * @brief  This encodes a byte vector from an int16_LSB-MSB encoded number
         * @note   int16_LSB-MSB means unsigned 16-bit both-endian integer (little then big)
         * @param  number: a uint16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt16LSB_MSB(uint16_t number);

        /**
         * @brief  This encodes a byte vector from an sint16_LSB encoded number
         * @note   sint16_LSB means signed 16-bit little-endian integer
         * @param  number: an int16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint16LSB(int16_t number);

        /**
         * @brief  This encodes a byte vector from an sint16_MSB encoded number
         * @note   sint16_MSB means signed 16-bit big-endian integer
         * @param  number: an int16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint16MSB(int16_t number);

        /**
         * @brief  This encodes a byte vector from an sint16_LSB-MSB encoded number
         * @note   sint16_LSB-MSB means signed 16-bit both-endian integer (little then big)
         * @param  number: an int16_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint16LSB_MSB(int16_t number);

        /**
         * @brief  This encodes a byte vector from an int32_LSB encoded number
         * @note   int32_LSB means unsigned 32-bit little-endian integer
         * @param  number: a uint32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt32LSB(uint32_t number);

        /**
         * @brief  This encodes a byte vector from an int32_MSB encoded number
         * @note   int32_MSB means unsigned 32-bit big-endian integer
         * @param  number: a uint32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt32MSB(uint32_t number);

        /**
         * @brief  This encodes a byte vector from an int32_LSB-MSB encoded number
         * @note   int32_LSB-MSB means unsigned 32-bit both-endian integer (little then big)
         * @param  number: a uint32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeInt32LSB_MSB(uint32_t number);

        /**
         * @brief  This encodes a byte vector from an sint32_LSB encoded number
         * @note   sint32_LSB means signed 32-bit little-endian integer
         * @param  number: an int32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint32LSB(int32_t number);

        /**
         * @brief  This encodes a byte vector from an sint32_MSB encoded number
         * @note   sint32_MSB means signed 32-bit big-endian integer
         * @param  number: an int32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint32MSB(int32_t number);

        /**
         * @brief  This encodes a byte vector from an sint32_LSB-MSB encoded number
         * @note   sint32_LSB-MSB means signed 32-bit both-endian integer (little then big)
         * @param  number: an int32_t number
         * @retval a byte vector
         */
        std::vector<char> encodeSint32LSB_MSB(int32_t number);

        /**
         * @brief  This gets a bit from a byte
         * @param  byte the byte
         * @param  pos the bit index
         * @return the requested bit
         */
        bool getBit(uint8_t byte, uint8_t pos);

        /**
         * @brief  This sets a bit inside a byte
         * @param  byte the byte
         * @param  pos the bit index
         * @param  bit the new bit
         * @return the new byte
         */
        uint8_t setBit(uint8_t byte, uint8_t pos, bool bit);

        /**
         * @brief  This converts a sector number to a byte address
         * @param  sector the sector number
         * @retval the address of the starting byte
         */
        uint32_t convertSectorToByteAddress(uint32_t sector);

        /**
         * @brief  This converts a byte address to a sector number
         * @param  address: a byte address
         * @retval the sector number containing the byte address
         */
        uint32_t convertByteAddressToSector(uint32_t address);

        /**
         * @brief  This generates a SHA256 checksum of a char vector
         * @param  vec: the vector to digest
         * @retval the SHA526 checksum of the vector
         */
        std::string sha256sum(std::vector<char> *vec);

        /**
         * @brief  This takes a vector of data and dumps it to a file
         * @param  vec: the data to write to the new file
         * @param  path: the path to the file to write
         * @param  error: becomes true if there was an error writing the file
         * @retval None
         */
        void dump_vector(std::vector<char> *vec, std::string path, bool *error);

        /**
         * @brief  This takes a file path and returns the contained data
         * @param  path: the path to the file to read
         * @param  error: becomes true of there was an error reading the file
         * @retval the bytes inside the loaded file
         */
        std::vector<char> load_vector(std::string path, bool *error);
    }
}
