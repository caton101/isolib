/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "FileFlags.hpp"
#include "definitions.hpp"
#include "utils.hpp"
#include <string>
#include <cstring>

isochop::FileFlags::FileFlags()
{
    this->data = 0;
}

isochop::FileFlags::FileFlags(uint8_t data)
{
    this->data = data;
}

isochop::FileFlags::~FileFlags()
{
    // valgrind does not report any leaks at this time
}

std::string isochop::FileFlags::toString()
{
    std::string str;
    str += "{";
    str += "Is Hidden: " + std::to_string(this->getHidden()) + ", ";
    str += "Is Directory: " + std::to_string(this->getIsDirectory()) + ", ";
    str += "Is Associated: " + std::to_string(this->getIsAssociatedFile()) + ", ";
    str += "Has Format Info: " + std::to_string(this->getHasFormatInformation()) + ", ";
    str += "Has Permissions: " + std::to_string(this->getArePermissionsInExtendedAttributeRecord()) + ", ";
    str += "Reserved 5: " + std::to_string(this->getReservedBit5()) + ", ";
    str += "Reserved 6: " + std::to_string(this->getReservedBit6()) + ", ";
    str += "More Records: " + std::to_string(this->getNotFinalRecord());
    str += "}";
    return str;
}

uint8_t isochop::FileFlags::getData()
{
    return this->data;
}

void isochop::FileFlags::setData(uint8_t data)
{
    this->data = data;
}

bool isochop::FileFlags::getHidden()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_HIDDEN);
}

void isochop::FileFlags::setHidden(bool hidden)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_HIDDEN, hidden);
}

bool isochop::FileFlags::getIsDirectory()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_DIRECTORY);
}

void isochop::FileFlags::setIsDirectory(bool isDirectory)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_DIRECTORY, isDirectory);
}

bool isochop::FileFlags::getIsAssociatedFile()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_ASSOCIATED);
}

void isochop::FileFlags::setIsAssociatedFile(bool isAssociated)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_ASSOCIATED, isAssociated);
}

bool isochop::FileFlags::getHasFormatInformation()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_FORMAT);
}

void isochop::FileFlags::setHasFormatInformation(bool hasFormatInformation)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_FORMAT, hasFormatInformation);
}

bool isochop::FileFlags::getArePermissionsInExtendedAttributeRecord()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_PERMISSIONS);
}

void isochop::FileFlags::setArePermissionsInExtendedAttributeRecord(bool arePermissionsInExtendedAttributeRecord)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_PERMISSIONS, arePermissionsInExtendedAttributeRecord);
}

bool isochop::FileFlags::getReservedBit5()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_RESERVED_5);
}

void isochop::FileFlags::setReservedBit5(bool bit)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_RESERVED_5, bit);
}

bool isochop::FileFlags::getReservedBit6()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_RESERVED_6);
}

void isochop::FileFlags::setReservedBit6(bool bit)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_RESERVED_6, bit);
}

bool isochop::FileFlags::getNotFinalRecord()
{
    return utils::getBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_FINAL);
}

void isochop::FileFlags::setNotFinalRecord(bool notFinalRecord)
{
    this->data = utils::setBit(this->data, ISOCHOP_FILEFLAGS_OFFSET_FINAL, notFinalRecord);
}