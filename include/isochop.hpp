/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <vector>
#include <string>
#include "definitions.hpp"
#include "ISO.hpp"
#include "VolumeDescriptor.hpp"
#include "ExtentTree.hpp"
#include "Orphan.hpp"

// put code behind library namespace
namespace isochop
{
    /**
     * @brief  This class represents the ISOChop library
     */
    class ISOChop
    {
    private:
        // this controls access to the underlying ISOChop data structures
        bool doFunStuff;
        // stores the status of the embedded ISO object
        bool populated;
        // this stores how the ISO was loaded
        int how;
        // the stored ISO object to proxy
        ISO iso;

    public:
        /**
         * @brief  This makes an empty ISOChop object
         * @retval a new ISOChop object
         */
        ISOChop();

        /**
         * @brief  This makes an ISOChop object by reading a file or directory
         * @param  path: the path to an ISO file or an exported directory archive
         * @retval a new ISOChop object
         */
        ISOChop(std::string path);

        /**
         * @brief  This makes an ISOChop object by reading a file or directory
         * @param  path: the path to an ISO file or an exported directory archive
         * @param  status_callback: a function to handle status updates while loading information
         * @param  result: the result code from the appropriate loader (look at ISO_PARSE_RET_* or ISO_IMPORT_RET_* in definitions.hpp)
         * @retval a new ISOChop object
         */
        ISOChop(std::string path, void status_callback(int code), int *result);

        /**
         * @brief  This makes an ISOChop object by reading a file or directory
         * @param  path: the path to an ISO file or an exported directory archive
         * @param  status_callback_file: a function to handle status updates if path is a file
         * @param  status_callback_directory: a function to handle status updates if path is a directory
         * @param  result: the result code from the appropriate loader (look at ISO_PARSE_RET_* or ISO_IMPORT_RET_* in definitions.hpp)
         * @retval a new ISOChop object
         */
        ISOChop(std::string path, void status_callback_file(int code), void status_callback_directory(int code), int *result);

        /**
         * @brief  Check an exported directory structure for issues
         * @param  dirpath: the directory path where all exported files are stored
         * @retval true of the exported directory structure is valid
         */
        static bool directoryValid(std::string dirpath);

        /**
         * @brief  This checks which loader will be used to load the ISO
         * @param  path: file or directory to read
         * @retval the result code from checking (see ISO_LOADER_* in definitions.hpp)
         */
        int whichLoader(std::string path);

        /**
         * @brief  This intelligently loads the ISO object using an ISO file or an exported directory
         * @param  path: file or directory to read
         * @param  status_callback: a function to call with loading updates
         * @retval the result code from parsing (see ISO_PARSE_RET_* or ISO_IMPORT_RET_* in definitions.hpp)
         */
        int loadISO(std::string path, void status_callback(int code));

        /**
         * @brief  This intelligently loads the ISO object using an ISO file or an exported directory
         * @param  path: file or directory to read
         * @param  status_callback_file: a function to call with loading updates (for files)
         * @param  status_callback_directory: a function to call with loading updates (for directories)
         * @retval the result code from parsing (see ISO_PARSE_RET_* or ISO_IMPORT_RET_* in definitions.hpp)
         */
        int loadISO(std::string path, void status_callback_file(int code), void status_callback_directory(int code));

        /**
         * @brief  This generates a string explaining the return code for loadISO
         * @param  status: the return code from loadISO
         * @retval an explanation of the result code
         */
        std::string explainLoadResult(int status);

        /**
         * @brief  This generates a string explaining the return code for makeFile
         * @param  status: the return code from makeFile
         * @retval an explanation of the result code
         */
        std::string explainMakeFileResult(int status);

        /**
         * @brief  Write ISO data to a file
         * @param  filepath: file to write
         * @param  status_callback: a function to call with writing updates
         * @retval the result code from writing (see ISO_WRITE_RET_* in definitions.hpp)
         */
        int makeFile(std::string filepath, void status_callback(int code));

        /**
         * @brief  Export all ISO data into a directory structure
         * @param  dirpath: the directory path where all exported files will be stored
         * @param  status_callback: a function to call with exporting updates
         * @retval the SHA256 checksum of this ISO's manifest
         */
        std::string makeDirectory(std::string dirpath, void status_callback(int code));

        /**
         * @brief  Get a graphical tree of the entire file structure
         * @note   do not rely on the output format for parsing
         * @param  indent: the number of spaces per indentation level
         * @retval a graphical tree of the entire ExtentNode structure
         */
        std::string tree(size_t indent);

        /**
         * @brief  Get a listing of all volume descriptors and their fields
         * @note   do not rely on the output format
         * @retval a listing of all volume descriptors and their fields
         */
        std::string showVolumeDescriptors();

        /**
         * @brief  Get the extent information for the loaded ISO
         * @note   do not rely on the output format
         * @retval the output of ExtentTree's toString method
         */
        std::string showExtentInfo();

        /**
         * @brief  Check if the underlying ISO is ready for operations
         * @retval true if the ISO is ready for operations
         */
        bool good();

        /**
         * @brief  Check if the underlying ISO methods are visible
         * @retval true if the underlying ISO methods are visible
         */
        bool fun();

        /**
         * @brief  Enable or disable access to underlying ISO methods
         * @param  enabled: enables access to underlying ISO methods
         * @retval None
         */
        void setFun(bool enabled);

        /**
         * @brief  Get the internal ISO object
         * @retval the internal ISO object
         */
        ISO getISO();

        /**
         * @brief  Set the internal ISO object
         * @retval None
         */
        void setISO(ISO iso);

        /**
         * @brief  Get the system area
         * @note   array is of length SYSTEM_AREA_SIZE
         * @retval a pointer to the system area array
         */
        std::vector<char> getSystemArea();

        /**
         * @brief  Set the system area
         * @note   vector must be of length SYSTEM_AREA_SIZE
         * @param  data: the system area data
         * @retval None
         */
        void setSystemArea(std::vector<char> data);

        /**
         * @brief  This returns a pointer to the ISO's volume descriptors
         * @note   these are in-order as on disc
         * @retval a pointer to the ISO's volume descriptors
         */
        std::vector<VolumeDescriptor *> *getVolumeDescriptors();

        /**
         * @brief  This sets ISO's volume descriptors from a pointer
         * @note   these should be in-order as on disc
         * @param  vds: a pointer to volume descriptors
         * @retval None
         */
        void setVolumeDescriptors(std::vector<VolumeDescriptor *> *vds);

        /**
         * @brief  This returns the extent tree
         * @note   this represents the entire filesystem
         * @retval the ExtentTree used by the ISO object
         */
        ExtentTree getTree();

        /**
         * @brief  This sets the extent tree
         * @note   this represents the entire filesystem
         * @param  tree: the ExtentTree to be used by the ISO object
         * @retval None
         */
        void setTree(ExtentTree tree);

        /**
         * @brief  This returns all orphaned data in the ISO object
         * @retval all orphaned data in the ISO object
         */
        std::vector<Orphan> getOrphans();

        /**
         * @brief  This sets the orphaned data in the ISO object
         * @param  orphans: the new orphaned data in the ISO object
         * @retval None
         */
        void setOrphans(std::vector<Orphan> orphans);

        /**
         * @brief  This fetches the size of the ISO image
         * @retval the size of the ISO image
         */
        unsigned long getSize();

        /**
         * @brief  This sets the size of the ISO image
         * @param  size: the new size of the ISO image
         * @retval None
         */
        void setSize(unsigned long size);
    };
}