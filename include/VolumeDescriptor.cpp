/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <vector>
#include <fstream>
#include "json/single_include/nlohmann/json.hpp"
#include "VolumeDescriptor.hpp"
#include "definitions.hpp"
#include "utils.hpp"

isochop::VolumeDescriptor::VolumeDescriptor()
{
    // set address
    this->address = 0;
    // initialize vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    this->data = data;
    // set some main information
    this->data[0] = (unsigned char)ISOCHOP_VD_TYPE_TERMINATOR;
    this->data[1] = 'C';
    this->data[2] = 'D';
    this->data[3] = '0';
    this->data[4] = '0';
    this->data[5] = '1';
    this->data[6] = 0x1;
}

isochop::VolumeDescriptor::VolumeDescriptor(const std::vector<char> &raw_data, unsigned long address)
{
    if (raw_data.size() != ISOCHOP_VD_SIZE_TOTAL)
    {
        throw std::invalid_argument("raw_data.size() != ISOCHOP_VD_TOTAL_SIZE");
    }
    // parse information into object
    this->setData(raw_data);
    // set the address
    this->address = address;
}

isochop::VolumeDescriptor::VolumeDescriptor(const std::string &dirpath, const std::string &manifesthash)
{
    // load volume descriptor from an exported directory structure
    this->import_data(dirpath, manifesthash);
}

bool isochop::VolumeDescriptor::isValid()
{
    // NOTE: all types are technically correct
    // check if identifier is correct
    if (this->getIdentifier() != "CD001")
    {
        return false;
    }
    // check if version is correct
    if (this->getVersion() != 1)
    {
        return false;
    }
    // all information is valid
    return true;
}

std::string isochop::VolumeDescriptor::toString()
{
    std::string str;
    str += "Address: " + std::to_string(this->getAddress()) + "\n";
    str += "Type: " + std::to_string(this->getType()) + "\n";
    str += "Identifier: ";
    str += this->getIdentifier();
    str += "\n";
    str += "Version: " + std::to_string(this->getVersion());
    return str;
}

isochop::VolumeDescriptor *isochop::VolumeDescriptor::clone()
{
    // make container
    VolumeDescriptor *vd = new VolumeDescriptor();
    // deep copy data
    vd->setData(this->getData());
    // return the new volume descriptor
    return vd;
}

unsigned long isochop::VolumeDescriptor::getAddress()
{
    return this->address;
}

void isochop::VolumeDescriptor::setAddress(unsigned long address)
{
    this->address = address;
}

unsigned char isochop::VolumeDescriptor::getType()
{
    return this->data[0];
}

void isochop::VolumeDescriptor::setType(unsigned char type)
{
    this->data[0] = type;
}

std::string isochop::VolumeDescriptor::getIdentifier()
{
    return utils::extractStringFromVector(this->data, 1, ISOCHOP_VD_SIZE_IDENTIFIER);
}

void isochop::VolumeDescriptor::setIdentifier(const std::string &identifier)
{
    if (identifier.size() != ISOCHOP_VD_SIZE_IDENTIFIER)
    {
        throw std::invalid_argument("identifier.size() != ISOCHOP_VD_IDENTIFIER_SIZE");
    }
    std::vector<char> id = utils::extractVectorFromString(identifier, 0, identifier.size());
    utils::copyVector(&id, &this->data, 0, 1, ISOCHOP_VD_SIZE_IDENTIFIER);
}

char isochop::VolumeDescriptor::getVersion()
{
    return this->data[6];
}

void isochop::VolumeDescriptor::setVersion(char version)
{
    this->data[6] = version;
}

std::vector<char> isochop::VolumeDescriptor::getData()
{
    return this->data;
}

void isochop::VolumeDescriptor::setData(const std::vector<char> &data)
{
    // check for size error
    if (data.size() != ISOCHOP_VD_SIZE_TOTAL)
    {
        throw std::invalid_argument("data.size() != ISOCHOP_VD_TOTAL_SIZE");
    }
    // clear data array
    this->data = data;
}

std::string isochop::VolumeDescriptor::export_data(const std::string &dirpath)
{
    // get the SHA256 hash of the data and store it
    std::string sha256data = utils::sha256sum(&this->data);
    utils::dump_vector(&this->data, dirpath + "/" + sha256data, nullptr);
    // generate manifest json
    nlohmann::json manifestJSON;
    manifestJSON["manifest_class"] = "VolumeDescriptor";
    manifestJSON["data_hash"] = sha256data;
    manifestJSON["byte_address"] = this->address;
    // generate manifest string
    std::string manifestString = manifestJSON.dump(4, ' ', true);
    // calculate manifest hash
    std::vector<char> tmp;
    for (char c : manifestString)
    {
        tmp.push_back(c);
    }
    std::string manifestSHA256 = utils::sha256sum(&tmp);
    tmp.clear();
    // write JSON to file
    std::fstream file;
    file.open(dirpath + "/" + manifestSHA256, std::fstream::out);
    file << manifestString;
    file.close();
    // return this VolumeDescriptor's SHA256 hash
    return manifestSHA256;
}

void isochop::VolumeDescriptor::import_data(const std::string &dirpath, const std::string &manifesthash)
{
    // parse manifest file
    std::fstream manifestFile;
    manifestFile.open(dirpath + "/" + manifesthash, std::fstream::in);
    nlohmann::json manifestJSON;
    manifestFile >> manifestJSON;
    manifestFile.close();
    // load data from file
    unsigned long addr = manifestJSON["byte_address"];
    std::string data_hash = manifestJSON["data_hash"];
    // set fields
    this->address = addr;
    this->data = utils::load_vector(dirpath + "/" + data_hash, nullptr);
}

void isochop::VolumeDescriptor::markAddresses(std::vector<bool> *bits)
{
    if (bits == nullptr)
    {
        throw std::invalid_argument("bits == nullptr");
    }
    for (unsigned long i = this->address; i < this->address + this->data.size(); i++)
    {
        bits->at(i) = true;
    }
}
