/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// only include once
#pragma once

// includes
#include <cstdint>
#include <string>

// put code behind library namespace
namespace isochop
{
    /**
     * @brief This class implements a FileFlags entry
     * @note  all fields and methods follow the ISO 9660 specification
     */
    class FileFlags
    {
    protected:
        // this stores the fileflags information
        uint8_t data;

    public:
        /**
         * @brief  The no-args constructor for a FileFlags object
         * @note   default values may not be valid
         * @retval a new FileFlags object
         */
        FileFlags();

        /**
         * @brief  The normal constructor for a FileFlags object
         * @retval a new FileFlags object
         */
        FileFlags(uint8_t data);

        /**
         * @brief The deconstructor for a FileFlags object
         */
        ~FileFlags();

        /**
         * @brief  Generates a string summarizing the FileFlags
         * @note   string output may change format in future versions
         * @retval string representing the FileFlags
         */
        std::string toString();

        /**
         * @brief  Fetches the byte used to store file flags
         * @retval the byte containing file flags
         */
        uint8_t getData();

        /**
         * @brief  Sets the byte used to store file flags
         * @param  data: byte containing file flags
         */
        void setData(uint8_t data);

        /**
         * @brief  Fetch the hidden flag
         * @return true if the file is hidden
         */
        bool getHidden();

        /**
         * @brief Set the hidden flag
         * @param hidden the hidden flag
         */
        void setHidden(bool hidden);

        /**
         * @brief  Fetch the directory flag
         * @return true if the file is a directory
         */
        bool getIsDirectory();

        /**
         * @brief Set the directory flag
         * @param isDirectory the directory flag
         */
        void setIsDirectory(bool isDirectory);

        /**
         * @brief  Fetch the associated flag
         * @return true if the file is an associated file
         */
        bool getIsAssociatedFile();

        /**
         * @brief Set the associated flag
         * @param isAssociated the associated flag
         */
        void setIsAssociatedFile(bool isAssociated);

        /**
         * @brief  Fetch the format flag
         * @return true if the extended attribute record contains format information
         */
        bool getHasFormatInformation();

        /**
         * @brief Set the format flag
         * @param hasFormatInformation the format flag
         */
        void setHasFormatInformation(bool hasFormatInformation);

        /**
         * @brief  Fetch the permissions flag
         * @return true of the file permissions are in the extended attribute record
         */
        bool getArePermissionsInExtendedAttributeRecord();

        /**
         * @brief Set the permissions flag
         * @param arePermissionsInExtendedAttributeRecord the permissions flag
         */
        void setArePermissionsInExtendedAttributeRecord(bool arePermissionsInExtendedAttributeRecord);

        /**
         * @brief  Fetch the reserved bit in the 5th position
         * @return the reserved bit in the 5th position
         */
        bool getReservedBit5();

        /**
         * @brief Set the reserved bit in the 5th position
         * @param bit reserved bit
         */
        void setReservedBit5(bool bit);

        /**
         * @brief  Fetch the reserved bit in the 6th position
         * @return the reserved bit in the 6th position
         */
        bool getReservedBit6();

        /**
         * @brief Set the reserved bit in the 6th position
         * @param bit reserved bit
         */
        void setReservedBit6(bool bit);

        /**
         * @brief  Fetch the final flag
         * @return true if there is another record for this file
         */
        bool getNotFinalRecord();

        /**
         * @brief Set the final flag
         * @param notFinalRecord the final flag
         */
        void setNotFinalRecord(bool notFinalRecord);
    };
}