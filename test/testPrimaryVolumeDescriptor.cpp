/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testPrimaryVolumeDescriptor.hpp"
#include "../include/PrimaryVolumeDescriptor.hpp"
#include "../include/definitions.hpp"
#include "../include/utils.hpp"

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setaddress_getaddress()
{
    isochop::PrimaryVolumeDescriptor pmvd;
    unsigned long addr = 123;
    pmvd.setAddress(addr);
    return addr == pmvd.getAddress();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setdata_getdata()
{
    std::vector<char> data;
    data.reserve(ISOCHOP_VD_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        data.push_back((i % 255) + 1);
    }
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    std::vector<char> copy = pmvd.getData();
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (data[i] != copy[i])
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_gettype()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    char type = 76;
    data[0] = type;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    char checkMe = pmvd.getType();
    return checkMe == type;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_settype()
{
    char type = 76;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setType(type);
    std::vector<char> data = pmvd.getData();
    return data[0] == type;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getidentifier()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    std::string checkMe = pmvd.getIdentifier();
    return checkMe == "CAMER";
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setidentifier()
{
    std::string id = "CAMER";
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setIdentifier(id);
    std::vector<char> checkMe = pmvd.getData();
    bool isCorrect = true;
    isCorrect &= checkMe[1] == 'C';
    isCorrect &= checkMe[2] == 'A';
    isCorrect &= checkMe[3] == 'M';
    isCorrect &= checkMe[4] == 'E';
    isCorrect &= checkMe[5] == 'R';
    return isCorrect;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getversion()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    char version = 87;
    data[6] = version;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    char checkMe = pmvd.getVersion();
    return checkMe == version;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setversion()
{
    char version = 87;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setVersion(version);
    std::vector<char> raw = pmvd.getData();
    return raw[6] == version;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_1()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_3()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_4()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[6] = 1;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_5()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_6()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_7()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_8()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_9()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_10()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_11()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_12()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_13()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_14()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_15()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_16()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = 69;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_17()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_18()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 69;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_19()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_20()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_21()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_22()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_23()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_24()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_25()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_26()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_27()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_isvalid_28()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 69;
    // make and test
    isochop::PrimaryVolumeDescriptor pmvd;
    pmvd.setData(data);
    return !pmvd.isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_clone()
{
    std::vector<char> raw(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        raw[i] = (i % 255) + 1;
    }
    isochop::PrimaryVolumeDescriptor *pmvd1 = new isochop::PrimaryVolumeDescriptor();
    pmvd1->setData(raw);
    isochop::PrimaryVolumeDescriptor *pmvd2 = (isochop::PrimaryVolumeDescriptor *)pmvd1->clone();
    std::vector<char> checkData = pmvd2->getData();
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (checkData[i] != raw[i])
        {
            return false;
        }
    }
    bool fieldsMatch = true;
    fieldsMatch &= pmvd1->getType() == pmvd2->getType();
    fieldsMatch &= pmvd1->getIdentifier() == pmvd2->getIdentifier();
    fieldsMatch &= pmvd1->getVersion() == pmvd2->getVersion();
    return fieldsMatch;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    return pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 123);
    return pmvd->getAddress() == 123;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_3()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_4()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_5()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data[6] = 1;
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_6()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_7()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_8()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_9()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_10()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_11()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_12()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_13()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_14()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::string testStr;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    std::vector<char> tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_15()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_16()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_17()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = 69;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_18()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'A';
    data[3] = 'M';
    data[4] = 'E';
    data[5] = 'R';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_19()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 69;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_20()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_21()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_22()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_23()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_24()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_25()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_26()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_27()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_28()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    testStr[0] = '\t';
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 1;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_constructor_29()
{
    // make vars for later
    std::string testStr;
    std::vector<char> tmp;
    // create the data vector
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    // add volume descriptor type
    data[0] = ISOCHOP_VD_TYPE_PRIMARY;
    // add volume descriptor identifier
    data[1] = 'C';
    data[2] = 'D';
    data[3] = '0';
    data[4] = '0';
    data[5] = '1';
    // add volume descriptor version
    data[6] = 1;
    // add system identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    // add volume identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    // add volume set identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    // add publisher identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    // add data preparer identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    // add application identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    // add copyright file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    // add abstract file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    // add biblographic file identifier
    testStr = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        testStr += 'A' + (i % 26);
    }
    tmp = isochop::utils::extractVectorFromString(testStr, 0, testStr.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    // add file structure version
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = 69;
    // make and test
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);

    return !pmvd->isValid();
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_markaddresses_1()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 1);
    isochop::PrimaryVolumeDescriptor pmvd = isochop::PrimaryVolumeDescriptor(data, 5);
    try
    {
        pmvd.markAddresses(nullptr);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_markaddresses_2()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 1);
    std::vector<bool> bits(ISOCHOP_VD_SIZE_TOTAL + 100, 0);
    isochop::PrimaryVolumeDescriptor pmvd = isochop::PrimaryVolumeDescriptor(data, 5);
    pmvd.markAddresses(&bits);
    for (int i = 0; i < 5; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (!bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5 + ISOCHOP_VD_SIZE_TOTAL; i < ISOCHOP_VD_SIZE_TOTAL + 100; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getsystemidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER, ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getSystemIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setsystemidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setSystemIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setsystemidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setSystemIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_SYSTEM_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_SYSTEM_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumeidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getVolumeIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumeidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setVolumeIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumeidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setVolumeIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumespacesize()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB_MSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SPACE_SIZE, 8);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getVolumeSpaceSize();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumespacesize()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setVolumeSpaceSize(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB_MSB(0x87654321);
    for (int i = 0; i < 8; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_SPACE_SIZE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumesetsize()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_SIZE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint16_t out = pmvd->getVolumeSetSize();
    return out == 0x8765;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumesetsize()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setVolumeSetSize(0x8765);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_SET_SIZE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumesequencenumber()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SEQUENCE_NUMBER, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint16_t out = pmvd->getVolumeSequenceNumber();
    return out == 0x8765;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumesequencenumber()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setVolumeSequenceNumber(0x8765);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_SEQUENCE_NUMBER) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getlogicalblocksize()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_LOGICAL_BLOCK_SIZE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint16_t out = pmvd->getLogicalBlockSize();
    return out == 0x8765;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setlogicalblocksize()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setLogicalBlockSize(0x8765);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt16LSB_MSB(0x8765);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_LOGICAL_BLOCK_SIZE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getpathtablesize()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB_MSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_PATH_TABLE_SIZE, 8);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getPathTableSize();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setpathtablesize()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setPathTableSize(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB_MSB(0x87654321);
    for (int i = 0; i < 8; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_PATH_TABLE_SIZE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getlocationoftypelpathtable()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_L_PATH_TABLE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getLocationOfTypeLPathTable();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setlocationoftypelpathtable()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setLocationOfTypeLPathTable(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_L_PATH_TABLE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getlocationofoptionaltypelpathtable()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_L_PATH_TABLE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getLocationOfOptionalTypeLPathTable();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setlocationofoptionaltypelpathtable()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setLocationOfOptionalTypeLPathTable(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_L_PATH_TABLE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getlocationoftypempathtable()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_M_PATH_TABLE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getLocationOfTypeMPathTable();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setlocationoftypempathtable()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setLocationOfTypeMPathTable(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_LOCATION_OF_TYPE_M_PATH_TABLE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getlocationofoptionaltypempathtable()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_M_PATH_TABLE, 4);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint32_t out = pmvd->getLocationOfOptionalTypeMPathTable();
    return out == 0x87654321;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setlocationofoptionaltypempathtable()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setLocationOfOptionalTypeMPathTable(0x87654321);
    std::vector<char> data = pmvd->getData();
    std::vector<char> ans = isochop::utils::encodeInt32LSB(0x87654321);
    for (int i = 0; i < 4; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_LOCATION_OF_OPTIONAL_TYPE_M_PATH_TABLE) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getrootdirectoryentry()
{
    std::vector<char> data;
    data.reserve(ISOCHOP_VD_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        data.push_back((i % 255) + 1);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    isochop::Directory rde = pmvd->getRootDirectoryEntry();
    std::vector<char> out = rde.getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY; i++)
    {
        if (out.at(i) != data.at(i + ISOCHOP_PMVD_OFFSET_ROOT_DIRECTORY_ENTRY))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setrootdirectoryentry()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> rdev;
    rdev.reserve(ISOCHOP_VD_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        rdev.push_back((i % 255) + 1);
    }
    isochop::Directory rde = isochop::Directory(rdev);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    pmvd->setRootDirectoryEntry(rde);
    std::vector<char> out = pmvd->getData();
    isochop::utils::copyVector(&rdev, &data, 0, ISOCHOP_PMVD_OFFSET_ROOT_DIRECTORY_ENTRY, ISOCHOP_PMVD_SIZE_ROOT_DIRECTORY_ENTRY);
    for (int i = 0; i < ISOCHOP_VD_SIZE_TOTAL; i++)
    {
        if (out.at(i) != data.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumesetidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER, ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getVolumeSetIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumesetidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setVolumeSetIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumesetidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setVolumeSetIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_VOLUME_SET_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_SET_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getpublisheridentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER, ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getPublisherIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setpublisheridentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setPublisherIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setpublisheridentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setPublisherIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_PUBLISHER_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_PUBLISHER_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getdataprepareridentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER, ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getDataPreparerIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setdataprepareridentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setDataPreparerIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setdataprepareridentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setDataPreparerIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_DATA_PREPARER_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_DATA_PREPARER_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getapplicationidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER, ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getApplicationIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setapplicationidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setApplicationIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setapplicationidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setApplicationIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_APPLICATION_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getcopyrightfileidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getCopyrightFileIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setcopyrightfileidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setCopyrightFileIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setcopyrightfileidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setCopyrightFileIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_COPYRIGHT_FILE_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_COPYRIGHT_FILE_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getabstractfileidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getAbstractFileIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setabstractfileidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setAbstractFileIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setabstractfileidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setAbstractFileIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_ABSTRACT_FILE_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_ABSTRACT_FILE_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getbiblographicfileidentifier()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> tmp = isochop::utils::extractVectorFromString(id, 0, id.size());
    isochop::utils::copyVector(&tmp, &data, 0, ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER, ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::string out = pmvd->getBiblographicFileIdentifier();
    return out == id;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setbiblographicfileidentifier_1()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    try
    {
        pmvd->setBiblographicFileIdentifier("");
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setbiblographicfileidentifier_2()
{
    std::string id = "";
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        id += 'A' + (i % 26);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setBiblographicFileIdentifier(id);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_BIBLOGRAPHIC_FILE_IDENTIFIER; i++)
    {
        if (out.at(i + ISOCHOP_PMVD_OFFSET_BIBLOGRAPHIC_FILE_IDENTIFIER) != id.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumecreationdateandtime()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME) = (i % 255) + 1;
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    isochop::DecDatetime dd = pmvd->getVolumeCreationDateAndTime();
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumecreationdateandtime()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    std::vector<char> in;
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        in.push_back((i % 255) + 1);
    }
    isochop::DecDatetime dd = isochop::DecDatetime(in);
    pmvd->setVolumeCreationDateAndTime(dd);
    std::vector<char> data = pmvd->getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_CREATION_DATE_AND_TIME) != in.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumemodificationdateandtime()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME) = (i % 255) + 1;
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    isochop::DecDatetime dd = pmvd->getVolumeModificationDateAndTime();
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumemodificationdateandtime()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    std::vector<char> in;
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        in.push_back((i % 255) + 1);
    }
    isochop::DecDatetime dd = isochop::DecDatetime(in);
    pmvd->setVolumeModificationDateAndTime(dd);
    std::vector<char> data = pmvd->getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_MODIFICATION_DATE_AND_TIME) != in.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumeexpirationdateandtime()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME) = (i % 255) + 1;
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    isochop::DecDatetime dd = pmvd->getVolumeExpirationDateAndTime();
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumeexpirationdateandtime()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    std::vector<char> in;
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        in.push_back((i % 255) + 1);
    }
    isochop::DecDatetime dd = isochop::DecDatetime(in);
    pmvd->setVolumeExpirationDateAndTime(dd);
    std::vector<char> data = pmvd->getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EXPIRATION_DATE_AND_TIME) != in.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getvolumeeffectivedateandtime()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME) = (i % 255) + 1;
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    isochop::DecDatetime dd = pmvd->getVolumeEffectiveDateAndTime();
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setvolumeeffectivedateandtime()
{
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    std::vector<char> in;
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        in.push_back((i % 255) + 1);
    }
    isochop::DecDatetime dd = isochop::DecDatetime(in);
    pmvd->setVolumeEffectiveDateAndTime(dd);
    std::vector<char> data = pmvd->getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (data.at(i + ISOCHOP_PMVD_OFFSET_VOLUME_EFFECTIVE_DATE_AND_TIME) != in.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getfilestructureversion()
{
    uint8_t ans = 24;
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION) = ans;
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    uint8_t out = pmvd->getFileStructureVersion();
    return out == ans;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setfilestructureversion()
{
    uint8_t ans = 24;
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setFileStructureVersion(ans);
    std::vector<char> data = pmvd->getData();
    uint8_t out = data.at(ISOCHOP_PMVD_OFFSET_FILE_STRUCTURE_VERSION);
    return out == ans;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getapplicationused()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_USED; i++)
    {
        ans.push_back((i % 255) + 1);
    }
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_APPLICATION_USED, ISOCHOP_PMVD_SIZE_APPLICATION_USED);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::vector<char> out = pmvd->getApplicationUsed();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_USED; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setapplicationused()
{
    std::vector<char> ans;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_USED; i++)
    {
        ans.push_back((i % 255) + 1);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setApplicationUsed(ans);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_APPLICATION_USED; i++)
    {
        if (ans.at(i) != out.at(i + ISOCHOP_PMVD_OFFSET_APPLICATION_USED))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_getreserved()
{
    std::vector<char> data(ISOCHOP_VD_SIZE_TOTAL, 0);
    std::vector<char> ans;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_RESERVED; i++)
    {
        ans.push_back((i % 255) + 1);
    }
    isochop::utils::copyVector(&ans, &data, 0, ISOCHOP_PMVD_OFFSET_RESERVED, ISOCHOP_PMVD_SIZE_RESERVED);
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor(data, 0);
    std::vector<char> out = pmvd->getReserved();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_RESERVED; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testPrimaryVolumeDescriptor::test_primaryvolumedescriptor_setreserved()
{
    std::vector<char> ans;
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_RESERVED; i++)
    {
        ans.push_back((i % 255) + 1);
    }
    isochop::PrimaryVolumeDescriptor *pmvd = new isochop::PrimaryVolumeDescriptor();
    pmvd->setReserved(ans);
    std::vector<char> out = pmvd->getData();
    for (int i = 0; i < ISOCHOP_PMVD_SIZE_RESERVED; i++)
    {
        if (ans.at(i) != out.at(i + ISOCHOP_PMVD_OFFSET_RESERVED))
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testPrimaryVolumeDescriptor::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_primaryvolumedescriptor_setaddress_getaddress, "PrimaryVolumeDescriptor.cpp: setAddress/getAddress (symmetry");
    pushTest(&tests, test_primaryvolumedescriptor_setdata_getdata, "PrimaryVolumeDescriptor.cpp: setData/getData (symmetry)");
    pushTest(&tests, test_primaryvolumedescriptor_gettype, "PrimaryVolumeDescriptor.cpp: getType");
    pushTest(&tests, test_primaryvolumedescriptor_settype, "PrimaryVolumeDescriptor.cpp: setType");
    pushTest(&tests, test_primaryvolumedescriptor_getidentifier, "PrimaryVolumeDescriptor.cpp: getIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setidentifier, "PrimaryVolumeDescriptor.cpp: setIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_getversion, "PrimaryVolumeDescriptor.cpp: getVersion");
    pushTest(&tests, test_primaryvolumedescriptor_setversion, "PrimaryVolumeDescriptor.cpp: setVersion");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_1, "PrimaryVolumeDescriptor.cpp: isValid (empty)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_2, "PrimaryVolumeDescriptor.cpp: isValid (only type)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_3, "PrimaryVolumeDescriptor.cpp: isValid (only identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_4, "PrimaryVolumeDescriptor.cpp: isValid (only version)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_5, "PrimaryVolumeDescriptor.cpp: isValid (only system identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_6, "PrimaryVolumeDescriptor.cpp: isValid (only volume identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_7, "PrimaryVolumeDescriptor.cpp: isValid (only set identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_8, "PrimaryVolumeDescriptor.cpp: isValid (only publisher identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_9, "PrimaryVolumeDescriptor.cpp: isValid (only data preparer identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_10, "PrimaryVolumeDescriptor.cpp: isValid (only application identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_11, "PrimaryVolumeDescriptor.cpp: isValid (only copyright file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_12, "PrimaryVolumeDescriptor.cpp: isValid (only abstract file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_13, "PrimaryVolumeDescriptor.cpp: isValid (only biblographic file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_14, "PrimaryVolumeDescriptor.cpp: isValid (only file structure version)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_15, "PrimaryVolumeDescriptor.cpp: isValid (legal header)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_16, "PrimaryVolumeDescriptor.cpp: isValid (illegal type)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_17, "PrimaryVolumeDescriptor.cpp: isValid (illegal identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_18, "PrimaryVolumeDescriptor.cpp: isValid (illegal version)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_19, "PrimaryVolumeDescriptor.cpp: isValid (illegal system identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_20, "PrimaryVolumeDescriptor.cpp: isValid (illegal volume identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_21, "PrimaryVolumeDescriptor.cpp: isValid (illegal volume set identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_22, "PrimaryVolumeDescriptor.cpp: isValid (illegal publisher identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_23, "PrimaryVolumeDescriptor.cpp: isValid (illegal data preparer identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_24, "PrimaryVolumeDescriptor.cpp: isValid (illegal application identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_25, "PrimaryVolumeDescriptor.cpp: isValid (illegal copyright file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_26, "PrimaryVolumeDescriptor.cpp: isValid (illegal abstract file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_27, "PrimaryVolumeDescriptor.cpp: isValid (illegal biblographic file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_isvalid_28, "PrimaryVolumeDescriptor.cpp: isValid (illegal file structure version)");
    pushTest(&tests, test_primaryvolumedescriptor_clone, "PrimaryVolumeDescriptor.cpp: clone");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_1, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (no-args)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_2, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (address)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_3, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (empty)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_4, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_5, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only version)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_6, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only system identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_7, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only volume identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_8, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only set identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_9, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only publisher identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_10, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only data preparer identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_11, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only application identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_12, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only copyright file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_13, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only abstract file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_14, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only biblographic file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_15, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (only file structure version)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_16, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (legal header)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_17, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal type)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_18, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_19, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal version)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_20, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal system identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_21, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal volume identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_22, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal volume set identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_23, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal publisher identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_24, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal data preparer identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_25, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal application identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_26, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal copyright file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_27, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal abstract file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_28, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal biblographic file identifier)");
    pushTest(&tests, test_primaryvolumedescriptor_constructor_29, "PrimaryVolumeDescriptor.cpp: PrimaryVolumeDescriptor (illegal file structure version)");
    pushTest(&tests, test_primaryvolumedescriptor_markaddresses_1, "PrimaryVolumeDescriptor.cpp: markAddresses (nullptr)");
    pushTest(&tests, test_primaryvolumedescriptor_markaddresses_2, "PrimaryVolumeDescriptor.cpp: markAddresses (data)");
    pushTest(&tests, test_primaryvolumedescriptor_getsystemidentifier, "PrimaryVolumeDescriptor.cpp: getSystemIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setsystemidentifier_1, "PrimaryVolumeDescriptor.cpp: setSystemIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setsystemidentifier_2, "PrimaryVolumeDescriptor.cpp: setSystemIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumeidentifier, "PrimaryVolumeDescriptor.cpp: getVolumeIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumeidentifier_1, "PrimaryVolumeDescriptor.cpp: setVolumeIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumeidentifier_2, "PrimaryVolumeDescriptor.cpp: setVolumeIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumespacesize, "PrimaryVolumeDescriptor.cpp: getVolumeSpaceSize");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumespacesize, "PrimaryVolumeDescriptor.cpp: setVolumeSpaceSize");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumesetsize, "PrimaryVolumeDescriptor.cpp: getVolumeSetSize");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumesetsize, "PrimaryVolumeDescriptor.cpp: setVolumeSetSize");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumesequencenumber, "PrimaryVolumeDescriptor.cpp: getVolumeSequenceNumber");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumesequencenumber, "PrimaryVolumeDescriptor.cpp: setVolumeSequenceNumber");
    pushTest(&tests, test_primaryvolumedescriptor_getlogicalblocksize, "PrimaryVolumeDescriptor.cpp: getLogicalBlockSize");
    pushTest(&tests, test_primaryvolumedescriptor_setlogicalblocksize, "PrimaryVolumeDescriptor.cpp: setLogicalBlockSize");
    pushTest(&tests, test_primaryvolumedescriptor_getpathtablesize, "PrimaryVolumeDescriptor.cpp: getPathTableSize");
    pushTest(&tests, test_primaryvolumedescriptor_setpathtablesize, "PrimaryVolumeDescriptor.cpp: setPathTableSize");
    pushTest(&tests, test_primaryvolumedescriptor_getlocationoftypelpathtable, "PrimaryVolumeDescriptor.cpp: getLocationOfTypeLPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_setlocationoftypelpathtable, "PrimaryVolumeDescriptor.cpp: setLocationOfTypeLPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_getlocationofoptionaltypelpathtable, "PrimaryVolumeDescriptor.cpp: getLocationOfOptionalTypeLPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_setlocationofoptionaltypelpathtable, "PrimaryVolumeDescriptor.cpp: setLocationOfOptionalTypeLPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_getlocationoftypempathtable, "PrimaryVolumeDescriptor.cpp: getLocationOfTypeMPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_setlocationoftypempathtable, "PrimaryVolumeDescriptor.cpp: setLocationOfTypeMPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_getlocationofoptionaltypempathtable, "PrimaryVolumeDescriptor.cpp: getLocationOfOptionalTypeMPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_setlocationofoptionaltypempathtable, "PrimaryVolumeDescriptor.cpp: setLocationOfOptionalTypeMPathTable");
    pushTest(&tests, test_primaryvolumedescriptor_getrootdirectoryentry, "PrimaryVolumeDescriptor.cpp: getRootDirectoryEntry");
    pushTest(&tests, test_primaryvolumedescriptor_setrootdirectoryentry, "PrimaryVolumeDescriptor.cpp: setRootDirectoryEntry");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumesetidentifier, "PrimaryVolumeDescriptor.cpp: getVolumeSetIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumesetidentifier_1, "PrimaryVolumeDescriptor.cpp: setVolumeSetIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumesetidentifier_2, "PrimaryVolumeDescriptor.cpp: setVolumeSetIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getpublisheridentifier, "PrimaryVolumeDescriptor.cpp: getPublisherIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setpublisheridentifier_1, "PrimaryVolumeDescriptor.cpp: setPublisherIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setpublisheridentifier_2, "PrimaryVolumeDescriptor.cpp: setPublisherIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getdataprepareridentifier, "PrimaryVolumeDescriptor.cpp: getDataPreparerIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setdataprepareridentifier_1, "PrimaryVolumeDescriptor.cpp: setDataPreparerIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setdataprepareridentifier_2, "PrimaryVolumeDescriptor.cpp: setDataPreparerIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getapplicationidentifier, "PrimaryVolumeDescriptor.cpp: getApplicationIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setapplicationidentifier_1, "PrimaryVolumeDescriptor.cpp: setApplicationIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setapplicationidentifier_2, "PrimaryVolumeDescriptor.cpp: setApplicationIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getcopyrightfileidentifier, "PrimaryVolumeDescriptor.cpp: getCopyrightFileIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setcopyrightfileidentifier_1, "PrimaryVolumeDescriptor.cpp: setCopyrightFileIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setcopyrightfileidentifier_2, "PrimaryVolumeDescriptor.cpp: setCopyrightFileIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getabstractfileidentifier, "PrimaryVolumeDescriptor.cpp: getAbstractFileIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setabstractfileidentifier_1, "PrimaryVolumeDescriptor.cpp: setAbstractFileIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setabstractfileidentifier_2, "PrimaryVolumeDescriptor.cpp: setAbstractFileIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getbiblographicfileidentifier, "PrimaryVolumeDescriptor.cpp: getBiblographicFileIdentifier");
    pushTest(&tests, test_primaryvolumedescriptor_setbiblographicfileidentifier_1, "PrimaryVolumeDescriptor.cpp: setBiblographicFileIdentifier (illegal size)");
    pushTest(&tests, test_primaryvolumedescriptor_setbiblographicfileidentifier_2, "PrimaryVolumeDescriptor.cpp: setBiblographicFileIdentifier (legal size)");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumecreationdateandtime, "PrimaryVolumeDescriptor.cpp: getVolumeCreationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumecreationdateandtime, "PrimaryVolumeDescriptor.cpp: setVolumeCreationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumemodificationdateandtime, "PrimaryVolumeDescriptor.cpp: getVolumeModificationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumemodificationdateandtime, "PrimaryVolumeDescriptor.cpp: setVolumeModificationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumeexpirationdateandtime, "PrimaryVolumeDescriptor.cpp: getVolumeExpirationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumeexpirationdateandtime, "PrimaryVolumeDescriptor.cpp: setVolumeExpirationDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_getvolumeeffectivedateandtime, "PrimaryVolumeDescriptor.cpp: getVolumeEffectiveDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_setvolumeeffectivedateandtime, "PrimaryVolumeDescriptor.cpp: setVolumeEffectiveDateAndTime");
    pushTest(&tests, test_primaryvolumedescriptor_getfilestructureversion, "PrimaryVolumeDescriptor.cpp: getFileStructureVersion");
    pushTest(&tests, test_primaryvolumedescriptor_setfilestructureversion, "PrimaryVolumeDescriptor.cpp: setFileStructureVersion");
    pushTest(&tests, test_primaryvolumedescriptor_getapplicationused, "PrimaryVolumeDescriptor.cpp: getApplicationUsed");
    pushTest(&tests, test_primaryvolumedescriptor_setapplicationused, "PrimaryVolumeDescriptor.cpp: setApplicationUsed");
    pushTest(&tests, test_primaryvolumedescriptor_getreserved, "PrimaryVolumeDescriptor.cpp: getReserved");
    pushTest(&tests, test_primaryvolumedescriptor_setreserved, "PrimaryVolumeDescriptor.cpp: setReserved");
    return tests;
}
