/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the Directory class
 */
namespace testDirectory
{
    /**
     * @brief  A test for Directory's setData and getData methods
     * @note   Checks if data can be set and retrived properly
     * @retval true if the test passed
     */
    bool test_directory_setdata_getdata();

    /**
     * @brief  A test for Directory's getDirectoryRecordLength method
     * @note   Checks if the directory record length can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getdirectoryrecordlength();

    /**
     * @brief  A test for Directory's setDirectoryRecordLength method
     * @note   Checks if the directory record length can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setdirectoryrecordlength();

    /**
     * @brief  A test for Directory's getExtendedAttributeRecordLength method
     * @note   Checks if the extended attribute record length can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getextendedattributerecordlength();

    /**
     * @brief  A test for Directory's setExtendedAttributeRecordLength method
     * @note   Checks if the extended attribute record length can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setextendedattributerecordlength();

    /**
     * @brief  A test for Directory's getLocationOfExtent method
     * @note   Checks if the extent location can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getlocationofextent();

    /**
     * @brief  A test for Directory's setLocationOfExtent method
     * @note   Checks if the extent location can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setlocationofextent();

    /**
     * @brief  A test for Directory's getDataLength method
     * @note   Checks if the data length can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getdatalength();

    /**
     * @brief  A test for Directory's setDataLength method
     * @note   Checks if the data length can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setdatalength();

    /**
     * @brief  A test for Directory's getRecordingDateAndTime method
     * @note   Checks if the recording date and time can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getrecordingdateandtime();

    /**
     * @brief  A test for Directory's setRecordingDateAndTime method
     * @note   Checks if the recording date and time can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setrecordingdateandtime();

    /**
     * @brief  A test for Directory's getFileFlags method
     * @note   Checks if the file flags can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getfileflags();

    /**
     * @brief  A test for Directory's setFileFlags method
     * @note   Checks if the file flags can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setfileflags();

    /**
     * @brief  A test for Directory's getFileUnitSize method
     * @note   Checks if the file unit size can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getfileunitsize();

    /**
     * @brief  A test for Directory's setFileUnitSize method
     * @note   Checks if the file unit size can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setfileunitsize();

    /**
     * @brief  A test for Directory's getInterleaveGapSize method
     * @note   Checks if the interleave gap size can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getinterleavegapsize();

    /**
     * @brief  A test for Directory's setInterleaveGapSize method
     * @note   Checks if the interleave gap size can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setinterleavegapsize();

    /**
     * @brief  A test for Directory's getVolumeSequenceNumber method
     * @note   Checks if the volume sequence number can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getvolumesequencenumber();

    /**
     * @brief  A test for Directory's setVolumeSequenceNumber method
     * @note   Checks if the volume sequence number can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setvolumesequencenumber();

    /**
     * @brief  A test for Directory's getFileIdentifierLength method
     * @note   Checks if the file identifier length can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getfileidentifierlength();

    /**
     * @brief  A test for Directory's getFileIdentifierLength method
     * @note   Checks if the file identifier length can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setfileidentifierlength();

    /**
     * @brief  A test for Directory's getFileIdentifier method
     * @note   Checks if the file identifier can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getfileidentifier();

    /**
     * @brief  A test for Directory's setFileIdentifier method
     * @note   Checks if the file identifier can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setfileidentifier();

    /**
     * @brief  A test for Directory's getSystemUse method
     * @note   Checks if the system use can be fetched correctly
     * @retval true if the test passed
     */
    bool test_directory_getsystemuse();

    /**
     * @brief  A test for Directory's setSystemUse method
     * @note   Checks if the system use can be set correctly
     * @retval true if the test passed
     */
    bool test_directory_setsystemuse();

    /**
     * @brief  A test for Directory's isValid method
     * @note   Checks if empty data is valid
     * @retval true if the test passed
     */
    bool test_directory_isvalid_1();

    /**
     * @brief  A test for Directory's isValid method
     * @note   Checks if a correct directory record is valid
     * @retval true if the test passed
     */
    bool test_directory_isvalid_2();

    /**
     * @brief  A test for Directory's isValid method
     * @note   Checks if wrong directory record length is valid
     * @retval true if the test passed
     */
    bool test_directory_isvalid_3();

    /**
     * @brief  A test for Directory's no-args constructor
     * @note   Checks if a directory object was initialized
     * @retval true if the test passed
     */
    bool test_directory_contructor_1();

    /**
     * @brief  A test for Directory's normal constructor
     * @note   Checks if a directory object was initialized
     * @retval true if the test passed
     */
    bool test_directory_constructor_2();

    /**
     * @brief  A test for Directory's clone method
     * @note   Checks if a directory object was cloned correctly
     * @retval true if the test passed
     */
    bool test_directory_clone();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}