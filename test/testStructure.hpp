/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all functions inside the testStructure.cpp file
 */
namespace testStructure
{
    /**
     * @brief  Checks that an ISO can be exported correctly
     * @retval true if the test passed
     */
    bool test_structure_export();

    /**
     * @brief  Checks that an ISO can be imported correctly
     * @retval true if the test passed
     */
    bool test_structure_import();

    /**
     * @brief  Checks that exported hashes are correct
     * @retval true if the test passed
     */
    bool test_structure_hashes();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}