/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// includes
#include <string>
#include <vector>

// define the test pointer type
#ifndef TEST_PTR_T
#define TEST_PTR_T

/**
 * @brief  This creates a type for every test function
 * @retval true if the test passed
 */
typedef bool test_ptr();

// end of test pointer type
#endif

// define the test data type
#ifndef TEST_DATA_T
#define TEST_DATA_T

/**
 * @brief  This creates a type for test data
 */
typedef struct test_data
{
    test_ptr *function;
    std::string description;
} test_data;

// end of test data type
#endif

// Creates a type for the test vector
#define TEST_VECTOR_T std::vector<test_data>

/**
 * @brief  This adds a test to a test vector
 * @note   throws illegal_argument if vec is null
 * @param  vec: vector
 * @param  test: function
 * @param  str: description
 * @retval None
 */
void pushTest(TEST_VECTOR_T *vec, test_ptr test, std::string str);