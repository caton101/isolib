/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the VolumeDescriptor class
 */
namespace testVolumeDescriptor
{
    /**
     * @brief  A test for VolumeDescriptor's setAddress and getAddress methods
     * @note   this checks if the address returned is the same as it was set to
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setaddress_getaddress();

    /**
     * @brief  A test for VolumeDescriptor's setData and getData methods
     * @note   this checks if the data array is altered upon setting or returning
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setdata_getdata();

    /**
     * @brief  A test for VolumeDescriptor's getType method
     * @note   Checks if a type is fetched correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_gettype();

    /**
     * @brief  A test for VolumeDescriptor's setType method
     * @note   Checks if a type is set correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_settype();

    /**
     * @brief  A test for VolumeDescriptor's getIdentifier method
     * @note   Checks if an identifier is fetched correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setIdentifier method
     * @note   Checks if an identifier is set correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getversion method
     * @note   Checks if a version is fetched correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getversion();

    /**
     * @brief  A test for VolumeDescriptor's setVersion method
     * @note   Checks if a version is set correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setversion();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a blank image is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_1();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a sole type is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_2();
    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a sole identifier is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_3();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a sole version is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_4();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a correct header is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_5();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a wrong identifier is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_6();

    /**
     * @brief  A test for VolumeDescriptor's isValid method
     * @note   Checks if a wrong version is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_isvalid_7();

    /**
     * @brief  A test for VolumeDescriptor's clone method
     * @note   Checks if the raw array in both is the same
     * @retval true if the test passed
     */
    bool test_volumedescriptor_clone();

    /**
     * @brief  A test for VolumeDescriptor's no-args constructor
     * @note   Checks if the no-args constructor makes a legal volume descriptor
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_1();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if the address is set correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_2();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a blank image is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_3();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a sole type is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_4();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a sole identifier is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_5();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a sole version is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_6();
    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a correct header is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_7();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a wrong identifier is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_8();

    /**
     * @brief  A test for VolumeDescriptor's constructor
     * @note   Checks if a wrong version is valid
     * @retval true if the test passed
     */
    bool test_volumedescriptor_constructor_9();

    /**
     * @brief  A test for VolumeDescriptor's markAddresses method
     * @note   Checks for an exception when bits is null
     * @retval true if the test passed
     */
    bool test_volumedescriptor_markaddresses_1();

    /**
     * @brief  A test for VolumeDescriptor's markAddresses method
     * @note   Checks if a VolumeDescriptor object marks its addresses correctly
     * @retval true if the test passed
     */
    bool test_volumedescriptor_markaddresses_2();

    /**
     * @brief  A test for VolumeDescriptor's getSystemIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getsystemidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setSystemIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setsystemidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumeidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumeidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeSpaceSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumespacesize();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeSpaceSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumespacesize();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeSetSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumesetsize();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeSetSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumesetsize();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeSequenceNumber method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumesequencenumber();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeSequenceNumber method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumesequencenumber();

    /**
     * @brief  A test for VolumeDescriptor's getLogicalBlockSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getlogicalblocksize();

    /**
     * @brief  A test for VolumeDescriptor's setLogicalBlockSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setlogicalblocksize();

    /**
     * @brief  A test for VolumeDescriptor's getPathTableSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getpathtablesize();

    /**
     * @brief  A test for VolumeDescriptor's setPathTableSize method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setpathtablesize();

    /**
     * @brief  A test for VolumeDescriptor's getLocationOfTypeLPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getlocationoftypelpathtable();

    /**
     * @brief  A test for VolumeDescriptor's setLocationOfTypeLPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setlocationoftypelpathtable();

    /**
     * @brief  A test for VolumeDescriptor's getLocationOfOptionalTypeLPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getlocationofoptionaltypelpathtable();

    /**
     * @brief  A test for VolumeDescriptor's setLocationOfOptionalTypeLPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setlocationofoptionaltypelpathtable();

    /**
     * @brief  A test for VolumeDescriptor's getLocationOfTypeMPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getlocationoftypempathtable();

    /**
     * @brief  A test for VolumeDescriptor's setLocationOfTypeMPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setlocationoftypempathtable();

    /**
     * @brief  A test for VolumeDescriptor's getLocationOfOptionalTypeMPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getlocationofoptionaltypempathtable();

    /**
     * @brief  A test for VolumeDescriptor's setLocationOfOptionalTypeMPathTable method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setlocationofoptionaltypempathtable();

    /**
     * @brief  A test for VolumeDescriptor's getRootDirectoryEntry method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getrootdirectoryentry();

    /**
     * @brief  A test for VolumeDescriptor's setRootDirectoryEntry method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setrootdirectoryentry();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeSetIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumesetidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeSetIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumesetidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getPublisherIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getpublisheridentifier();

    /**
     * @brief  A test for VolumeDescriptor's setPublisherIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setpublisheridentifier();

    /**
     * @brief  A test for VolumeDescriptor's getDataPreparerIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getdataprepareridentifier();

    /**
     * @brief  A test for VolumeDescriptor's setDataPreparerIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setdataprepareridentifier();

    /**
     * @brief  A test for VolumeDescriptor's getApplicationIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getapplicationidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setApplicationIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setapplicationidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getCopyrightFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getcopyrightfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setCopyrightFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setcopyrightfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getAbstractFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getabstractfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setAbstractFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setabstractfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getBiblographicFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getbiblographicfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's setBiblographicFileIdentifier method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setbiblographicfileidentifier();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeCreationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumecreationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeCreationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumecreationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeModificationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumemodificationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeModificationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumemodificationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeExpirationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumeexpirationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeExpirationDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumeexpirationdateandtime();

    /**
     * @brief  A test for VolumeDescriptor's getVolumeEffectiveDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getvolumeeffectivedateandtime();

    /**
     * @brief  A test for VolumeDescriptor's setVolumeEffectiveDateAndTime method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setvolumeeffectivedateandtime();

    /**
     * @brief  A test for VolumeDescriptor's getFileStructureVersion method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getfilestructureversion();

    /**
     * @brief  A test for VolumeDescriptor's setFileStructureVersion method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setfilestructureversion();

    /**
     * @brief  A test for VolumeDescriptor's getApplicationUsed method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getapplicationused();

    /**
     * @brief  A test for VolumeDescriptor's setApplicationUsed method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setapplicationused();

    /**
     * @brief  A test for VolumeDescriptor's getReserved method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_getreserved();

    /**
     * @brief  A test for VolumeDescriptor's setReserved method
     * @note   Checks if an exception is thrown
     * @retval true if the test passed
     */
    bool test_volumedescriptor_setreserved();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}
