/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testExtentData.hpp"
#include "../include/ExtentData.hpp"

bool testExtentData::test_extentdata_setdata_getdata()
{
    std::vector<char> ans;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::ExtentData ext;
    ext.setData(ans);
    std::vector<char> out = ext.getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testExtentData::test_extentdata_constructor_1()
{
    isochop::ExtentData *ext = new isochop::ExtentData();
    return ext != nullptr;
}

bool testExtentData::test_extentdata_constructor_2()
{
    std::vector<char> ans;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::ExtentData *ext = new isochop::ExtentData(ans);
    std::vector<char> out = ext->getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testExtentData::test_extentdata_clone()
{
    std::vector<char> ans;
    for (int i = 0; i < 100; i++)
    {
        ans.push_back(i % 256);
    }
    isochop::ExtentData *ext = new isochop::ExtentData(ans);
    isochop::ExtentData dup = ext->clone();
    std::vector<char> out = dup.getData();
    if (ans.size() != out.size())
    {
        return false;
    }
    for (size_t i = 0; i < out.size(); i++)
    {
        if (out.at(i) != ans.at(i))
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testExtentData::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_extentdata_setdata_getdata, "ExtentData.cpp: getData and setData (symmetry)");
    pushTest(&tests, test_extentdata_constructor_1, "ExtentData.cpp: Extent (no-args)");
    pushTest(&tests, test_extentdata_constructor_2, "ExtentData.cpp: Extent (args)");
    pushTest(&tests, test_extentdata_clone, "ExtentData.cpp: clone");
    return tests;
}