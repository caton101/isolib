/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testDirDatetime.hpp"
#include "../include/DirDatetime.hpp"
#include "../include/definitions.hpp"

bool testDirDatetime::test_dirdatetime_setyear_getyear()
{
    uint8_t year = 70;
    isochop::DirDatetime dd;
    dd.setYear(year);
    return dd.getYear() == year;
}

bool testDirDatetime::test_dirdatetime_setmonth_getmonth()
{
    uint8_t month = 12;
    isochop::DirDatetime dd;
    dd.setMonth(month);
    return dd.getMonth() == month;
}

bool testDirDatetime::test_dirdatetime_setday_getday()
{
    uint8_t day = 12;
    isochop::DirDatetime dd;
    dd.setDay(day);
    return dd.getDay() == day;
}

bool testDirDatetime::test_dirdatetime_sethour_gethour()
{
    uint8_t hour = 12;
    isochop::DirDatetime dd;
    dd.setHour(hour);
    return dd.getHour() == hour;
}

bool testDirDatetime::test_dirdatetime_setminute_getminute()
{
    uint8_t minute = 12;
    isochop::DirDatetime dd;
    dd.setMinute(minute);
    return dd.getMinute() == minute;
}

bool testDirDatetime::test_dirdatetime_setsecond_getsecond()
{
    uint8_t second = 12;
    isochop::DirDatetime dd;
    dd.setSecond(second);
    return dd.getSecond() == second;
}

bool testDirDatetime::test_dirdatetime_settimezone_gettimezone()
{
    int8_t timezone = 4;
    isochop::DirDatetime dd;
    dd.setTimezone(timezone);
    return dd.getTimezone() == timezone;
}

bool testDirDatetime::test_dirdatetime_setdata_getdata()
{
    std::vector<char> ans;
    ans.reserve(ISOCHOP_DIR_DATETIME_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_DIR_DATETIME_SIZE_TOTAL; i++)
    {
        ans.push_back('0' + (i % 10));
    }
    isochop::DirDatetime dd;
    dd.setData(ans);
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DIR_DATETIME_SIZE_TOTAL; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDirDatetime::test_dirdatetime_isvalid_1()
{
    isochop::DirDatetime dd;
    return dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_2()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_YEAR) = 70;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_3()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_MONTH) = 1;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_4()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_DAY) = 1;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_5()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_HOUR) = 1;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_6()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_MINUTE) = 1;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_7()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_SECOND) = 1;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_8()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_TIMEZONE) = 12;
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_9()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        1,  // day
        1,  // hour
        1,  // minute
        1,  // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_10()
{
    std::vector<char> data = {
        70, // year
        99, // month
        1,  // day
        1,  // hour
        1,  // minute
        1,  // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_11()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        99, // day
        1,  // hour
        1,  // minute
        1,  // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_12()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        1,  // day
        99, // hour
        1,  // minute
        1,  // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_13()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        1,  // day
        1,  // hour
        99, // minute
        1,  // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_14()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        1,  // day
        1,  // hour
        1,  // minute
        99, // second
        4   // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_isvalid_15()
{
    std::vector<char> data = {
        70, // year
        1,  // month
        1,  // day
        1,  // hour
        1,  // minute
        1,  // second
        99  // timezone
    };
    isochop::DirDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_1()
{
    isochop::DirDatetime *dd = new isochop::DirDatetime();
    return dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_2()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_YEAR) = 70;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_3()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_MONTH) = 1;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_4()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_DAY) = 1;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_5()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_HOUR) = 1;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_6()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_MINUTE) = 1;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_7()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_SECOND) = 1;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_8()
{
    std::vector<char> data(ISOCHOP_DIR_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DIR_DATETIME_OFFSET_TIMEZONE) = 12;
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_9()
{
    std::vector<char> data = {
        70, // year
        12, // month
        12, // day
        12, // hour
        12, // minute
        12, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_10()
{
    std::vector<char> data = {
        70, // year
        99, // month
        12, // day
        12, // hour
        12, // minute
        12, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_11()
{
    std::vector<char> data = {
        70, // year
        12, // month
        99, // day
        12, // hour
        12, // minute
        12, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_12()
{
    std::vector<char> data = {
        70, // year
        12, // month
        12, // day
        99, // hour
        12, // minute
        12, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_13()
{
    std::vector<char> data = {
        70, // year
        12, // month
        12, // day
        12, // hour
        99, // minute
        12, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_14()
{
    std::vector<char> data = {
        70, // year
        12, // month
        12, // day
        12, // hour
        12, // minute
        99, // second
        4   // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

bool testDirDatetime::test_dirdatetime_constructor_15()
{
    std::vector<char> data = {
        70, // year
        12, // month
        12, // day
        12, // hour
        12, // minute
        12, // second
        99  // timezone
    };
    isochop::DirDatetime *dd = new isochop::DirDatetime(data);
    return !dd->isValid();
}

TEST_VECTOR_T testDirDatetime::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_dirdatetime_setyear_getyear, "DirDatetime.cpp: setYear/getYear");
    pushTest(&tests, test_dirdatetime_setmonth_getmonth, "DirDatetime.cpp: setMonth/getMonth");
    pushTest(&tests, test_dirdatetime_setday_getday, "DirDatetime.cpp: setDay/getDay");
    pushTest(&tests, test_dirdatetime_sethour_gethour, "DirDatetime.cpp: setHour/getHour");
    pushTest(&tests, test_dirdatetime_setminute_getminute, "DirDatetime.cpp: setMinute/getMinute");
    pushTest(&tests, test_dirdatetime_setsecond_getsecond, "DirDatetime.cpp: setSecond/getSecond");
    pushTest(&tests, test_dirdatetime_settimezone_gettimezone, "DirDatetime.cpp: setTimezone/getTimezone");
    pushTest(&tests, test_dirdatetime_setdata_getdata, "DirDatetime.cpp: setData/getData");
    pushTest(&tests, test_dirdatetime_isvalid_1, "DirDatetime.cpp: isValid (empty)");
    pushTest(&tests, test_dirdatetime_isvalid_2, "DirDatetime.cpp: isValid (only year)");
    pushTest(&tests, test_dirdatetime_isvalid_3, "DirDatetime.cpp: isValid (only month)");
    pushTest(&tests, test_dirdatetime_isvalid_4, "DirDatetime.cpp: isValid (only day)");
    pushTest(&tests, test_dirdatetime_isvalid_5, "DirDatetime.cpp: isValid (only hour)");
    pushTest(&tests, test_dirdatetime_isvalid_6, "DirDatetime.cpp: isValid (only minute)");
    pushTest(&tests, test_dirdatetime_isvalid_7, "DirDatetime.cpp: isValid (only second)");
    pushTest(&tests, test_dirdatetime_isvalid_8, "DirDatetime.cpp: isValid (only timezone)");
    pushTest(&tests, test_dirdatetime_isvalid_9, "DirDatetime.cpp: isValid (legal)");
    pushTest(&tests, test_dirdatetime_isvalid_10, "DirDatetime.cpp: isValid (illegal month)");
    pushTest(&tests, test_dirdatetime_isvalid_11, "DirDatetime.cpp: isValid (illegal day)");
    pushTest(&tests, test_dirdatetime_isvalid_12, "DirDatetime.cpp: isValid (illegal hour)");
    pushTest(&tests, test_dirdatetime_isvalid_13, "DirDatetime.cpp: isValid (illegal minute)");
    pushTest(&tests, test_dirdatetime_isvalid_14, "DirDatetime.cpp: isValid (illegal second)");
    pushTest(&tests, test_dirdatetime_isvalid_15, "DirDatetime.cpp: isValid (illegal timezone)");
    pushTest(&tests, test_dirdatetime_constructor_1, "DirDatetime.cpp: DirDatetime (empty)");
    pushTest(&tests, test_dirdatetime_constructor_2, "DirDatetime.cpp: DirDatetime (only year)");
    pushTest(&tests, test_dirdatetime_constructor_3, "DirDatetime.cpp: DirDatetime (only month)");
    pushTest(&tests, test_dirdatetime_constructor_4, "DirDatetime.cpp: DirDatetime (only day)");
    pushTest(&tests, test_dirdatetime_constructor_5, "DirDatetime.cpp: DirDatetime (only hour)");
    pushTest(&tests, test_dirdatetime_constructor_6, "DirDatetime.cpp: DirDatetime (only minute)");
    pushTest(&tests, test_dirdatetime_constructor_7, "DirDatetime.cpp: DirDatetime (only second)");
    pushTest(&tests, test_dirdatetime_constructor_8, "DirDatetime.cpp: DirDatetime (only timezone)");
    pushTest(&tests, test_dirdatetime_constructor_9, "DirDatetime.cpp: DirDatetime (legal)");
    pushTest(&tests, test_dirdatetime_constructor_10, "DirDatetime.cpp: DirDatetime (illegal month)");
    pushTest(&tests, test_dirdatetime_constructor_11, "DirDatetime.cpp: DirDatetime (illegal day)");
    pushTest(&tests, test_dirdatetime_constructor_12, "DirDatetime.cpp: DirDatetime (illegal hour)");
    pushTest(&tests, test_dirdatetime_constructor_13, "DirDatetime.cpp: DirDatetime (illegal minute)");
    pushTest(&tests, test_dirdatetime_constructor_14, "DirDatetime.cpp: DirDatetime (illegal second)");
    pushTest(&tests, test_dirdatetime_constructor_15, "DirDatetime.cpp: DirDatetime (illegal timezone)");
    return tests;
}