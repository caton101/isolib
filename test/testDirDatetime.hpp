/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the DirDatetime class
 */
namespace testDirDatetime
{
    /**
     * @brief  A test for DirDatetime's setYear and getYear methods
     * @note   Checks if the year can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setyear_getyear();

    /**
     * @brief  A test for DirDatetime's setMonth and getMonth methods
     * @note   Checks if the month can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setmonth_getmonth();

    /**
     * @brief  A test for DirDatetime's setDay and getDay methods
     * @note   Checks if the day can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setday_getday();

    /**
     * @brief  A test for DirDatetime's setHour and getHour methods
     * @note   Checks if the hour can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_sethour_gethour();

    /**
     * @brief  A test for DirDatetime's setMinute and getMinute methods
     * @note   Checks if the minute can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setminute_getminute();

    /**
     * @brief  A test for DirDatetime's setSecond and getSecond methods
     * @note   Checks if the second can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setsecond_getsecond();

    /**
     * @brief  A test for DirDatetime's setTimezone and getTimezone methods
     * @note   Checks if the timezone can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_settimezone_gettimezone();

    /**
     * @brief  A test for DirDatetime's setData and getData methods
     * @note   Checks if the data vector can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_dirdatetime_setdata_getdata();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if an empty DirDatetime is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_1();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole year is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_2();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole month is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_3();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole day is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_4();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole hour is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_5();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole minute is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_6();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole second is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_7();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if a sole timezone is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_8();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if correct data is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_9();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad month is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_10();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad day is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_11();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad hour is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_12();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad minute is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_13();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad second is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_14();

    /**
     * @brief  A test for DirDatetime's isValid method
     * @note   Checks if bad timezone is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_isvalid_15();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if an empty DirDatetime is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_1();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole year is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_2();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole month is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_3();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole day is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_4();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole hour is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_5();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole minute is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_6();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole second is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_7();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if a sole timezone is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_8();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if correct data is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_9();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad month is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_10();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad day is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_11();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad hour is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_12();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad minute is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_13();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad second is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_14();

    /**
     * @brief  A test for DirDatetime's constructor
     * @note   Checks if bad timezone is valid
     * @retval true if the test passed
     */
    bool test_dirdatetime_constructor_15();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}