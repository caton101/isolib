/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the ExtentData class
 */
namespace testExtentData
{
    /**
     * @brief  A test for ExtentData's setData and getData methods
     * @note   Checks if the data vector can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_extentdata_setdata_getdata();

    /**
     * @brief  A test for ExtentData's no-args constructor
     * @note   Checks if a Extent object can be created correctly
     * @retval true if the test passed
     */
    bool test_extentdata_constructor_1();

    /**
     * @brief  A test for ExtentData's normal constructor
     * @note   Checks if a Extent object is initialized correctly
     * @retval true if the test passed
     */
    bool test_extentdata_constructor_2();

    /**
     * @brief  A test for ExtentData's clone method
     * @note   Checks if a Extent object is cloned correctly
     * @retval true if the test passed
     */
    bool test_extentdata_clone();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}