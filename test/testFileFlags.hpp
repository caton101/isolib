/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the FileFlags class
 */
namespace testFileFlags
{
    /**
     * @brief  A test for FileFlags' setData and getData methods
     * @note   Checks if the flags byte can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_fileflags_setdata_getdata();

    /**
     * @brief  A test for FileFlags' no-args constructor
     * @note   Checks if a FileFlags object can be created correctly
     * @retval true if the test passed
     */
    bool test_fileflags_constructor_1();

    /**
     * @brief  A test for FileFlags' normal constructor
     * @note   Checks if a FileFlags object is initialized correctly
     * @retval true if the test passed
     */
    bool test_fileflags_constructor_2();

    /**
     * @brief  A test for FileFlags' getHidden method
     * @note   Checks if the hidden flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_gethidden();

    /**
     * @brief  A test for FileFlags' setHidden method
     * @note   Checks if the hidden flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_sethidden();

    /**
     * @brief  A test for FileFlags' getIsDirectory method
     * @note   Checks if the directory flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getisdirectory();

    /**
     * @brief  A test for FileFlags' setIsDirectory method
     * @note   Checks if the directory flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setisdirectory();

    /**
     * @brief  A test for FileFlags' getIsAssociatedFile method
     * @note   Checks if the associated flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getisassociatedfile();

    /**
     * @brief  A test for FileFlags' setIsAssociatedFile method
     * @note   Checks if the associated flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setisassociatedfile();

    /**
     * @brief  A test for FileFlags' getHasFormatInformation method
     * @note   Checks if the format flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_gethasformatinformation();

    /**
     * @brief  A test for FileFlags' setHasFormatInformation method
     * @note   Checks if the format flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_sethasformatinformation();

    /**
     * @brief  A test for FileFlags' getArePermissionsInExtendedAttributeRecord method
     * @note   Checks if the permissions flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getarepermissionsinextendedattributerecord();

    /**
     * @brief  A test for FileFlags' setArePermissionsInExtendedAttributeRecord method
     * @note   Checks if the permissions flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setarepermissionsinextendedattributerecord();

    /**
     * @brief  A test for FileFlags' getReservedBit5 method
     * @note   Checks if the reserved5 flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getreservedbit5();

    /**
     * @brief  A test for FileFlags' setReservedBit5 method
     * @note   Checks if the reserved5 flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setreservedbit5();

    /**
     * @brief  A test for FileFlags' getReservedBit6 method
     * @note   Checks if the reserved6 flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getreservedbit6();

    /**
     * @brief  A test for FileFlags' setReservedBit6 method
     * @note   Checks if the reserved6 flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setreservedbit6();

    /**
     * @brief  A test for FileFlags' getNotFinalRecord method
     * @note   Checks if the final flag is fetched correctly
     * @retval true of the test passed
     */
    bool test_fileflags_getnotfinalrecord();

    /**
     * @brief  A test for FileFlags' setNotFinalRecord method
     * @note   Checks if the final flag is set correctly
     * @retval true of the test passed
     */
    bool test_fileflags_setnotfinalrecord();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}