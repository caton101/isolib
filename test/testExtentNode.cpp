/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testExtentNode.hpp"
#include "../include/ExtentNode.hpp"
#include "../include/Directory.hpp"
#include "../include/ExtentData.hpp"
#include "../include/definitions.hpp"

bool testExtentNode::test_extentnode_getaddress_setaddress()
{
    unsigned long addr = 0x8765432112345678;
    isochop::ExtentNode fn;
    fn.setAddress(addr);
    return fn.getAddress() == addr;
}

bool testExtentNode::test_extentnode_getextent_setextent()
{
    std::vector<char> data;
    for (int i = 0; i < 100; i++)
    {
        data.push_back(i % 10);
    }
    isochop::ExtentData ans = isochop::ExtentData(data);
    isochop::ExtentNode fn;
    fn.setExtentData(ans);
    std::vector<char> out = fn.getExtentData().getData();
    if (out.size() != data.size())
    {
        return false;
    }
    for (size_t i = 0; i < data.size(); i++)
    {
        if (out.at(i) != data.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testExtentNode::test_extentnode_getdirectoryentries_setdirectoryentries()
{
    std::vector<char> data;
    for (int i = 0; i < 255; i++)
    {
        data.push_back(i);
    }
    data.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory dir = isochop::Directory(data);
    isochop::ExtentNode fn;
    fn.setDirectoryEntry(dir);
    isochop::Directory out = fn.getDirectoryEntry();
    std::vector<char> a = dir.getData();
    std::vector<char> b = out.getData();
    if (a.size() != b.size())
    {
        return false;
    }
    for (size_t iByte = 0; iByte < b.size(); iByte++)
    {
        if (a.at(iByte) != b.at(iByte))
        {
            return false;
        }
    }
    return true;
}

bool testExtentNode::test_extentnode_getchildren_setchildren()
{
    isochop::ExtentNode fnA = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 42, std::vector<isochop::ExtentNode>(0));
    isochop::ExtentNode fnB = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 72, std::vector<isochop::ExtentNode>(0));
    std::vector<isochop::ExtentNode> ans = {fnA, fnB};
    isochop::ExtentNode fn;
    fn.setChildren(ans);
    std::vector<isochop::ExtentNode> out = fn.getChildren();
    if (out.size() != ans.size())
    {
        return false;
    }
    for (size_t i = 0; i < ans.size(); i++)
    {
        if (out.at(i).getAddress() != ans.at(i).getAddress())
        {
            return false;
        }
    }
    return true;
}

bool testExtentNode::test_extentnode_constructor_1()
{
    isochop::ExtentNode *fn = new isochop::ExtentNode();
    return fn != nullptr;
}

bool testExtentNode::test_extentnode_constructor_2()
{
    // make the directory entry
    std::vector<char> directoryData;
    for (int i = 0; i < 255; i++)
    {
        directoryData.push_back(i);
    }
    directoryData.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory directory = isochop::Directory(directoryData);
    // make the extent
    std::vector<char> extentData;
    for (int i = 0; i < 100; i++)
    {
        extentData.push_back(i % 10);
    }
    isochop::ExtentData extent = isochop::ExtentData(extentData);
    // make the address
    unsigned long address = 0x8765432112345678;
    // make the children
    isochop::ExtentNode childA = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 42, std::vector<isochop::ExtentNode>(0));
    isochop::ExtentNode childB = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 72, std::vector<isochop::ExtentNode>(0));
    std::vector<isochop::ExtentNode> children = {childA, childB};
    // make object
    isochop::ExtentNode *fn = new isochop::ExtentNode(directory, extent, address, children);
    if (fn == nullptr)
    {
        return false;
    }
    // check directory
    std::vector<char> a = fn->getDirectoryEntry().getData();
    std::vector<char> b = directory.getData();
    if (a.size() != b.size())
    {
        return false;
    }
    for (size_t iByte = 0; iByte < b.size(); iByte++)
    {
        if (a.at(iByte) != b.at(iByte))
        {
            return false;
        }
    }
    // check extent
    if (fn->getExtentData().getData().size() != extent.getData().size())
    {
        return false;
    }
    for (size_t i = 0; i < fn->getExtentData().getData().size(); i++)
    {
        if (extent.getData().at(i) != fn->getExtentData().getData().at(i))
        {
            return false;
        }
    }
    // check address
    if (fn->getAddress() != address)
    {
        return false;
    }
    // check children
    if (fn->getChildren().size() != children.size())
    {
        return false;
    }
    for (size_t i = 0; i < children.size(); i++)
    {
        if (fn->getChildren().at(i).getAddress() != children.at(i).getAddress())
        {
            return false;
        }
    }
    return true;
}

bool testExtentNode::test_extentnode_clone()
{
    // make the directory entry
    std::vector<char> directoryData;
    for (int i = 0; i < 255; i++)
    {
        directoryData.push_back(i);
    }
    directoryData.at(ISOCHOP_DIRECTORY_OFFSET_DIRECTORY_RECORD_LENGTH) = (unsigned char)255;
    isochop::Directory directory = isochop::Directory(directoryData);
    // make the extent
    std::vector<char> extentData;
    for (int i = 0; i < 100; i++)
    {
        extentData.push_back(i % 10);
    }
    isochop::ExtentData extent = isochop::ExtentData(extentData);
    // make the address
    unsigned long address = 0x8765432112345678;
    // make the children
    isochop::ExtentNode childA = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 42, std::vector<isochop::ExtentNode>(0));
    isochop::ExtentNode childB = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(), 72, std::vector<isochop::ExtentNode>(0));
    std::vector<isochop::ExtentNode> children = {childA, childB};
    // make object
    isochop::ExtentNode *original = new isochop::ExtentNode(directory, extent, address, children);
    if (original == nullptr)
    {
        return false;
    }
    isochop::ExtentNode clone = original->clone();
    // check directory
    std::vector<char> a = clone.getDirectoryEntry().getData();
    std::vector<char> b = directory.getData();
    if (a.size() != b.size())
    {
        return false;
    }
    for (size_t iByte = 0; iByte < b.size(); iByte++)
    {
        if (a.at(iByte) != b.at(iByte))
        {
            return false;
        }
    }
    // check extent
    if (clone.getExtentData().getData().size() != extent.getData().size())
    {
        return false;
    }
    for (size_t i = 0; i < clone.getExtentData().getData().size(); i++)
    {
        if (extent.getData().at(i) != clone.getExtentData().getData().at(i))
        {
            return false;
        }
    }
    // check address
    if (clone.getAddress() != address)
    {
        return false;
    }
    // check children
    if (clone.getChildren().size() != children.size())
    {
        return false;
    }
    for (size_t i = 0; i < children.size(); i++)
    {
        if (clone.getChildren().at(i).getAddress() != children.at(i).getAddress())
        {
            return false;
        }
    }
    return true;
}

bool testExtentNode::test_extentnode_markaddresses_1()
{
    isochop::ExtentNode en = isochop::ExtentNode(isochop::Directory(), 20);
    try
    {
        en.markAddresses(nullptr);
    }
    catch (const std::invalid_argument &ia)
    {
        return true;
    }
    return false;
}

bool testExtentNode::test_extentnode_markaddresses_2()
{
    std::vector<char> data = {1, 2, 3, 4, 5};
    std::vector<bool> bits(20, 0);
    isochop::ExtentNode en = isochop::ExtentNode(isochop::Directory(), isochop::ExtentData(data), 5, std::vector<isochop::ExtentNode>());
    en.markAddresses(&bits);
    for (int i = 0; i < 5; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    for (int i = 5; i < 10; i++)
    {
        if (!bits.at(i))
        {
            return false;
        }
    }
    for (int i = 10; i < 20; i++)
    {
        if (bits.at(i))
        {
            return false;
        }
    }
    return true;
}

TEST_VECTOR_T testExtentNode::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_extentnode_getaddress_setaddress, "ExtentNode.cpp: getAddress/setAddress (symmetry)");
    pushTest(&tests, test_extentnode_getextent_setextent, "ExtentNode.cpp: getExtent/setExtent (symmetry)");
    pushTest(&tests, test_extentnode_getdirectoryentries_setdirectoryentries, "ExtentNode.cpp: getDirectoryEntry/setDirectoryEntry (symmetry)");
    pushTest(&tests, test_extentnode_getchildren_setchildren, "ExtentNode.cpp: getChildren/setChildren (symmetry)");
    pushTest(&tests, test_extentnode_constructor_1, "ExtentNode.cpp: ExtentNode (no-args)");
    pushTest(&tests, test_extentnode_constructor_2, "ExtentNode.cpp: ExtentNode (args)");
    pushTest(&tests, test_extentnode_clone, "ExtentNode.cpp: Clone");
    pushTest(&tests, test_extentnode_markaddresses_1, "ExtentNode.cpp: markAddresses (nullptr)");
    pushTest(&tests, test_extentnode_markaddresses_2, "ExtentNode.cpp: markAddresses (data)");
    return tests;
}