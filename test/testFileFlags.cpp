/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testFileFlags.hpp"
#include "../include/FileFlags.hpp"
#include "../include/definitions.hpp"

bool testFileFlags::test_fileflags_setdata_getdata()
{
    uint8_t ans = 0b10101010;
    isochop::FileFlags ff;
    ff.setData(ans);
    return ff.getData() == ans;
}

bool testFileFlags::test_fileflags_constructor_1()
{
    isochop::FileFlags *ff = new isochop::FileFlags();
    return ff != nullptr;
}

bool testFileFlags::test_fileflags_constructor_2()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b10101010);
    return ff != nullptr && ff->getData() == 0b10101010;
}

bool testFileFlags::test_fileflags_gethidden()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000001);
    return ff->getHidden() == 1;
}

bool testFileFlags::test_fileflags_sethidden()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setHidden(true);
    return ff->getData() == 0b00000001;
}

bool testFileFlags::test_fileflags_getisdirectory()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000010);
    return ff->getIsDirectory() == 1;
}

bool testFileFlags::test_fileflags_setisdirectory()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setIsDirectory(true);
    return ff->getData() == 0b00000010;
}

bool testFileFlags::test_fileflags_getisassociatedfile()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000100);
    return ff->getIsAssociatedFile() == 1;
}

bool testFileFlags::test_fileflags_setisassociatedfile()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setIsAssociatedFile(true);
    return ff->getData() == 0b00000100;
}

bool testFileFlags::test_fileflags_gethasformatinformation()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00001000);
    return ff->getHasFormatInformation() == 1;
}

bool testFileFlags::test_fileflags_sethasformatinformation()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setArePermissionsInExtendedAttributeRecord(true);
    return ff->getData() == 0b00001000;
}

bool testFileFlags::test_fileflags_getarepermissionsinextendedattributerecord()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00010000);
    return ff->getArePermissionsInExtendedAttributeRecord() == 1;
}

bool testFileFlags::test_fileflags_setarepermissionsinextendedattributerecord()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setArePermissionsInExtendedAttributeRecord(true);
    return ff->getData() == 0b00010000;
}

bool testFileFlags::test_fileflags_getreservedbit5()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00100000);
    return ff->getReservedBit5() == 1;
}

bool testFileFlags::test_fileflags_setreservedbit5()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setReservedBit5(true);
    return ff->getData() == 0b00100000;
}

bool testFileFlags::test_fileflags_getreservedbit6()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b01000000);
    return ff->getReservedBit6() == 1;
}

bool testFileFlags::test_fileflags_setreservedbit6()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setReservedBit6(true);
    return ff->getData() == 0b01000000;
}

bool testFileFlags::test_fileflags_getnotfinalrecord()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b10000000);
    return ff->getNotFinalRecord() == 1;
}

bool testFileFlags::test_fileflags_setnotfinalrecord()
{
    isochop::FileFlags *ff = new isochop::FileFlags(0b00000000);
    ff->setNotFinalRecord(true);
    return ff->getData() == 0b10000000;
}

TEST_VECTOR_T testFileFlags::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_fileflags_setdata_getdata, "FileFlags.cpp: getData and setData (symmetry)");
    pushTest(&tests, test_fileflags_constructor_1, "FileFlags.cpp: FileFlags (no-args)");
    pushTest(&tests, test_fileflags_constructor_2, "FileFlags.cpp: FileFlags (args)");
    pushTest(&tests, test_fileflags_gethidden, "FileFlags.cpp: getHidden");
    pushTest(&tests, test_fileflags_sethidden, "FileFlags.cpp: setHidden");
    pushTest(&tests, test_fileflags_getisdirectory, "FileFlags.cpp: getIsDirectory");
    pushTest(&tests, test_fileflags_setisdirectory, "FileFlags.cpp: setIsDirectory");
    pushTest(&tests, test_fileflags_getisassociatedfile, "FileFlags.cpp: getIsAssociatedFile");
    pushTest(&tests, test_fileflags_setisassociatedfile, "FileFlags.cpp: setIsAssociatedFile");
    pushTest(&tests, test_fileflags_gethasformatinformation, "FileFlags.cpp: getHasFormatInformation");
    pushTest(&tests, test_fileflags_gethasformatinformation, "FileFlags.cpp: setHasFormatInformation");
    pushTest(&tests, test_fileflags_getarepermissionsinextendedattributerecord, "FileFlags.cpp: getArePermissionsInExtendedAttributeRecord");
    pushTest(&tests, test_fileflags_setarepermissionsinextendedattributerecord, "FileFlags.cpp: setArePermissionsInExtendedAttributeRecord");
    pushTest(&tests, test_fileflags_getreservedbit5, "FileFlags.cpp: getReservedBit5");
    pushTest(&tests, test_fileflags_setreservedbit5, "FileFlags.cpp: setReservedBit5");
    pushTest(&tests, test_fileflags_getreservedbit6, "FileFlags.cpp: getReservedBit6");
    pushTest(&tests, test_fileflags_setreservedbit6, "FileFlags.cpp: setReservedBit6");
    pushTest(&tests, test_fileflags_getnotfinalrecord, "FileFlags.cpp: getNotFinalRecord");
    pushTest(&tests, test_fileflags_setnotfinalrecord, "FileFlags.cpp: setNotFinalRecord");
    return tests;
}