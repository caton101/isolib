/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"

/**
 * @brief  This tests all methods inside the Orphan class
 */
namespace testOrphan
{
    /**
     * @brief  A test for Orphan's setData and getData methods
     * @note   Checks if the data vector can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_orphan_setdata_getdata();

    /**
     * @brief  A test for Orphan's setAddress and getAddress methods
     * @note   Checks if the address can be set and fetched correctly
     * @retval true if the test passed
     */
    bool test_orphan_setaddress_getaddress();

    /**
     * @brief  A test for Orphan's no-args constructor
     * @note   Checks if a Orphan object can be created correctly
     * @retval true if the test passed
     */
    bool test_orphan_constructor_1();

    /**
     * @brief  A test for Orphan's normal constructor
     * @note   Checks if a Orphan object is initialized correctly
     * @retval true if the test passed
     */
    bool test_orphan_constructor_2();

    /**
     * @brief  A test for Orphan's clone method
     * @note   Checks if a Orphan object is cloned correctly
     * @retval true if the test passed
     */
    bool test_orphan_clone();

    /**
     * @brief  A test for Orphan's markAddresses method
     * @note   Checks for an exception when bits is null
     * @retval true if the test passed
     */
    bool test_orphan_markaddresses_1();

    /**
     * @brief  A test for Orphan's markAddresses method
     * @note   Checks if a Orphan object marks its addresses correctly
     * @retval true if the test passed
     */
    bool test_orphan_markaddresses_2();

    /**
     * @brief  This returns a vector of tests to run
     * @retval a vector of tests to run
     */
    TEST_VECTOR_T getTests();
}