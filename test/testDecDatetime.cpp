/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testDecDatetime.hpp"
#include "../include/DecDatetime.hpp"
#include "../include/definitions.hpp"

bool testDecDatetime::test_decdatetime_setyear_getyear()
{
    std::string year = "1970";
    isochop::DecDatetime dd;
    dd.setYear(year);
    return dd.getYear() == year;
}

bool testDecDatetime::test_decdatetime_setmonth_getmonth()
{
    std::string month = "12";
    isochop::DecDatetime dd;
    dd.setMonth(month);
    return dd.getMonth() == month;
}

bool testDecDatetime::test_decdatetime_setday_getday()
{
    std::string day = "12";
    isochop::DecDatetime dd;
    dd.setDay(day);
    return dd.getDay() == day;
}

bool testDecDatetime::test_decdatetime_sethour_gethour()
{
    std::string hour = "12";
    isochop::DecDatetime dd;
    dd.setHour(hour);
    return dd.getHour() == hour;
}

bool testDecDatetime::test_decdatetime_setminute_getminute()
{
    std::string minute = "12";
    isochop::DecDatetime dd;
    dd.setMinute(minute);
    return dd.getMinute() == minute;
}

bool testDecDatetime::test_decdatetime_setsecond_getsecond()
{
    std::string second = "12";
    isochop::DecDatetime dd;
    dd.setSecond(second);
    return dd.getSecond() == second;
}

bool testDecDatetime::test_decdatetime_sethundredths_gethundredths()
{
    std::string hundredths = "12";
    isochop::DecDatetime dd;
    dd.setHundredths(hundredths);
    return dd.getHundredths() == hundredths;
}

bool testDecDatetime::test_decdatetime_settimezone_gettimezone()
{
    char timezone = 4;
    isochop::DecDatetime dd;
    dd.setTimezone(timezone);
    return dd.getTimezone() == timezone;
}

bool testDecDatetime::test_decdatetime_setdata_getdata()
{
    std::vector<char> ans;
    ans.reserve(ISOCHOP_DEC_DATETIME_SIZE_TOTAL);
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        ans.push_back('0' + (i % 10));
    }
    isochop::DecDatetime dd;
    dd.setData(ans);
    std::vector<char> out = dd.getData();
    for (int i = 0; i < ISOCHOP_DEC_DATETIME_SIZE_TOTAL; i++)
    {
        if (ans.at(i) != out.at(i))
        {
            return false;
        }
    }
    return true;
}

bool testDecDatetime::test_decdatetime_isvalid_1()
{
    isochop::DecDatetime dd;
    return dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_2()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR) = '1';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 1) = '9';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 2) = '7';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 3) = '0';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_3()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MONTH) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MONTH + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_4()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_DAY) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_DAY + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_5()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HOUR) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HOUR + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_6()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MINUTE) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MINUTE + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_7()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_SECOND) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_SECOND + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_8()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS + 1) = '1';
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_9()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_TIMEZONE) = 12;
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_10()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_11()
{
    std::vector<char> data = {
        '0', '0', '0', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_12()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '9', '9',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_13()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '9', '9',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_14()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '9', '9',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_15()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '9', '9',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_16()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '9', '9',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_17()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        'N', 'O',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_isvalid_18()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        99                  // timezone
    };
    isochop::DecDatetime dd;
    dd.setData(data);
    return !dd.isValid();
}

bool testDecDatetime::test_decdatetime_constructor_1()
{
    isochop::DecDatetime *dd = new isochop::DecDatetime();
    return dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_2()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR) = '1';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 1) = '9';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 2) = '7';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_YEAR + 3) = '0';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_3()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MONTH) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MONTH + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_4()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_DAY) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_DAY + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_5()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HOUR) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HOUR + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_6()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MINUTE) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_MINUTE + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_7()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_SECOND) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_SECOND + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_8()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS) = '0';
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_HUNDREDTHS + 1) = '1';
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_9()
{
    std::vector<char> data(ISOCHOP_DEC_DATETIME_SIZE_TOTAL, 0);
    data.at(ISOCHOP_DEC_DATETIME_OFFSET_TIMEZONE) = 12;
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_10()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_11()
{
    std::vector<char> data = {
        '0', '0', '0', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_12()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '9', '9',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_13()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '9', '9',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_14()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '9', '9',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_15()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '9', '9',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_16()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '9', '9',           // second
        '5', '5',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_17()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        'N', 'O',           // hundredths
        4                   // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

bool testDecDatetime::test_decdatetime_constructor_18()
{
    std::vector<char> data = {
        '1', '9', '7', '0', // year
        '1', '2',           // month
        '1', '2',           // day
        '1', '2',           // hour
        '1', '2',           // minute
        '1', '2',           // second
        '5', '5',           // hundredths
        99                  // timezone
    };
    isochop::DecDatetime *dd = new isochop::DecDatetime(data);
    return !dd->isValid();
}

TEST_VECTOR_T testDecDatetime::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_decdatetime_setyear_getyear, "DecDatetime.cpp: setYear/getYear");
    pushTest(&tests, test_decdatetime_setmonth_getmonth, "DecDatetime.cpp: setMonth/getMonth");
    pushTest(&tests, test_decdatetime_setday_getday, "DecDatetime.cpp: setDay/getDay");
    pushTest(&tests, test_decdatetime_sethour_gethour, "DecDatetime.cpp: setHour/getHour");
    pushTest(&tests, test_decdatetime_setminute_getminute, "DecDatetime.cpp: setMinute/getMinute");
    pushTest(&tests, test_decdatetime_setsecond_getsecond, "DecDatetime.cpp: setSecond/getSecond");
    pushTest(&tests, test_decdatetime_sethundredths_gethundredths, "DecDatetime.cpp: setHundredths/getHundredths");
    pushTest(&tests, test_decdatetime_settimezone_gettimezone, "DecDatetime.cpp: setTimezone/getTimezone");
    pushTest(&tests, test_decdatetime_setdata_getdata, "DecDatetime.cpp: setData/getData");
    pushTest(&tests, test_decdatetime_isvalid_1, "DecDatetime.cpp: isValid (empty)");
    pushTest(&tests, test_decdatetime_isvalid_2, "DecDatetime.cpp: isValid (only year)");
    pushTest(&tests, test_decdatetime_isvalid_3, "DecDatetime.cpp: isValid (only month)");
    pushTest(&tests, test_decdatetime_isvalid_4, "DecDatetime.cpp: isValid (only day)");
    pushTest(&tests, test_decdatetime_isvalid_5, "DecDatetime.cpp: isValid (only hour)");
    pushTest(&tests, test_decdatetime_isvalid_6, "DecDatetime.cpp: isValid (only minute)");
    pushTest(&tests, test_decdatetime_isvalid_7, "DecDatetime.cpp: isValid (only second)");
    pushTest(&tests, test_decdatetime_isvalid_8, "DecDatetime.cpp: isValid (only hundredth)");
    pushTest(&tests, test_decdatetime_isvalid_9, "DecDatetime.cpp: isValid (only timezone)");
    pushTest(&tests, test_decdatetime_isvalid_10, "DecDatetime.cpp: isValid (legal)");
    pushTest(&tests, test_decdatetime_isvalid_11, "DecDatetime.cpp: isValid (illegal year)");
    pushTest(&tests, test_decdatetime_isvalid_12, "DecDatetime.cpp: isValid (illegal month)");
    pushTest(&tests, test_decdatetime_isvalid_13, "DecDatetime.cpp: isValid (illegal day)");
    pushTest(&tests, test_decdatetime_isvalid_14, "DecDatetime.cpp: isValid (illegal hour)");
    pushTest(&tests, test_decdatetime_isvalid_15, "DecDatetime.cpp: isValid (illegal minute)");
    pushTest(&tests, test_decdatetime_isvalid_16, "DecDatetime.cpp: isValid (illegal second)");
    pushTest(&tests, test_decdatetime_isvalid_17, "DecDatetime.cpp: isValid (illegal hundredth)");
    pushTest(&tests, test_decdatetime_isvalid_18, "DecDatetime.cpp: isValid (illegal timezone)");
    pushTest(&tests, test_decdatetime_constructor_1, "DecDatetime.cpp: DecDatetime (empty)");
    pushTest(&tests, test_decdatetime_constructor_2, "DecDatetime.cpp: DecDatetime (only year)");
    pushTest(&tests, test_decdatetime_constructor_3, "DecDatetime.cpp: DecDatetime (only month)");
    pushTest(&tests, test_decdatetime_constructor_4, "DecDatetime.cpp: DecDatetime (only day)");
    pushTest(&tests, test_decdatetime_constructor_5, "DecDatetime.cpp: DecDatetime (only hour)");
    pushTest(&tests, test_decdatetime_constructor_6, "DecDatetime.cpp: DecDatetime (only minute)");
    pushTest(&tests, test_decdatetime_constructor_7, "DecDatetime.cpp: DecDatetime (only second)");
    pushTest(&tests, test_decdatetime_constructor_8, "DecDatetime.cpp: DecDatetime (only hundredth)");
    pushTest(&tests, test_decdatetime_constructor_9, "DecDatetime.cpp: DecDatetime (only timezone)");
    pushTest(&tests, test_decdatetime_constructor_10, "DecDatetime.cpp: DecDatetime (legal)");
    pushTest(&tests, test_decdatetime_constructor_11, "DecDatetime.cpp: DecDatetime (illegal year)");
    pushTest(&tests, test_decdatetime_constructor_12, "DecDatetime.cpp: DecDatetime (illegal month)");
    pushTest(&tests, test_decdatetime_constructor_13, "DecDatetime.cpp: DecDatetime (illegal day)");
    pushTest(&tests, test_decdatetime_constructor_14, "DecDatetime.cpp: DecDatetime (illegal hour)");
    pushTest(&tests, test_decdatetime_constructor_15, "DecDatetime.cpp: DecDatetime (illegal minute)");
    pushTest(&tests, test_decdatetime_constructor_16, "DecDatetime.cpp: DecDatetime (illegal second)");
    pushTest(&tests, test_decdatetime_constructor_17, "DecDatetime.cpp: DecDatetime (illegal hundredth)");
    pushTest(&tests, test_decdatetime_constructor_18, "DecDatetime.cpp: DecDatetime (illegal timezone)");
    return tests;
}