/*
 * ISOLib
 * Copyright (C) 2022 Cameron Himes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "testCommon.hpp"
#include "testDummy.hpp"

bool testDummy::test_example_pass()
{
    return true;
}

bool testDummy::test_example_fail()
{
    return false;
}

TEST_VECTOR_T testDummy::getTests()
{
    TEST_VECTOR_T tests;
    pushTest(&tests, test_example_pass, "Dummy test (always passes)");
    // pushTest(&tests, test_example_fail, "Dummy test (always fails)");
    return tests;
}